﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" Title="Client Api" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%=ClientScript.GetWebResourceUrl(typeof(Krystalware.SlickUpload.SlickUploadContext), "Krystalware.SlickUpload.Resources.SlickUpload.js") %>"></script>
    <script type="text/javascript">
        function onFileCountChanged(data)
        {
            var hasFiles = (kw("fileSelector").get_Files().length > 0);

            document.getElementById("fileListTable").style.display = (hasFiles > 0 ? "" : "none");
            document.getElementById("uploadButton").className = "button" + (hasFiles ? "" : " disabled");
        }

        function onBeforeSessionEnd(data)
        {
            document.getElementById("cancelButton").style.display = "none";
        }

        function cancelUpload()
        {
            kw("uploadConnector").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        function onSessionStarted(data)
        {
            document.getElementById("uploadButton").style.display = "none";
            document.getElementById("cancelButton").style.display = "block";
        }

        kw(function ()
        {
            var uploadConnector = new kw.UploadConnector(
            {
                id: "uploadConnector",
                uploadHandlerUrl: "<%=ResolveUrl("~/SlickUpload.axd") %>",
                uploadForm: "uploadForm",
                autoUploadOnSubmit: true,
                onBeforeSessionEnd: onBeforeSessionEnd,
                onUploadSessionStarted: onSessionStarted
            });

            var fileSelector = new kw.FileSelector(
            {
                id : "fileSelector",
                uploadConnector : uploadConnector,
                onFileAdded: onFileCountChanged,
                onFileRemoved: onFileCountChanged,
                maxFileSize: <%=SlickUploadContext.Config.UploadProfiles["default"].MaxRequestLength %>,
                dropZone: "dropZone"
            });

            var fileList = new kw.FileList(
            {
                id : "fileList",
                templateElement: "fileList_template",
                fileSelector : "fileSelector"
            });

            var progressDisplay = new kw.UploadProgressDisplay(
            {
                id:"progressDisplay",
                uploadConnector: "uploadConnector"
            });

            if (kw.support.dragDrop)
		    {
                document.getElementById("dropZone").style.display = "block";
                document.getElementById("dropZoneInstructions").style.display = "inline";
		    }
        });

        kw.debug = <%=HttpContext.Current.IsDebuggingEnabled.ToString().ToLower()%>;
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <% using (Html.BeginForm("ClientApiUploadResult", "Home", FormMethod.Post, new { id = "uploadForm", enctype = "multipart/form-data" })) { %>
        <p>Select files using the add button<span id="dropZoneInstructions" style="display:none"> or by dragging them onto the dropzone</span>.</p>
        <div id="slickUpload">
            <table id="fileListTable" width="100%" style="display:none" cellpadding="2" cellspacing="0">
                <thead>
                    <tr>
                        <th width="20"></th>
                        <th width="50%">Name</th>
                        <th width="25%">Size</th>
                        <th>Progress</th>
                    </tr>
                </thead>
                <tbody id="fileList">
                    <tr id="fileList_template">
                        <td>
                            <a href="javascript:;" class="su-removecommand"><img src="<%=ResolveUrl("~/content/delete.png") %>" width="16" height="16" /></a>
                        </td>
                        <td>
                            <span class="su-filename"></span>
                        </td>
                        <td>
                            <span class="su-filesize"></span>
                            <span class="su-validationmessage"></span>
                        </td>
                        <td>
                            <div style="width:80%;float:left;border:1px solid #0c0">
                                <div class="su-progressbar" style="background-color:#080;height:10px;width:0;text-align:center">
                                    <span class="su-error" style="font-size:65%"></span>                                
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="button-container">
                <a id="fileSelector" class="su-fileselector button">
                    <span><b class="icon add"></b> Add files</span>
                </a>
            </p>
            <div id="dropZone" class="su-dropzone">
                <div>Drag and drop files here.</div>
            </div>
            <div id="progressDisplay" class="su-uploadprogressdisplay">
                <div id="duringUpload">
                    <div>
                        Uploading <span class="su-filecount"></span> file(s), <span class="su-contentlengthtext">(calculating)</span>.
                    </div>
                    <div>
                        Currently uploading: <span class="su-currentfilename"></span>, file <span class="su-currentfileindex"></span> of <span class="su-filecount"></span>.
                    </div>
                    <div>
                        Speed: <span class="su-speedtext">(calculating)</span>.
                    </div>
                    <div>
                        <span class="su-timeremainingtext">(calculating)</span> remaining.
                    </div>
                    <div class="progressBarContainer">
                        <div class="su-progressbar"></div>
                        <div class="progressBarText">
                            <span class="su-PercentCompleteText"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p>
            <a id="uploadButton" class="button disabled" onclick="kw('uploadConnector').start()">
                <span><b class="icon upload"></b> Upload Files</span>
            </a>
            <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                <span><b class="icon cancel"></b> Cancel</span>
            </a>
            <div style="clear:both"></div>
        </p>
    <% } %>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="description" runat="server">
    <p>This sample uses the SlickUpload client API to create an upload form without using the SlickUpload server side controls or helpers.</p>
</asp:Content>