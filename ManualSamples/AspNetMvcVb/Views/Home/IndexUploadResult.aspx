﻿<%@ Page Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage(Of AspNetMvcVb.OverviewModel)" Title="Overview" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <h2>
        Upload Result</h2>
    <% If Not Model Is Nothing and Not Model.UploadSession is Nothing Then%>
    <p>
        Result:
        <%=Model.UploadSession.State.ToString()%></p>
        <% If Model.UploadSession.State <> UploadState.Error Then%>
        <p>
            Files Uploaded:
            <%=Model.UploadSession.UploadedFiles.Count.ToString()%></p>
        <table class="results" width="99%" cellpadding="4" cellspacing="0">
            <thead>
                <tr>
                    <th align="left">
                        Server Location
                    </th>
                    <th align="left">
                        Mime Type
                    </th>
                    <th align="left">
                        Length (bytes)
                    </th>
                </tr>
            </thead>
            <tbody>
                <% For Each file As UploadedFile In Model.UploadSession.UploadedFiles%>
                <tr>
                    <td>
                        <%= file.ServerLocation.Replace("\\", "\\<wbr />")%>
                    </td>
                    <td>
                        <%=file.ContentType %>
                    </td>
                    <td>
                        <%=file.ContentLength %>
                    </td>
                </tr>
                <% Next file%>
            </tbody>
        </table>
        <%  Else%>
        <p>
            Error Type:
            <%=Model.UploadSession.ErrorType.ToString() %></p>
        <% End If%>
    <% Else%>
    <p>
        No upload recieved.</p>
    <% End If%>
    <p>
        <% Using Html.BeginForm("Index", "Home", FormMethod.Post, New With {.id = "settingsForm"})%>
            <%=Html.HiddenFor(Function(x) x.MaxFiles)%>
            <%=Html.HiddenFor(Function(x) x.RequireFileSelection)%>
            <%=Html.HiddenFor(Function(x) x.ConfirmNavigateMessage)%>
            <%=Html.HiddenFor(Function(x) x.ValidExtensions)%>
            <%=Html.HiddenFor(Function(x) x.FileTypeMessage)%>
            <%=Html.HiddenFor(Function(x) x.MaxFileSize)%>
            <%=Html.HiddenFor(Function(x) x.FileSizeMessage)%>
            <%=Html.HiddenFor(Function(x) x.SummaryMessage)%>
            <a href="javascript:;" class="button" onclick="document.getElementById('settingsForm').submit()">
                <span><b class="icon newupload"></b> New Upload</span>
            </a>
        <% End Using%>
        <div style="clear:both"></div>
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="description" runat="server">
    <p>This sample demonstrates the basics of SlickUpload including file selection, maximum files limits, maximum file size limits, file type validation, requiring file selection.</p>
</asp:Content>