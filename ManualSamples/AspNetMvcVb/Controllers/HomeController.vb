﻿Imports Krystalware.SlickUpload

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Public Function Index(model As OverviewModel) As ActionResult
        If model Is Nothing Then
            model = New OverviewModel()
        End If

        Return View(model)
    End Function

    Public Function IndexUploadResult(model As OverviewModel, session As UploadSession) As ActionResult
        model.UploadSession = session

        Return View(model)
    End Function

    Public Function ClientApi() As ActionResult
        Return View()
    End Function

    Public Function ClientApiUploadResult(session As UploadSession) As ActionResult
        Return View(session)
    End Function
End Class
