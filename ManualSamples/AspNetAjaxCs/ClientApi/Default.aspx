﻿<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetAjaxCs.ClientApi.Default" EnableViewState="false" Title="Client Api Sample - SlickUpload 6 Samples" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function startUpload()
        {
            var uploadConnector = kw("<%=uploadConnector.ClientID %>");
            
            if (uploadConnector)
                uploadConnector.start();
        }
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <form id="uploadForm" runat="server">
        <asp:ScriptManager ID="scriptManager" runat="server" EnablePartialRendering="true">
        </asp:ScriptManager>               
        <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="border:2px dotted #ccc;padding:1em">
                    <h2>UpdatePanel</h2>
                    <p>Time: <asp:Label ID="updateLabel" runat="server" /></p>
                    <p><asp:Button ID="updateButton" runat="server" Text="Update Time" 
                            onclick="updateButton_Click" /></p>
                                    <asp:Panel ID="uploadPanel" runat="server">
            <p>Select files using the add button<span id="dropZoneInstructions" style="display:none"> or by dragging them onto the dropzone</span>.</p>
            <p>You can upload files up to <strong><%=SlickUploadContext.Config.UploadProfiles["default"].MaxRequestLength / 1024 %> MB</strong>.</p>
            <div id="slickUpload">
<div class="button-container">
                    <kw:FileSelector id="fileSelector" CssClass="button" uploadConnectorId="uploadConnector" runat="server">
                        <Template>
                                <span><b class="icon add"></b> Add files</span>
                        </Template>
                    </kw:FileSelector>
                </div>
                <table id="fileListTable" width="100%">
                    <thead>
                        <tr>
                            <th width="20"></th>
                            <th width="50%">Name</th>
                            <th width="25%">Size</th>
                            <th>Progress</th>
                        </tr>
                    </thead>
                    <kw:FileList FileSelectorId="fileSelector" runat="server" ContainerTagName="tbody" ItemTagName="tr">
                        <ItemTemplate>
                            <td>
                                <kw:FileListRemoveCommand runat="server"><img src="<%=ResolveUrl("~/content/delete.png") %>" width="16" height="16" /></kw:FileListRemoveCommand>
                            </td>
                            <td>
                                <kw:FileListElement Element="FileName" runat="server" />
                            </td>
                            <td>
                                <kw:FileListElement Element="FileSize" runat="server" />
                                <kw:FileListElement ELement="ValidationMessage" runat="server" />
                            </td>
                            <td>
                                <div style="width:80%;float:left;border:1px solid #0c0">
                                    <kw:UploadProgressBar style="background-color:#080;height:10px;width:0;text-align:center" runat="server">
                                        <span class="su-error" style="font-size:65%"></span>
                                    </kw:UploadProgressBar>
                                </div>
                            </td>
                        </ItemTemplate>
                    </kw:FileList>
                </table>
                <kw:UploadConnector id="uploadConnector" runat="server" AutoUploadOnSubmit="false" OnUploadComplete="uploadConnector_UploadComplete" />

                <kw:UploadProgressDisplay UploadConnectorId="uploadConnector" runat="server">
                    <Template>
                        <div id="duringUpload">
                            <div>
                                Uploading <kw:UploadProgressElement Element="FileCount" runat="server" /> file(s), <kw:UploadProgressElement Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                            </div>
                            <div>
                                Currently uploading: <kw:UploadProgressElement Element="CurrentFileName" runat="server" />, file <kw:UploadProgressElement Element="CurrentFileIndex" runat="server"></kw:UploadProgressElement> of <kw:UploadProgressElement Element="FileCount" runat="server"></kw:UploadProgressElement>.
                            </div>
                            <div>
                                Speed: <kw:UploadProgressElement Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>.
                            </div>
                            <div>
                                <kw:UploadProgressElement Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement> remaining.
                            </div>
                            <div class="progressBarContainer">
                                <kw:UploadProgressBar runat="server" />
                                <div class="progressBarText">
                                    <kw:UploadProgressElement Element="PercentCompleteText" runat="server" />
                                </div>
                            </div>
                        </div>
                    </Template>
                </kw:UploadProgressDisplay>
            </div>
            <p>
                <asp:TextBox ID="txt" runat="server" />
                <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="txt" ValidationGroup="Test" ErrorMessage="oh noes!" />
                <asp:Button ID="btn" runat="server" Text="Submit" ValidationGroup="Test" /> 
            </p>
            <p>
                <asp:Button ID="uploadButton" runat="server" Text="Upload" 
                    onclientclick="startUpload();return false;" />
                <input id="cancelButton" type="button" value="Cancel" onclick="kw('<%=uploadConnector.ClientID %>').cancel()" />
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
            <% if (uploadConnector.UploadSession != null) { %>
                <p>Result: <%=uploadConnector.UploadSession.State%></p>
                <% if (uploadConnector.UploadSession.State != UploadState.Error)
                   { %>
                <p>Files Uploaded: <%=uploadConnector.UploadSession.UploadedFiles.Count.ToString()%></p>
                <asp:Repeater ID="resultsRepeater" runat="server">
                    <HeaderTemplate>
                        <table class="results" width="99%" cellpadding="4" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">Server Location</th>
                                    <th align="left">Mime Type</th>
                                    <th align="left">Length (bytes)</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%#((UploadedFile)Container.DataItem).ServerLocation %></td>
                            <td><%#((UploadedFile)Container.DataItem).ContentType %></td>
                            <td><%#((UploadedFile)Container.DataItem).ContentLength %></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <% } else { %>
                <p>Error Type: <%=uploadConnector.UploadSession.ErrorType%></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <asp:Button ID="newUploadButton" runat="server" Text="New upload" 
                onclick="newUploadButton_Click" />
        </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>    
            </form>
</asp:Content>