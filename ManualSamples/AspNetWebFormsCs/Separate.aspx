﻿<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Separate.aspx.cs" Inherits="AspNetWebFormsCs.Separate" EnableViewState="false" Title="Separate" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <% if (uploadPanel.Visible) { %>
        <script type="text/javascript">
            function onFileCountChanged(data)
            {
                var hasFiles = (kw("<%=fileSelector.ClientID%>").get_Files().length > 0);

                document.getElementById("<%=fileList.ClientID%>").style.display = (hasFiles > 0 ? "" : "none");
                document.getElementById("uploadButton").className = "button" + (hasFiles ? "" : " disabled");
            }

            function onBeforeSessionEnd(data)
            {
                document.getElementById("cancelButton").style.display = "none";
            }

            function startUpload()
            {
                connector = kw("<%=uploadConnector.ClientID%>");
                selector = kw("<%=fileSelector.ClientID%>");

                var files = selector.get_Files();
                var isValid = files.length > 0;

                for (var i = 0; isValid && i < files.length; i++)
                    isValid = files[i].get_IsValid();

                if (isValid)
                    connector.start();
            }
            /* function startUpload()
            {
                kw("<%=uploadConnector.ClientID%>").start();
            }*/

            function cancelUpload()
            {
                kw("<%=uploadConnector.ClientID%>").cancel();

                document.getElementById("cancelButton").style.display = "none";
            }

            function onSessionStarted(data)
            {
                document.getElementById("uploadButton").style.display = "none";
                document.getElementById("cancelButton").style.display = "block";
            }

            window.onload = function ()
            {
                if (kw.support.dragDrop)
                    document.getElementById("dropZoneInstructions").style.display = "inline";
            };
        </script>
    <% } %>
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <form id="uploadForm" runat="server">
        <p>
            <asp:TextBox ID="txt" runat="server" />
            <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="txt" ValidationGroup="Test" ErrorMessage="oh noes!" />
            <asp:Button ID="btn" runat="server" Text="Submit" ValidationGroup="Test" />
        </p>
        <asp:Panel ID="uploadPanel" runat="server">
            <p>Select files using the add button<span id="dropZoneInstructions" style="display:none"> or by dragging them onto the dropzone</span>.</p>
            <div id="slickUpload" class="simple" style="overflow:hidden;zoom:1">
                <kw:UploadConnector id="uploadConnector" runat="server" OnUploadComplete="uploadConnector_UploadComplete" OnClientUploadSessionStarted="onSessionStarted" OnClientBeforeSessionEnd="onBeforeSessionEnd" AutoUploadOnSubmit="false" />
                <kw:FileSelector id="fileSelector" runat="server" OnClientFileAdded="onFileCountChanged" OnClientFileRemoved="onFileCountChanged" ShowDropZoneOnDocumentDragOver="true" Style="float:left" ValidExtensions=".exe">
                    <Template>
                        <a class="button">
                            <span><b class="icon add"></b> Add files</span>
                        </a>
                    </Template>
                    <DropZoneTemplate>
                        <div>Drag and drop files here.</div>                
                    </DropZoneTemplate>
                </kw:FileSelector>
                <kw:FileList id="fileList" runat="server" style="clear:both">
                    <ItemTemplate>
                        <img src="<%=ResolveUrl("~/content/icons/default.png") %>" width="24" height="24" style="vertical-align:middle;margin: -3px 0 0 0;" />
                        <kw:FileListElement Element="FileName" runat="server" />
                        &ndash;
                        <kw:FileListElement Element="FileSize" runat="server" />
                        <kw:FileListRemoveCommand runat="server" style="vertical-align: middle;" Title="Remove file"><img src="<%=ResolveUrl("~/content/delete.png") %>" width="16" height="16" /></kw:FileListRemoveCommand>
                        <kw:FileListElement Element="ValidationMessage" runat="server" />
                    </ItemTemplate>
                </kw:FileList>
                <kw:UploadProgressDisplay id="uploadProgressDisplay" runat="server" style="clear:both">
                    <Template>
                        <div id="duringUpload">
                            <div>
                                Uploading <kw:UploadProgressElement Element="FileCount" runat="server" /> file(s), <kw:UploadProgressElement Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                            </div>
                            <div>
                                Currently uploading: <kw:UploadProgressElement Element="CurrentFileName" runat="server" />, file <kw:UploadProgressElement Element="CurrentFileIndex" runat="server"></kw:UploadProgressElement> of <kw:UploadProgressElement Element="FileCount" runat="server"></kw:UploadProgressElement>.
                            </div>
                            <div>
                                Speed: <kw:UploadProgressElement Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>.
                            </div>
                            <div>
                                <kw:UploadProgressElement Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement> remaining.
                            </div>
                            <div class="progressBarContainer">
                                <kw:UploadProgressBar runat="server" />
                                <div class="progressBarText">
                                    <kw:UploadProgressElement Element="PercentCompleteText" runat="server" />
                                </div>
                            </div>
                        </div>
                    </Template>
                </kw:UploadProgressDisplay>
            </div>
            <p>
                <a ID="uploadButton" class="button disabled" onclick="startUpload()">
                    <span><b class="icon upload"></b> Upload Files</span>
                </a>
                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
            <% if (uploadConnector.UploadSession != null) { %>
                <p>Result: <%=uploadConnector.UploadSession.State.ToString()%></p>
                <% if (uploadConnector.UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%=uploadConnector.UploadSession.UploadedFiles.Count.ToString()%></p>
                <asp:Repeater ID="resultsRepeater" runat="server">
                    <HeaderTemplate>
                        <table class="results" width="99%" cellpadding="4" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">Server Location</th>
                                    <th align="left">Mime Type</th>
                                    <th align="left">Length (bytes)</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%#((UploadedFile)Container.DataItem).ServerLocation.Replace("\\", "\\<wbr />") %></td>
                            <td><%#((UploadedFile)Container.DataItem).ContentType %></td>
                            <td><%#((UploadedFile)Container.DataItem).ContentLength %></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <% } else { %>
                <p>Error Type: <%=uploadConnector.UploadSession.ErrorType.ToString()%></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>
                <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button" OnClick="newUploadButton_Click">
                    <span><b class="icon newupload"></b> New Upload</span>
                </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
    </form>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="description" runat="server">
    <p>This sample uses the FileSelector, FileList, UploadProgressDisplay, and UploadConnector controls individually rather than using the SlickUpload control which composes those controls together.</p>
</asp:Content>