﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;

namespace AspNetWebFormsCs
{
    public partial class ClientApi : System.Web.UI.Page
    {
        UploadSession _uploadSession;

        protected UploadSession UploadSession
        {
            get { return _uploadSession; }
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            uploadResultPanel.Visible = true;
            uploadPanel.Visible = false;

            _uploadSession = SlickUploadContext.CurrentUploadSession;

            if (UploadSession != null && UploadSession.State == UploadState.Complete)
            {
                if (UploadSession.UploadedFiles.Count > 0)
                {
                    resultsRepeater.DataSource = UploadSession.UploadedFiles;
                    resultsRepeater.DataBind();
                }
            }
        }

        protected void newUploadButton_Click(object sender, EventArgs e)
        {
            uploadResultPanel.Visible = false;
            uploadPanel.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ToString();
        }
    }
}