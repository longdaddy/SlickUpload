﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;

namespace AspNetWebFormsCs
{
    public partial class Overview : System.Web.UI.Page
    {
        protected void slickUpload_UploadComplete(object sender, UploadSessionEventArgs e)
        {
            uploadResultPanel.Visible = true;
            uploadPanel.Visible = false;

            if (e.UploadSession != null && e.UploadSession.State == UploadState.Complete)
            {
                if (e.UploadSession.UploadedFiles.Count > 0)
                {
                    resultsRepeater.DataSource = e.UploadSession.UploadedFiles;
                    resultsRepeater.DataBind();
                }
            }            
        }

        protected void newUploadButton_Click(object sender, EventArgs e)
        {
            uploadResultPanel.Visible = false;
            uploadPanel.Visible = true;
        }
        
        protected void saveSettingsButton_Click(object sender, EventArgs e)
        {
            maxFilesTextBox.Value = maxFilesTextBox_new.Text;
            maxFileSizeTextBox.Value = maxFileSizeTextBox_new.Text;
            requireFileCheckBox.Value = requireFileCheckBox_new.Checked.ToString();
            confirmNavigateTextBox.Value = confirmNavigateTextBox_new.Text;
            validExtensionsTextBox.Value = validExtensionsTextBox_new.Text;
            invalidExtensionMessageTextBox.Value = invalidExtensionMessageTextBox_new.Text;
            invalidSizeMessageTextBox.Value = invalidSizeMessageTextBox_new.Text;
            validationSummaryMessageTextBox.Value = validationSummaryMessageTextBox_new.Text;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            int maxFiles;
            int maxFileSize;

            if (int.TryParse(maxFilesTextBox.Value, out maxFiles))
            {
                slickUpload.MaxFiles = maxFiles;
                maxFilesSpan.InnerText = maxFilesTextBox_new.Text = maxFiles.ToString();
            }

            if (int.TryParse(maxFileSizeTextBox.Value, out maxFileSize))
            {
                slickUpload.MaxFileSize = maxFileSize;
                maxFileSizeTextBox_new.Text = maxFileSize.ToString();
                maxFileSizeSpan.InnerText = maxFileSize.ToString() + " KB";
            }

            slickUpload.ConfirmNavigateDuringUploadMessage = confirmNavigateTextBox.Value;
            confirmNavigateSpan.InnerText = confirmNavigateTextBox.Value;
            slickUpload.ValidExtensions = validExtensionsTextBox.Value;
            validExtensionsSpan.InnerText = validExtensionsTextBox.Value;
            slickUpload.InvalidExtensionMessage = invalidExtensionMessageTextBox.Value;
            invalidExtensionMessageSpan.InnerText = invalidExtensionMessageTextBox.Value;
            slickUpload.InvalidFileSizeMessage = invalidSizeMessageTextBox.Value;
            invalidSizeMessageSpan.InnerText = invalidSizeMessageTextBox.Value;

            requireFileSpan.InnerText = requireFileCheckBox.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase) ? "Yes" : "No";
            requiredFilesValidator.Enabled = requireFileCheckBox.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase);

            validationSummaryMessageSpan.InnerText = validationSummaryMessageTextBox.Value;
            summaryValidator.Text = validationSummaryMessageTextBox.Value;
            summaryValidator.Enabled = !string.IsNullOrEmpty(validExtensionsTextBox.Value) && !string.IsNullOrEmpty(validationSummaryMessageTextBox.Value);
        }
    }
}