﻿Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Web.Controls

Public Class Overview
    Inherits System.Web.UI.Page

    Protected Sub slickUpload_UploadComplete(ByVal sender As Object, ByVal e As UploadSessionEventArgs) Handles slickUpload.UploadComplete
        uploadResultPanel.Visible = True
        uploadPanel.Visible = False

        If Not e.UploadSession Is Nothing AndAlso e.UploadSession.State = UploadState.Complete Then
            If e.UploadSession.UploadedFiles.Count > 0 Then
                resultsRepeater.DataSource = e.UploadSession.UploadedFiles
                resultsRepeater.DataBind()
            End If
        End If
    End Sub

    Protected Sub newUploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newUploadButton.Click
        uploadResultPanel.Visible = False
        uploadPanel.Visible = True
    End Sub

    Protected Sub saveSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveSettingsButton.Click
        maxFilesTextBox.Value = maxFilesTextBox_new.Text
        maxFileSizeTextBox.Value = maxFileSizeTextBox_new.Text
        requireFileCheckBox.Value = requireFileCheckBox_new.Checked.ToString()
        confirmNavigateTextBox.Value = confirmNavigateTextBox_new.Text
        validExtensionsTextBox.Value = validExtensionsTextBox_new.Text
        invalidExtensionMessageTextBox.Value = invalidExtensionMessageTextBox_new.Text
        invalidSizeMessageTextBox.Value = invalidSizeMessageTextBox_new.Text
        validationSummaryMessageTextBox.Value = validationSummaryMessageTextBox_new.Text
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim maxFiles As Integer
        Dim maxFileSize As Integer

        If Integer.TryParse(maxFilesTextBox.Value, maxFiles) Then
            slickUpload.MaxFiles = maxFiles
            maxFilesSpan.InnerText = maxFilesTextBox_new.Text = maxFiles.ToString()
        End If

        If Integer.TryParse(maxFileSizeTextBox.Value, maxFileSize) Then
            slickUpload.MaxFileSize = maxFileSize
            maxFileSizeTextBox_new.Text = maxFileSize.ToString()
            maxFileSizeSpan.InnerText = maxFileSize.ToString() + " KB"
        End If

        slickUpload.ConfirmNavigateDuringUploadMessage = confirmNavigateTextBox.Value
        confirmNavigateSpan.InnerText = confirmNavigateTextBox.Value
        slickUpload.ValidExtensions = validExtensionsTextBox.Value
        validExtensionsSpan.InnerText = validExtensionsTextBox.Value
        slickUpload.InvalidExtensionMessage = invalidExtensionMessageTextBox.Value
        invalidExtensionMessageSpan.InnerText = invalidExtensionMessageTextBox.Value
        slickUpload.InvalidFileSizeMessage = invalidSizeMessageTextBox.Value
        invalidSizeMessageSpan.InnerText = invalidSizeMessageTextBox.Value

        requireFileSpan.InnerText = IIf(requireFileCheckBox.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase), "Yes", "No")
        requiredFilesValidator.Enabled = requireFileCheckBox.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase)

        validationSummaryMessageSpan.InnerText = validationSummaryMessageTextBox.Value
        summaryValidator.Text = validationSummaryMessageTextBox.Value
        summaryValidator.Enabled = Not String.IsNullOrEmpty(validExtensionsTextBox.Value) AndAlso Not String.IsNullOrEmpty(validationSummaryMessageTextBox.Value)
    End Sub
End Class