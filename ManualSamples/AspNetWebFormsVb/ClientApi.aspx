﻿<%@ Page Language="vb" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="false" CodeBehind="ClientApi.aspx.vb" Inherits="AspNetWebFormsVb.ClientApi" EnableViewState="false" Title="Client Api" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <% If uploadPanel.Visible Then%>
        <script type="text/javascript" src="<%=ClientScript.GetWebResourceUrl(GetType(Krystalware.SlickUpload.SlickUploadContext), "Krystalware.SlickUpload.Resources.SlickUpload.js") %>"></script>
        <script type="text/javascript">
            function onFileCountChanged(data)
            {
                var hasFiles = (kw("fileSelector").get_Files().length > 0);

                document.getElementById("fileListTable").style.display = (hasFiles > 0 ? "" : "none");
                document.getElementById("<%=uploadButton.ClientID%>").className = "button" + (hasFiles ? "" : " disabled");
            }

            function onBeforeSessionEnd(data)
            {
                document.getElementById("cancelButton").style.display = "none";
            }

            function cancelUpload()
            {
                kw("uploadConnector").cancel();

                document.getElementById("cancelButton").style.display = "none";
            }

            function onSessionStarted(data)
            {
                document.getElementById("<%=uploadButton.ClientID%>").style.display = "none";
                document.getElementById("cancelButton").style.display = "block";
            }

            kw(function ()
            {
                var uploadConnector = new kw.UploadConnector(
                {
                    id: "uploadConnector",
                    uploadHandlerUrl: "<%=ResolveUrl("~/SlickUpload.axd") %>",
                    uploadForm: "<%=uploadForm.ClientID %>",
                    autoUploadOnSubmit: true,
                    onBeforeSessionEnd: onBeforeSessionEnd,
                    onUploadSessionStarted: onSessionStarted
                });

                var fileSelector = new kw.FileSelector(
                {
                    id : "fileSelector",
                    uploadConnector : uploadConnector,
                    onFileAdded: onFileCountChanged,
                    onFileRemoved: onFileCountChanged,
                    maxFileSize: <%=SlickUploadContext.Config.UploadProfiles("default").MaxRequestLength %>,
                    dropZone: "dropZone"
                });

                var fileList = new kw.FileList(
                {
                    id : "fileList",
                    templateElement: "fileList_template",
                    fileSelector : "fileSelector"
                });

                var progressDisplay = new kw.UploadProgressDisplay(
                {
                    id:"progressDisplay",
                    uploadConnector: "uploadConnector"
                });

                if (kw.support.dragDrop)
		        {
                    document.getElementById("dropZone").style.display = "block";
                    document.getElementById("dropZoneInstructions").style.display = "inline";
		        }
            });

            kw.debug = <%=HttpContext.Current.IsDebuggingEnabled.ToString().ToLower()%>;
        </script>
    <% End If%>
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <form id="uploadForm" runat="server">
        <asp:Panel ID="uploadPanel" runat="server">
            <p>Select files using the add button<span id="dropZoneInstructions" style="display:none"> or by dragging them onto the dropzone</span>.</p>
            <p>You can upload files up to <strong><%=SlickUploadContext.Config.UploadProfiles("default").MaxRequestLength / 1024 %> MB</strong>.</p>
            <div id="slickUpload">
                <table id="fileListTable" width="100%" style="display:none">
                    <thead>
                        <tr>
                            <th width="20"></th>
                            <th width="50%">Name</th>
                            <th width="25%">Size</th>
                            <th>Progress</th>
                        </tr>
                    </thead>
                    <tbody id="fileList">
                        <tr id="fileList_template">
                            <td>
                                <a href="javascript:;" class="su-removecommand"><img src="<%=ResolveUrl("~/content/delete.png") %>" width="16" height="16" /></a>
                            </td>
                            <td>
                                <span class="su-filename"></span>
                            </td>
                            <td>
                                <span class="su-filesize"></span>
                                <span class="su-validationmessage"></span>
                            </td>
                            <td>
                                <div style="width:80%;float:left;border:1px solid #0c0">
                                    <div class="su-progressbar" style="background-color:#080;height:10px;width:0;text-align:center">
                                        <span class="su-error" style="font-size:65%"></span>                                
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class="button-container">
                    <a id="fileSelector" class="su-fileselector button">
                        <span><b class="icon add"></b> Add files</span>
                    </a>
                </p>
                <div id="dropZone" class="su-dropzone">
                    <div>Drag and drop files here.</div>
                </div>
                <div id="progressDisplay" class="su-uploadprogressdisplay">
                    <div id="duringUpload">
                        <div>
                            Uploading <span class="su-filecount"></span> file(s), <span class="su-contentlengthtext">(calculating)</span>.
                        </div>
                        <div>
                            Currently uploading: <span class="su-currentfilename"></span>, file <span class="su-currentfileindex"></span> of <span class="su-filecount"></span>.
                        </div>
                        <div>
                            Speed: <span class="su-speedtext">(calculating)</span>.
                        </div>
                        <div>
                            <span class="su-timeremainingtext">(calculating)</span> remaining.
                        </div>
                        <div class="progressBarContainer">
                            <div class="su-progressbar"></div>
                            <div class="progressBarText">
                                <span class="su-PercentCompleteText"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p>
                <asp:LinkButton ID="uploadButton" runat="server" CssClass="button disabled" CausesValidation="true">
                    <span><b class="icon upload"></b> Upload Files</span>
                </asp:LinkButton>
                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
            <% If Not UploadSession Is Nothing Then%>
                <p>Result: <%=UploadSession.State.ToString()%></p>
                <% If UploadSession.State <> UploadState.Error Then%>
                <p>Files Uploaded: <%=UploadSession.UploadedFiles.Count.ToString()%></p>
                <asp:Repeater ID="resultsRepeater" runat="server">
                    <HeaderTemplate>
                        <table class="results" width="99%" cellpadding="4" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">Server Location</th>
                                    <th align="left">Mime Type</th>
                                    <th align="left">Length (bytes)</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# DirectCast(Container.DataItem, UploadedFile).ServerLocation.Replace("\", "\<wbr />")%></td>
                            <td><%# DirectCast(Container.DataItem, UploadedFile).ContentType%></td>
                            <td><%# DirectCast(Container.DataItem, UploadedFile).ContentLength%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <%  Else%>
                <p>Error Type: <%=UploadSession.ErrorType.ToString() %></p>
                <% End If%>
            <%  Else%>
            <p>No upload recieved.</p>
            <% End If%>
            <p>
                <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button" onclick="newUploadButton_Click">
                    <span><b class="icon newupload"></b> New Upload</span>
                </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
    </form>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="description" runat="server">
    <p>This sample uses the SlickUpload client API to create an upload form without using the SlickUpload server side controls or helpers.</p>
</asp:Content>