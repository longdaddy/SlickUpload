﻿Imports Krystalware.SlickUpload

Public Class ClientApi
    Inherits System.Web.UI.Page

    Private _uploadSession As UploadSession

    Protected ReadOnly Property UploadSession As UploadSession
        Get
            Return _uploadSession
        End Get
    End Property

    Protected Sub uploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uploadButton.Click
        uploadResultPanel.Visible = True
        uploadPanel.Visible = False

        _uploadSession = SlickUploadContext.CurrentUploadSession

        If Not UploadSession Is Nothing AndAlso UploadSession.State = UploadState.Complete Then
            If UploadSession.UploadedFiles.Count > 0 Then
                resultsRepeater.DataSource = UploadSession.UploadedFiles
                resultsRepeater.DataBind()
            End If
        End If
    End Sub

    Protected Sub newUploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newUploadButton.Click
        uploadResultPanel.Visible = False
        uploadPanel.Visible = True
    End Sub

End Class