﻿<%@ Page Language="VB" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="AspNetWebFormsVb.Overview" EnableViewState="false" Title="Overview" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Validate_SlickUploadRequiredFiles(source, args)
        {
            args.IsValid = (kw("<%=slickUpload.ClientID %>").get_Files().length > 0);
        }

        function Validate_SlickUploadValidFiles(source, args)
        {
            var files = kw("<%=slickUpload.ClientID %>").get_Files();

            args.IsValid = true;

            for (var i = 0; i < files.length; i++)
            {
                if (!files[i].isValid)
                {
                    args.IsValid = false;

                    return;
                }
            }
        }

        function onFileSelectionChanged(data)
        {
            document.getElementById("<%=uploadButton.ClientID%>").className = "button" + (kw("<%=slickUpload.ClientID %>").get_Files().length > 0 ? "" : " disabled");
        }

        function onBeforeSessionEnd(data)
        {
            document.getElementById("<%=uploadButton.ClientID%>").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }

        function cancelUpload()
        {
            kw("<%=slickUpload.ClientID %>").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        function startEdit()
        {
            if (window.kw)
                kw("<%=slickUpload.ClientID %>").clear();

            var panel = document.getElementById("<%=uploadPanel.ClientID %>");

            if (!panel)
                panel = document.getElementById("<%=uploadResultPanel.ClientID %>");

            panel.style.display = "none";

            document.getElementById("<%=settingsBox.ClientID%>").style.backgroundColor = "#ffffe0";
            document.getElementById("<%=settingsBox.ClientID%>").style.padding = "1em";
            document.getElementById("<%=settingsBox.ClientID%>").style.border = "1px solid #ccc";
            document.getElementById("settingsTable").style.width = "99%";
            window.setTimeout(function () { document.getElementById("settingsTable").style.width = "100%"; }, 1);

            document.getElementById("editSettingsButton").style.display = "none";

            document.getElementById("<%=saveSettingsButton.ClientID%>").style.display = "";
            document.getElementById("<%=cancelSettingsButton.ClientID%>").style.display = "";

            document.getElementById("<%= maxFilesSpan.ClientID %>").style.display = "none";
            document.getElementById("<%= maxFileSizeSpan.ClientID %>").style.display = "none";
            document.getElementById("<%= requireFileSpan.ClientID %>").style.display = "none";
            document.getElementById("<%= confirmNavigateSpan.ClientID %>").style.display = "none";
            document.getElementById("<%= validExtensionsSpan.ClientID %>").style.display = "none";
            document.getElementById("<%= invalidExtensionMessageSpan.ClientID %>").style.display = "none";
            document.getElementById("<%= invalidSizeMessageSpan.ClientID %>").style.display = "none";
            document.getElementById("<%= validationSummaryMessageSpan.ClientID %>").style.display = "none";

            document.getElementById("<%= maxFilesTextBox_new.ClientID %>").style.display = "";
            document.getElementById("<%= maxFileSizeTextBox_new.ClientID %>").style.display = "";
            document.getElementById("<%= requireFileCheckBox_new.ClientID %>").parentNode.style.display = "";
            document.getElementById("<%= confirmNavigateTextBox_new.ClientID %>").style.display = "";
            document.getElementById("<%= validExtensionsTextBox_new.ClientID %>").style.display = "";
            document.getElementById("<%= invalidExtensionMessageTextBox_new.ClientID %>").style.display = "";
            document.getElementById("<%= invalidSizeMessageTextBox_new.ClientID %>").style.display = "";
            document.getElementById("<%= validationSummaryMessageTextBox_new.ClientID %>").style.display = "";
        }

        function onSessionStarted(data)
        {
            document.getElementById("<%=uploadButton.ClientID%>").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <form id="uploadForm" runat="server">
        <div style="margin-bottom:1em;" id="settingsBox" runat="server">
            <table id="settingsTable" class="settings">
                <tbody>
                    <tr>
                        <th colspan="3" style="font-weight:bold;border-bottom:solid 1px #ccc">General Settings</th>
                    </tr>
                    <tr>
                        <th style="width:12em">Max files:</th>
                        <td style="vertical-align:middle">                            
                            <span id="maxFilesSpan" runat="server"></span>
                            <asp:HiddenField ID="maxFilesTextBox" runat="server" />
                            <asp:TextBox ID="maxFilesTextBox_new" runat="server" style="display:none" />
                        </td>
                        <td style="text-align:right"><em>Number of files</em></td>
                    </tr>
                    <tr>
                        <th>Require file selection:</th>
                        <td colspan="2">
                            <span id="requireFileSpan" runat="server"></span>
                            <asp:HiddenField ID="requireFileCheckBox" runat="server" />
                            <asp:CheckBox ID="requireFileCheckBox_new" runat="server" style="display:none" />
                        </td>
                    </tr>
                    <tr>
                        <th>Confirm navigate message:</th>
                        <td>
                            <span id="confirmNavigateSpan" runat="server"></span>
                            <asp:HiddenField ID="confirmNavigateTextBox" runat="server" />
                            <asp:TextBox ID="confirmNavigateTextBox_new" runat="server" style="display:none" />
                        </td>
                        <td style="text-align:right"><em>Prompt when user navigates during upload</em></td>
                    </tr>
                    <tr>
                        <th colspan="3" style="font-weight:bold;border-bottom:solid 1px #ccc">Validation</th>
                    </tr>
                    <tr>
                        <th>Valid extensions:</th>
                        <td>                            
                            <span id="validExtensionsSpan" runat="server"></span>
                            <asp:HiddenField ID="validExtensionsTextBox" runat="server" />
                            <asp:TextBox ID="validExtensionsTextBox_new" runat="server" style="display:none" />
                        </td>
                        <td style="text-align:right"><em>Comma seperated list of valid extensions</em></td>
                    </tr>
                    <tr>
                        <th>Per file type message:</th>
                        <td>                            
                            <span id="invalidExtensionMessageSpan" runat="server"></span>
                            <asp:HiddenField ID="invalidExtensionMessageTextBox" runat="server" />
                            <asp:TextBox ID="invalidExtensionMessageTextBox_new" runat="server" style="display:none" />
                        </td>
                        <td style="text-align:right"><em>Displayed next to files with invalid types</em></td>
                    </tr>
                    <tr>
                        <th style="width:12em">Max file size:</th>
                        <td style="vertical-align:middle">                            
                            <span id="maxFileSizeSpan" runat="server"></span>
                            <asp:HiddenField ID="maxFileSizeTextBox" runat="server" />
                            <asp:TextBox ID="maxFileSizeTextBox_new" runat="server" style="display:none" />
                        </td>
                        <td style="text-align:right"><em>Size, in KB</em></td>
                    </tr>
                    <tr>
                        <th>Per file size message:</th>
                        <td>                            
                            <span id="invalidSizeMessageSpan" runat="server"></span>
                            <asp:HiddenField ID="invalidSizeMessageTextBox" runat="server" />
                            <asp:TextBox ID="invalidSizeMessageTextBox_new" runat="server" style="display:none" />
                        </td>
                        <td style="text-align:right"><em>Displayed next to files with invalid sizes</em></td>
                    </tr>
                    <tr>
                        <th>Summary message:</th>
                        <td>                            
                            <span id="validationSummaryMessageSpan" runat="server"></span>
                            <asp:HiddenField ID="validationSummaryMessageTextBox" runat="server" />
                            <asp:TextBox ID="validationSummaryMessageTextBox_new" runat="server" style="display:none" />
                        </td>
                        <td style="text-align:right"><em>Summary validation message for invalid files</em></td>
                    </tr>
                </tbody>
            </table>
            <p style="margin:.5em 0">
                <a id="editSettingsButton" href="javascript:;" onclick="startEdit()" class="button">
                    <span><b class="icon settings"></b> Edit Settings</span>
                </a>
                <asp:LinkButton ID="saveSettingsButton" runat="server" CausesValidation="false" style="display:none" CssClass="button" OnClick="saveSettingsButton_Click">
                    <span><b class="icon save"></b> Save Settings</span>
                </asp:LinkButton>
                <asp:LinkButton ID="cancelSettingsButton" runat="server" CausesValidation="false" style="display:none" CssClass="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </asp:LinkButton>
            </p>
            <div style="clear:both"></div>
        </div>
        <asp:Panel ID="uploadPanel" runat="server">
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            <kw:SlickUpload id="slickUpload" runat="server" OnUploadComplete="slickUpload_UploadComplete" OnClientUploadSessionStarted="onSessionStarted" OnClientBeforeSessionEnd="onBeforeSessionEnd" OnClientFileAdded="onFileSelectionChanged" OnClientFileRemoved="onFileSelectionChanged" CssClass="simple" ShowDropZoneOnDocumentDragOver="true" Style="overflow:hidden;zoom:1" FileSelectorStyle="float:left" FileListStyle="clear:both" UploadProgressDisplayStyle="clear:both">
                                <SelectorTemplate>
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                </SelectorTemplate>
                                <SelectorDropZoneTemplate>
                                    <div>Drag and drop files here.</div>                
                                </SelectorDropZoneTemplate>
                                <FileItemTemplate>
                                    <img src="<%=ResolveUrl("~/content/icons/default.png") %>"width="24" height="24" style="vertical-align:middle;margin: -3px 0 0 0;" />
                                    <kw:FileListElement ID="FileListElement1" Element="FileName" runat="server" />
                                    &ndash;
                                    <kw:FileListElement ID="FileListElement2" Element="FileSize" runat="server" />
                                    <kw:FileListRemoveCommand ID="FileListRemoveCommand1" runat="server" style="vertical-align: middle;" Title="Remove file"><img src="<%=ResolveUrl("~/content/delete.png") %>" width="16" height="16" /></kw:FileListRemoveCommand>
                                    <kw:FileListElement ID="FileListElement3" ELement="ValidationMessage" runat="server" />
                                </FileItemTemplate>
                                <ProgressTemplate>
                                    <div id="duringUpload">
                                        <div>
                                            Uploading <kw:UploadProgressElement ID="UploadProgressElement1" Element="FileCount" runat="server" /> file(s), <kw:UploadProgressElement ID="UploadProgressElement2" Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                                        </div>
                                        <div>
                                            Currently uploading: <kw:UploadProgressElement ID="UploadProgressElement3" Element="CurrentFileName" runat="server" />, file <kw:UploadProgressElement ID="UploadProgressElement4" Element="CurrentFileIndex" runat="server"></kw:UploadProgressElement> of <kw:UploadProgressElement ID="UploadProgressElement5" Element="FileCount" runat="server"></kw:UploadProgressElement>.
                                        </div>
                                        <div>
                                            Speed: <kw:UploadProgressElement ID="UploadProgressElement6" Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>.
                                        </div>
                                        <div>
                                            <kw:UploadProgressElement ID="UploadProgressElement7" Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement> remaining.
                                        </div>
                                        <div class="progressBarContainer">
                                            <kw:UploadProgressBar ID="UploadProgressBar1" runat="server" />
                                            <div class="progressBarText">
                                                <kw:UploadProgressElement ID="UploadProgressElement8" Element="PercentCompleteText" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </ProgressTemplate>
                            </kw:SlickUpload>
                            <asp:CustomValidator ID="requiredFilesValidator" runat="server" ClientValidationFunction="Validate_SlickUploadRequiredFiles" Text="Please select at least one file to upload." Display="Dynamic" />
                            <asp:CustomValidator ID="summaryValidator" runat="server" ClientValidationFunction="Validate_SlickUploadValidFiles" Display="Dynamic" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                <asp:LinkButton ID="uploadButton" runat="server" CssClass="button disabled" CausesValidation="true">
                    <span><b class="icon upload"></b> Upload Files</span>
                </asp:LinkButton>
                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
            <% If Not slickUpload.UploadSession Is Nothing Then%>
                <p>Result: <%=slickUpload.UploadSession.State.ToString()%></p>
                <% If slickUpload.UploadSession.State <> UploadState.Error Then%>
                <p>Files Uploaded: <%=slickUpload.UploadSession.UploadedFiles.Count.ToString()%></p>
                <asp:Repeater ID="resultsRepeater" runat="server">
                    <HeaderTemplate>
                        <table class="results" width="99%" cellpadding="4" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">Server Location</th>
                                    <th align="left">Mime Type</th>
                                    <th align="left">Length (bytes)</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# DirectCast(Container.DataItem, UploadedFile).ServerLocation.Replace("\", "\<wbr />")%></td>
                            <td><%# DirectCast(Container.DataItem, UploadedFile).ContentType%></td>
                            <td><%# DirectCast(Container.DataItem, UploadedFile).ContentLength%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <%  Else%>
                <p>Error Type: <%=slickUpload.UploadSession.ErrorType.ToString()%></p>
                <% End If%>
            <%  Else%>
            <p>No upload recieved.</p>
            <% End If%>
            <p>
                <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button" onclick="newUploadButton_Click">
                    <span><b class="icon newupload"></b> New Upload</span>
                </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
    </form>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="description" runat="server">
    <p>This sample demonstrates the basics of SlickUpload including file selection, maximum files limits, maximum file size limits, file type validation, requiring file selection.</p>
</asp:Content>