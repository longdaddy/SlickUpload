﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AspNetMvcCs.Models;
using Krystalware.SlickUpload;

namespace AspNetMvcRazorCs.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(OverviewModel model)
        {
            if (model == null)
                model = new OverviewModel();

            return View(model);
        }

        public ActionResult IndexUploadResult(OverviewModel model, UploadSession session)
        {
            model.UploadSession = session;

            return View(model);
        }

        public ActionResult ClientApi()
        {
            return View();
        }

        public ActionResult ClientApiUploadResult(UploadSession session)
        {
            return View(session);
        }
    }
}
