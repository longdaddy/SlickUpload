IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SlickUploadStatus]') AND type in (N'U'))
DROP TABLE [dbo].[SlickUploadStatus]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SlickUploadStatus](
	[SessionId] [uniqueidentifier] NOT NULL,
	[RequestId] [uniqueidentifier] NULL,
	[Status] [varchar](max) NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [IX_SlickUploadStatus_SessionIdRequestId] UNIQUE CLUSTERED 
(
	[SessionId] ASC,
	[RequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO