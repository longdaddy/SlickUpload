﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WatiN.Core;
using System.Threading;

namespace Krystalware.SlickUpload.StressThread
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            using (IE ie = new IE("http://localhost:8888/", true))
            {
                //ie.Frame(Find.ById("uploadManager")).FileUpload(Find.ById("test")).Set("e:\\downloads\\VS7.1sp1-KB918007-X86.exe");
                //ie.Frame(Find.ById("uploadManager")).FileUpload(Find.ById("test")).Set("e:\\downloads\\Sandcastle.msi");
                SetFileUpload(ie, "slickUpload_selector_html_file0", @"C:\Users\Chris\Downloads\Firefox Setup 7.0.exe");

                //_waitEvent.WaitOne();
                ie.Link(Find.ById("uploadButton")).Click();

                while (!ie.ContainsText("Upload Result"))
                {
                    Thread.Sleep(1000);
                    //ie.WaitForComplete();
                }

                //Assert.True(ie.ContainsText("Complete"));

                ie.Link(Find.ById("newUploadButton")).Click();
                ie.WaitForComplete();

                SetFileUpload(ie, "slickUpload_selector_html_file0", @"C:\Users\Chris\Downloads\SlickUpload-6.1-S3MetadataFix.zip");
                SetFileUpload(ie, "slickUpload_selector_html_file1", @"C:\Users\Chris\Downloads\MSBuild Extension Pack April 2011 (All Files) (1).zip");
                ie.Link(Find.ById("uploadButton")).Click();

                while (!ie.ContainsText("Upload Result"))
                {
                    Thread.Sleep(1000);
                    //ie.WaitForComplete();
                }

                //Assert.True(ie.ContainsText("Complete"));

                //Thread.Sleep(10000);
            }
        }

        static void SetFileUpload(IE ie, string id, string fileName)
        {
                ie.FileUpload(id).Set(fileName);
        }
    }
}
