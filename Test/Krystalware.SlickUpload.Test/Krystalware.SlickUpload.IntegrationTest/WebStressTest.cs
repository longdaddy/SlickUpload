﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using WatiN.Core;
using Xunit;
using System.Diagnostics;

namespace Krystalware.SlickUpload.IntegrationTest
{
    public class WebStressTest
    {
        ManualResetEvent _waitEvent = new ManualResetEvent(false);
        object _setFileLock = new object();

        public void StressTest()
        {
            /*List<Thread> threads = new List<Thread>();

            for (int i = 0; i < 20; i++)
            {
                Thread t = new Thread(StressThread);

                t.SetApartmentState(ApartmentState.STA);

                t.Start();

                threads.Add(t);

                Thread.Sleep(1000);
            }

            _waitEvent.Set();

            foreach (Thread t in threads)
                t.Join();*/

            List<Process> processes = new List<Process>();

            for (int i = 0; i < 5; i++)
            {
                processes.Add(Process.Start(@"C:\Dev\Krystalware\SlickUpload\trunk\Test\Krystalware.SlickUpload.Test\Krystalware.SlickUpload.StressThread\bin\Debug\Krystalware.SlickUpload.StressThread.exe"));
            }

            foreach (Process p in processes)
                p.WaitForExit();

            //StressThread();
        }

        void StressThread()
        {
            //Settings.WaitForCompleteTimeOut = 3600;
            //Settings.WaitUntilExistsTimeOut = 3600;
            
            using (IE ie = new IE("http://localhost:8888/"))
            {
                //ie.Frame(Find.ById("uploadManager")).FileUpload(Find.ById("test")).Set("e:\\downloads\\VS7.1sp1-KB918007-X86.exe");
                //ie.Frame(Find.ById("uploadManager")).FileUpload(Find.ById("test")).Set("e:\\downloads\\Sandcastle.msi");
                SetFileUpload(ie, "slickUpload_selector_html_file0", @"C:\Users\Chris\Downloads\Firefox Setup 7.0.exe");

                //_waitEvent.WaitOne();
                ie.Link(Find.ById("uploadButton")).Click();

                while (!ie.ContainsText("Upload Result"))
                {
                    Thread.Sleep(1000);
                    //ie.WaitForComplete();
                }

                Assert.True(ie.ContainsText("Complete"));

                ie.Link(Find.ById("newUploadButton")).Click();
                ie.WaitForComplete();

                SetFileUpload(ie, "slickUpload_selector_html_file0", @"C:\Users\Chris\Downloads\SlickUpload-6.1-S3MetadataFix.zip");
                SetFileUpload(ie, "slickUpload_selector_html_file1", @"C:\Users\Chris\Downloads\MSBuild Extension Pack April 2011 (All Files) (1).zip");
                ie.Link(Find.ById("uploadButton")).Click();

                while (!ie.ContainsText("Upload Result"))
                {
                    Thread.Sleep(1000);
                    //ie.WaitForComplete();
                }

                Assert.True(ie.ContainsText("Complete"));

                //Thread.Sleep(10000);
            }
        }

        void SetFileUpload(IE ie, string id, string fileName)
        {
            lock (_setFileLock)
            {
                Thread.Sleep(500);

                ie.FileUpload(id).Set(fileName);

                Thread.Sleep(500);
            }
        }
    }
}
