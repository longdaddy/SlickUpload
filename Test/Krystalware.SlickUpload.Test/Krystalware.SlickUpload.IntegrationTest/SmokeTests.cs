﻿using System;
using System.Collections.Generic;
using System.Text;
using Krystalware.Core.Testing;
using Xunit;
using System.Net;
using System.IO;
using WatiN.Core;
using mshtml;
using WatiN.Core.Native.InternetExplorer;
using SHDocVw;
using Xunit.Extensions;

namespace Krystalware.SlickUpload.IntegrationTest
{
    //[SampleProjectTestClassCommand()]
    public class SmokeTests : IUseFixture<WebHostFixture>, IUseFixture<BrowserFixture>
    {        
        WebHostFixture _host;
        Browser _browser;

        public string RootPath
        {
            get { return Path.Combine(
                    Path.GetDirectoryName(
                    Path.GetDirectoryName(
                    Path.GetDirectoryName(
                    Path.GetDirectoryName(
                    Path.GetDirectoryName(Directory.GetCurrentDirectory())))))
                , @"Build\Deploy\Samples"); }
        }

        public static IEnumerable<object[]> ProjectsWithSamples
        {
            get
            {
                foreach (string projectName in new string[] {"AspNetWebFormsCs", "AspNetWebFormsVb", "AspNetAjaxCs", "AspNetAjaxVb", "AspNetMvc2Cs", "AspNetMvc2Vb", "AspNetMvc3Cs", "AspNetMvc3Vb", "AspNetMvcRazorCs" })
                {
                    foreach (string sampleName in new string[] { "ClientApi", "CustomFileName", "CustomSqlServer", "CustomUploadStreamProvider", "Multiple", "PostProcess", "Slick", "SqlServer", "Validation", "S3" })
                    {
                        if (!(projectName.Contains("AspNetAjax") && (sampleName.Contains("ClientApi") || sampleName.Contains("PostProcess"))))
                            yield return new string[] { projectName, sampleName };
                    }
                }
            }
        }

        [Theory]
        [PropertyData("ProjectsWithSamples")]
        public void SampleSmokeTest(string projectRoot, string sample)
        {
            string applicationPath = Path.Combine(RootPath, projectRoot);

            if (_host.RootPath != applicationPath)
                _host.Start(applicationPath);

            Assert.DoesNotThrow(() =>
            {
                WebClient client = new WebClient();

                using (StreamReader r = new StreamReader(client.OpenRead(_host.RootUrl + sample)))
                {
                    string value = r.ReadToEnd();

                    Assert.NotEqual<int>(-1, value.IndexOf("SlickUpload"));
                }
            });

            _browser.GoTo(_host.RootUrl + sample);

            Assert.True(_browser.ContainsText("SlickUpload"));
        }

        /*[Fact]
        public void RootExecuteTest()
        {
            Assert.DoesNotThrow(() =>
            {
                WebClient client = new WebClient();

                using (StreamReader r = new StreamReader(client.OpenRead(_host.RootUrl)))
                {
                    string value = r.ReadToEnd();

                    Assert.NotEqual<int>(-1, value.IndexOf("SlickUpload"));
                }
            });
        }

        [Fact]
        
        public void RootBrowserLoadTest()
        {
            _browser.GoTo(_host.RootUrl);

            Assert.True(_browser.ContainsText("SlickUpload"));
        }

        [Fact]
        public void SampleBrowserLoadTest()
        {
            string[] sampleNames = new string[] { "ClientApi", "CustomFileName", "CustomSqlServer", "CustomUploadStreamProvider", "Multiple", "PostProcess", "Slick", "SqlServer", "Validation" };

            foreach (string sampleName in sampleNames)
            {
                _browser.GoTo(_host.RootUrl + sampleName);

                //IWebBrowser2 nativeBrowser = ((IEBrowser)_browser.NativeBrowser).WebBrowser;
                //nativeBrowser.
                Assert.True(_browser.ContainsText("SlickUpload"));
                // TODO: figure out how to assert that no javascript errors occured
            }
        }*/

        public void SetFixture(BrowserFixture data)
        {
            _browser = data.Browser;
        }

        public void SetFixture(WebHostFixture host)
        {
            _host = host;
        }
    }
}
