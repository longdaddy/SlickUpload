using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Krystalware.SlickUpload;

namespace Krystalware.SlickUpload.WebStressTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UploadResult(UploadSession session)
        {

            return View(session);
        }
    }
}
