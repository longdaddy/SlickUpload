using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;

namespace AspNetWebFormsCs
{
    public partial class QuickStartDefault : System.Web.UI.Page
    {
        protected void slickUpload_UploadComplete(object sender, UploadSessionEventArgs e)
        {
            uploadResult.Text = "Upload Result: " + e.UploadSession.State.ToString();

            if (e.UploadSession.State == UploadState.Error)
                uploadResult.Text += "<br /><br /> Message: " + e.UploadSession.ErrorSummary;

            if (e.UploadSession.State == UploadState.Complete) 
            {
                uploadFileList.DataSource = e.UploadSession.UploadedFiles; 
                uploadFileList.DataBind(); 
            }
        }
    }
}
