<%@ Page Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage(Of Krystalware.SlickUpload.UploadSession)" EnableViewState="false" Title="SlickUpload Quick Start Example" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
    function cancelUpload()
    {
        kw("slickUpload").cancel();
    }

    function onSessionStarted(data)
    {
        document.getElementById("uploadButton").style.display = "none";
        document.getElementById("cancelButton").style.display = "block";
    }
        
    function onBeforeSessionEnd(data)
    {
        document.getElementById("cancelButton").style.display = "none";
    }
</script>

<style type="text/css">
    /* hide initially  */
    .su-fileselector, .su-folderselector, .su-filelisttemplate, .su-uploadprogressdisplay
    {
        display: none;
        zoom:1;
    }
    
    /* fileselector cursor, link color */
    .su-fileselector, .su-fileselector *, .su-folderselector, .su-folderselector *
    {
        color:blue;
        cursor:pointer;
    }

    /* hover links */
    a
    {
        text-decoration:none
    }
    
    a:hover, .su-hover a
    {
        text-decoration:underline;
    }
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <% Using (Html.BeginForm("Index", "Home", FormMethod.Post, New With { .id = "uploadForm", .enctype = "multipart/form-data" })) %>

                       <% Html.KrystalwareWebForms(New SlickUpload() With { _
.Id = "slickUpload", _
.FileSelectorHtmlAttributes = New With { .Style = "float:left;padding-right:1em"}, _
.FileListHtmlAttributes = New With { .Style = "clear:both"}, _
.UploadProgressDisplayHtmlAttributes = New With { .Style = "clear:both"}, _
.HtmlAttributes = New With { .Style = "overflow: hidden; zoom: 1"}, _
.UploadProfile = "quickStart", _
.OnClientUploadSessionStarted = "onSessionStarted", _
.OnClientBeforeSessionEnd = "onBeforeSessionEnd", _
.ShowDropZoneOnDocumentDragOver = true, _
.AutoUploadOnSubmit = true, _
.UploadFormId = "uploadForm", _
.SelectorTemplate = New Template(Sub() %>
                          
                                    <a href="javascript:;">Add files</a>
                                    
<% End Sub), _
.SelectorFolderTemplate = New Template(Sub() %>
                          
                                    <a href="javascript:;">Add folder</a>
                                    
<% End Sub), _
.FileItemTemplate = New Template(Sub() %>
                          
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileName } ) %>
                                        &ndash;
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileSize, .Template = New Template("(calculating)") } ) %>
                                        <% Html.KrystalwareWebForms(New FileListRemoveCommand() With { .HtmlAttributes = New With { .href = "javascript:;"}, .Template = New Template("[x]") } ) %>
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.ValidationMessage, .HtmlAttributes = New With { .style = "color:#f00"} } ) %>
                                    
<% End Sub), _
.ProgressTemplate = New Template(Sub() %>
                          
                                        <div>
                                            Uploading <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.FileCount } ) %> file(s),
                                            <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.ContentLengthText, .Template = New Template("(calculating)") } ) %>.
                                        </div>
                                        <div>
                                            Currently uploading: <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.CurrentFileName } ) %>
                                            file <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.CurrentFileIndex } ) %>
                                            of <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.FileCount } ) %>.
                                        </div>
                                        <div>
                                            Speed: <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.SpeedText, .Template = New Template("(calculating)") } ) %>
                                        </div>
                                        <div>
                                            <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.TimeRemainingText, .Template = New Template("(calculating)") } ) %>
                                        </div>
                                        <div style="border:1px solid #008800; height: 1.5em; position: relative;">
                                            <% Html.KrystalwareWebForms(New UploadProgressBar() With { .HtmlAttributes = New With { .style = "background-color:#00ee00;width:0; height:1.5em;"} } ) %>
                                            <div style="text-align: center; position: absolute; top: .15em; width: 100%;">
                                                <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.PercentCompleteText, .Template = New Template("(calculating)") } ) %>
                                            </div>
                                        </div>
                                    
<% End Sub) _
 } ) %>
<% Html.KrystalwareWebForms(New KrystalwareScriptRenderer()) %>                            <hr />
                                <p>
                                    <input type="submit" value="Upload" id="uploadButton" />
                                    <input type="button" value="Cancel" id="cancelButton" style="display:none" onclick="cancelUpload()" />
                                </p>
<% If Not Model Is Nothing Then %>                        
                        Upload Result: <%:Model.State.ToString() %><% If Model.State = UploadState.Error Then %>                        <br /><br />
                        Message: <%:Model.ErrorSummary %><% End If %>                        <ul>
                        <% For Each file As UploadedFile In Model.UploadedFiles %>
                        <li>
                            <%:file.ClientName %>
                        </li>
                        <% Next file %>
                        </ul>
<% End If %>
    <% End Using %>
</asp:Content>
