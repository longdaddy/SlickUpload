﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit.Sdk;
using System.Reflection;

namespace Krystalware.SlickUpload.IntegrationTest
{
    public class SampleProjectTestClassCommand : ITestClassCommand
    {
        readonly TestClassCommand cmd = new TestClassCommand();

        public object ObjectUnderTest
        {
            get { return cmd.ObjectUnderTest; }
        }

        public ITypeInfo TypeUnderTest
        {
            get { return cmd.TypeUnderTest; }
            set { cmd.TypeUnderTest = value; }
        }

        public int ChooseNextTest(ICollection<IMethodInfo> testsLeftToRun)
        {
            return cmd.ChooseNextTest(testsLeftToRun);
        }

        public Exception ClassFinish()
        {
            return cmd.ClassFinish();
        }

        public Exception ClassStart()
        {
            return cmd.ClassStart();
        }

        public IEnumerable<ITestCommand> EnumerateTestCommands(IMethodInfo testMethod)
        {
            foreach (ITestCommand command in cmd.EnumerateTestCommands(testMethod))
            {
                //command.
                yield return command;
                yield return command;
            }
        }

        public bool IsTestMethod(IMethodInfo testMethod)
        {
            return cmd.IsTestMethod(testMethod);
        }

        public IEnumerable<IMethodInfo> EnumerateTestMethods()
        {
            return cmd.EnumerateTestMethods();
        }
    }
}
