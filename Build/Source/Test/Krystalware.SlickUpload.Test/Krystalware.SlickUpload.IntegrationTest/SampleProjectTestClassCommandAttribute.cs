﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Krystalware.SlickUpload.IntegrationTest
{
    public class SampleProjectTestClassCommandAttribute : RunWithAttribute
    {
        public SampleProjectTestClassCommandAttribute()
            : base(typeof(SampleProjectTestClassCommand))
        { }
    }
}
