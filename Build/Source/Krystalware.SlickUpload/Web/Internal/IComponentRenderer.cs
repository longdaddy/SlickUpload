﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Krystalware.SlickUpload.Web
{
    interface IComponentRenderer
    {
        void Register(IRenderableComponent component);
        void Render(IRenderableComponent component, HtmlTextWriter w);
    }
}
