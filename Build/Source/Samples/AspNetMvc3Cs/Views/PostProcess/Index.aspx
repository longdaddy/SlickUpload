<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" EnableViewState="false" Title="Post Processing" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function startUpload()
        {
            var slickUpload = kw("slickUpload");

            if (slickUpload)
                slickUpload.start();
        }
        
        
        function cancelUpload()
        {
            kw("slickUpload").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("uploadButton").className = "button" + (kw("slickUpload").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
            document.getElementById("uploadButton").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("uploadButton").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
        function onUploadSessionProgress(data)
        {
            if (data.state == "Completing" && data.percentComplete != 100)
            {
                // Show the post processing display
                document.getElementById("duringUpload").style.display = "none";
                document.getElementById("postProcessing").style.display = "block";
            }
        }

    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <% using (Html.BeginForm("UploadResult", "PostProcess", FormMethod.Post, new { id = "uploadForm", enctype = "multipart/form-data" })) { %>
        
        <div id="uploadPanel">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

                            <% Html.KrystalwareWebForms(new SlickUpload() { 
Id = "slickUpload",
OnClientUploadSessionStarted = "onSessionStarted",
OnClientBeforeSessionEnd = "onBeforeSessionEnd",
OnClientFileAdded = "onFileSelectionChanged",
OnClientFileRemoved = "onFileSelectionChanged",
OnClientUploadSessionProgress = "onUploadSessionProgress",
ShowDropZoneOnDocumentDragOver = true,
HtmlAttributes = new { @class = "simple", Style = "overflow:hidden;zoom:1"},
FileSelectorHtmlAttributes = new { Style = "float:left"},
FileListHtmlAttributes = new { Style = "clear:both"},
UploadProgressDisplayHtmlAttributes = new { Style = "clear:both"},
UploadProfile = "postProcess",
AutoUploadOnSubmit = true,
UploadFormId = "uploadForm",
SelectorTemplate = new Template(() => { %>
                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    
<% }),
SelectorFolderTemplate = new Template(() => { %>
                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    
<% }),
SelectorDropZoneTemplate = new Template(() => { %>
                               
                                    <div>Drag and drop files here.</div>                
                                    
<% }),
FileItemTemplate = new Template(() => { %>
                               
                                    <div class="filedata">
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.FileName } ); %>
                                        &ndash;
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.FileSize, Template = new Template("(calculating)") } ); %>
                                    </div>
                                        <% Html.KrystalwareWebForms(new FileListRemoveCommand() { HtmlAttributes = new { href = "javascript:;"}, Template = new Template("[x]") } ); %>
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.ValidationMessage, HtmlAttributes = new { style = "color:#f00"} } ); %>
                                    
<% }),
ProgressTemplate = new Template(() => { %>
                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.FileCount } ); %> file(s),
                                                <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.ContentLengthText, Template = new Template("(calculating)") } ); %>.
                                            </div>
                                            <div>
                                                Currently uploading: <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.CurrentFileName } ); %>
                                                file <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.CurrentFileIndex } ); %>
                                                of <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.FileCount } ); %>.
                                            </div>
                                            <div>
                                                Speed: <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.SpeedText, Template = new Template("(calculating)") } ); %>
                                            </div>
                                            <div>
                                                <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.TimeRemainingText, Template = new Template("(calculating)") } ); %>
                                            </div>
                                            <div class="progressBarContainer">
                                                <% Html.KrystalwareWebForms(new UploadProgressBar()); %>
                                                <div class="progressBarText">
                                                    <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.PercentCompleteText, Template = new Template("(calculating)") } ); %>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="postProcessing" style="display:none">
                                            <div>
                                                Post processing files...
                                            </div>
                                            <div class="progressBarContainer">
                                                <% Html.KrystalwareWebForms(new UploadProgressBar()); %>
                                                <div class="progressBarText">
                                                    <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.PercentCompleteText, Template = new Template("(calculating)") } ); %>
                                                </div>
                                            </div>
                                        </div>                                            
                                    
<% })
 } ); %>
<% Html.KrystalwareWebForms(new KrystalwareScriptRenderer()); %>                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <a id="uploadButton" href="javascript:;" onclick="startUpload()" class="button disabled" >
 <span><b class="icon upload"></b> Upload Files</span>        </a>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </div>
    <% } %>
    

</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    Implements a post processing step to process files after they are uploaded, with accompanying progress bar.
</asp:Content>
