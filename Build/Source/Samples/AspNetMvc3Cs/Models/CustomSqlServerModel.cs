using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Krystalware.SlickUpload;

using AspNetMvc3Cs.Storage;

namespace AspNetMvc3Cs.Models
{
    public class CustomSqlServerModel
    {
        public List<SqlFile> ExistingFiles { get; set; }
        public Exception Exception { get; set; }
        public UploadSession UploadSession { get; set; }
    }
}