Imports Krystalware.SlickUpload

Namespace Models
    Public Class ValidationModel
        Public Property MaxFiles As Nullable(Of Integer)
        Public Property RequireFileSelection As Boolean
        Public Property AllowInvalidUpload As Boolean
        Public Property ConfirmNavigateMessage As String
        Public Property ValidExtensions As String
        Public Property FileTypeMessage As String
        Public Property MaxFileSize As Nullable(Of Integer)
        Public Property FileSizeMessage As String
        Public Property SummaryMessage As String

        Public Property UploadSession As UploadSession
    End Class
End Namespace