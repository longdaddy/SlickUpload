<%@ Page Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage(Of AspNetMvc3Vb.Models.CustomSqlServerModel)" EnableViewState="false" Title="Custom Sql Server" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function startUpload()
        {
            var slickUpload = kw("slickUpload");

            if (slickUpload)
                slickUpload.start();
        }
        
        
        function cancelUpload()
        {
            kw("slickUpload").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("uploadButton").className = "button" + (kw("slickUpload").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
    kw("slickUpload").set_Data("fileCategory", document.getElementById("fileCategory").value);

            document.getElementById("uploadButton").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("uploadButton").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <% Using (Html.BeginForm("UploadResult", "CustomSqlServer", FormMethod.Post, New With { .id = "uploadForm", .enctype = "multipart/form-data" })) %>
        
        <div id="uploadPanel">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">File category</th>
                    </tr>
                    <tr>
                        <td>
                            <select id="fileCategory">
                                <option value="documents">Documents</option>
                                <option value="images">Images</option>
                                <option value="audio">Audio</option>
                                <option value="video">Video</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>The category selected above will be passed to the server with the upload (as a cookie), and used to specify the folder in which the upload files are stored.</p>

            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

                            <% Html.KrystalwareWebForms(New SlickUpload() With { _
.Id = "slickUpload", _
.OnClientUploadSessionStarted = "onSessionStarted", _
.OnClientBeforeSessionEnd = "onBeforeSessionEnd", _
.OnClientFileAdded = "onFileSelectionChanged", _
.OnClientFileRemoved = "onFileSelectionChanged", _
.SelectorTemplate = New Template(Sub() %>
                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    
<% End Sub), _
.ShowDropZoneOnDocumentDragOver = true, _
.HtmlAttributes = New With { .class = "simple", .Style = "overflow:hidden;zoom:1"}, _
.FileSelectorHtmlAttributes = New With { .Style = "float:left"}, _
.FileListHtmlAttributes = New With { .Style = "clear:both"}, _
.UploadProgressDisplayHtmlAttributes = New With { .Style = "clear:both"}, _
.UploadProfile = "customSqlServer", _
.AutoUploadOnSubmit = true, _
.UploadFormId = "uploadForm", _
.SelectorFolderTemplate = New Template(Sub() %>
                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    
<% End Sub), _
.SelectorDropZoneTemplate = New Template(Sub() %>
                               
                                    <div>Drag and drop files here.</div>                
                                    
<% End Sub), _
.FileItemTemplate = New Template(Sub() %>
                               
                                    <div class="filedata">
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileName } ) %>
                                        &ndash;
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileSize, .Template = New Template("(calculating)") } ) %>
                                    </div>
                                        <% Html.KrystalwareWebForms(New FileListRemoveCommand() With { .HtmlAttributes = New With { .href = "javascript:;"}, .Template = New Template("[x]") } ) %>
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.ValidationMessage, .HtmlAttributes = New With { .style = "color:#f00"} } ) %>
                                    
<% End Sub), _
.ProgressTemplate = New Template(Sub() %>
                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.FileCount } ) %> file(s),
                                                <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.ContentLengthText, .Template = New Template("(calculating)") } ) %>.
                                            </div>
                                            <div>
                                                Currently uploading: <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.CurrentFileName } ) %>
                                                file <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.CurrentFileIndex } ) %>
                                                of <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.FileCount } ) %>.
                                            </div>
                                            <div>
                                                Speed: <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.SpeedText, .Template = New Template("(calculating)") } ) %>
                                            </div>
                                            <div>
                                                <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.TimeRemainingText, .Template = New Template("(calculating)") } ) %>
                                            </div>
                                            <div class="progressBarContainer">
                                                <% Html.KrystalwareWebForms(New UploadProgressBar()) %>
                                                <div class="progressBarText">
                                                    <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.PercentCompleteText, .Template = New Template("(calculating)") } ) %>
                                                </div>
                                            </div>
                                        </div>
                                    
<% End Sub) _
 } ) %>
<% Html.KrystalwareWebForms(New KrystalwareScriptRenderer()) %>                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <a id="uploadButton" href="javascript:;" onclick="startUpload()" class="button disabled" >
 <span><b class="icon upload"></b> Upload Files</span>        </a>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </div>
    <% End Using %>
    




<% If Model.Exception Is Nothing Then %><% If Model.ExistingFiles.Count > 0 Then %>        <h2>Existing Files</h2>
        <table class="results" width="99%" cellpadding="4" cellspacing="0">
            <thead>
                <th align="left">Name</th>
                <th align="left">Category</th>
                <th align="left">Length (bytes)</th>
            </thead>
            <tbody>
            <% For Each file As AspNetMvc3Vb.Storage.SqlFile In Model.ExistingFiles %>
                <tr>
                    <td>                        
                        <%:Html.ActionLink(file.Name, "Download", New With { .id = file.Id }, New With { .target = "_blank" }) %>
                    </td>
                    <td>
                        <%:file.Category %>
                    </td>
                    <td>
                        <%:file.Length %>
                    </td>
                </tr>
            <% Next file %>
            </tbody>
        </table>
        <br />
<% End If %><% Else %>    <div style="border:1px solid #c00;padding:.5em;margin-bottom:1em;">
        <b style="color:#f00">ERROR:</b> Could not connect to database. Please ensure the connection string and table information in the web.config are correct and that the file table has been properly created. Exception:<br /> <br /><%:Model.Exception.GetType().FullName %>: <%:Model.Exception.Message %>
    </div>
<% End If %>
</asp:Content>

<asp:Content ContentPlaceHolderID="configuration" runat="server">
    <h2>Configuration</h2>
<p>Configuration for the SQL sample involves creating the file database and table and configuring SlickUpload
   to point at that database. To prepare the database, perform the following steps:</p>
<ol>
    <li>Create a new database, or select an existing database to use</li>
    <li>Change the su connection string in the &lt;connectionStrings&gt; web.config section to point to the database selected above</li>
</ol>
<p>SlickUpload supports writing to one of 3 file column data types: IMAGE, VARBINARY(MAX), and FILESTREAM. Each has different configuration steps:</p>
<h3>IMAGE</h3>
<ol>
    <li>Open the SqlFileTable-IMAGE-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named "customSqlServer", and change the dataType attribute to "IMAGE"</li>
</ol>
<h3>VARBINARY(MAX)</h3>
<ol>
    <li>Open the SqlFileTable-VARBINARYMAX-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named "customSqlServer", and change the dataType attribute to "VarBinaryMax"</li>
</ol>
<h3>FILESTREAM</h3>
<ol>
    <li>Modify the su connection string in the &lt;connectionStrings&gt; web.config section to use integrated security</li>
    <li>Ensure the database selected above has been enabled for FILESTREAM access. For more information, see <a href="http://msdn.microsoft.com/en-us/library/cc645585.aspx">How to: Create a FILESTREAM-Enabled Database</a></li>
    <li>Open the SqlFileTable-FILESTREAM-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above. Make sure to update the script based on the name of the FILESTREAM FileGroup you have created.</li>
    <li>Open the web.config, locate the uploadProfile named "customSqlServer", and change the dataType attribute to "FileStream"</li>
</ol>
</asp:Content>
<asp:Content ContentPlaceHolderID="description" runat="server">

    Demonstrates writing additional fields to SQL as a file is created.
</asp:Content>
