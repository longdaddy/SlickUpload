<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetWebFormsCs.Slick.SlickDefault" EnableViewState="false" Title="Slick" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">


    <style type="text/css">
        #uploadContainer
        {
            display:block;border:2px dashed #BBD8FB;height:400px;position:relative;margin-bottom:1em;overflow:hidden;
        }
             
        #uploadContainer.kw-dragging
        {
            border-color:#666;
            background-color:#fafafa;
        }
             
        #uploadContainer.kw-dragging.kw-dragover
        {
            background-color:#f0f0f0;
        }

        #uploadContainer .su-filelist
        {
            overflow:auto;padding:3px;display:none;
            position:absolute;top:24px;bottom:22px;left:0px;right:0px;
        }
        
        #uploadContainer .su-filelistitem
        {
            float:left;padding:5px;margin-right:10px;
        }
        
        .su-progressbar
        {
            margin-left:30px;margin-right:40px;background-color:#008800;height:3px;width:0px;            
        }

        #uploadTopContainer
        {
            background-color:#EEF5FE;height:18px;padding:3px;
            position:absolute;top:0;left:0px;right:0px;
            width:99%;
        }
        
        #completeContainer
        {
            float:right;display:none            
        }

        #completingIndicator
        {
            background-image:url(/content/ajax-loader.gif);
            background-position:0 center;
            padding-left:20px;
            background-repeat:no-repeat;
        }

        #uploadContainer.initial #selectorContainer
        {
            margin-top:140px;text-align:center;font-size:20px;font-family: 'Segoe UI Light', 'Segoe UI' ,Helvetica,Arial,Sans-Serif;
        }
            
        #uploadContainer.initial  #dropInstructions
        {
            margin-bottom:6px
        }
            
        #uploadContainer.small #selectorContainer
        {
            height:18px;
            padding:2px 4px;
            text-align:center;
            background:#f3f3f3;
            position:absolute;bottom:0;left:0px;right:0px;
        }
            
        #uploadContainer.small #dropInstructions
        {
            float:left;
        }                  

        #uploadContainer.small .su-fileselector, #uploadContainer.small .su-folderselector
        {
            float:right;
            margin-right:3px;
        }        
                                   
        .su-removecommand
        {
            cursor:pointer;
            width:16px;
            height:16px;
            margin-left:4px;
            background-image:url(/content/button-icons.png);
            background-position: -80px 50%;
            text-indent:-10000px;
            background-position:center top;
            background-repeat:no-repeat;
            margin-top:3px;    
            display:-moz-inline-stack;
            display:inline-block;
            zoom:1;
            *display:inline;
        }
            
        a:hover, .su-hover a, input[type=file]
        {
            text-decoration:underline;
        }
        
        .su-fileselector a { color:Blue }                    
    </style>

<% if (uploadPanel.Visible) { %>        <kw:KrystalwareScriptRenderer ScriptInclude="true" runat="server"/>
        <script type="text/javascript">
        function onFileCountChanged(data)
        {
            var files = kw("<%=fileSelector.ClientID %>").get_Files();

            var hasFiles = (files.length > 0);

            document.getElementById("<%=fileList.ClientID %>").style.display = (hasFiles > 0 ? "block" : "none");
            document.getElementById("uploadContainer").className = (hasFiles > 0 ? "small" : "initial");
            /*document.getElementById("uploadButton").className = "button" + (hasFiles ? "" : " disabled");*/

            var fileSelectorText = document.getElementById("fileSelectorText");

            if (fileSelectorText)
                fileSelectorText.innerHTML = (hasFiles > 0 ? "Select more files." : "Select files from your computer.");

            var folderSelectorText = document.getElementById("folderSelectorText");

            if (folderSelectorText)
                folderSelectorText.innerHTML = (hasFiles > 0 ? "Select another folder." : "Select a folder from your computer.");

            updateFileSize();

            var fileCountText;

            fileCountText = files.length + " file";

            if (files.length == 0 || files.length > 1)
                fileCountText += "s";

            document.getElementById("fileCount").innerHTML = fileCountText;
        }

        function updateFileSize(data)
        {
            var files = kw("<%=fileSelector.ClientID %>").get_Files();
            var totalFileSize = 0;

            for (var i = 0; i < files.length; i++)
            {
                var size = files[i].get_Size();

                if (size)
                    totalFileSize += size;
            }

            var sizeString = kw.defaultFileSizeFormatter(totalFileSize);

            document.getElementById("fileSize").innerHTML = sizeString ? sizeString : "0 KB";
        }

        function onFileStart(file)
        {
            var fileEl = kw("<%=fileList.ClientID %>").getItemElementById(file.get_Id());
            var listEl = document.getElementById("<%=fileList.ClientID %>");

            listEl.scrollTop = fileEl.offsetTop - fileEl.offsetHeight;

            document.getElementById("completeContainer").style.display = "block";
        }

        function completeUpload()
        {
            document.getElementById("completeLink").style.display = "none";
            document.getElementById("completingIndicator").style.display = "block";
            document.getElementById("selectorContainer").style.display = "none";
            document.getElementById("<%=fileList.ClientID %>").style.bottom = "0px";

            kw("<%=slickUpload.ClientID %>").complete();
        }

        kw(function ()
        {
            if (kw.support.dragDrop)
                document.getElementById("dropInstructions").style.display = "block";

            kw("<%=slickUpload.ClientID %>").start();
        });        
        </script>
<% } %></asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

        <form id="uploadForm" runat="server">

        
        <asp:Panel ID="uploadPanel" runat="server">
            

    <div id="uploadContainer" class="initial">
        <div id="uploadTopContainer">
            <div id="completeContainer"><a href="javascript:completeUpload()" id="completeLink">Complete Upload</a><div id="completingIndicator" style="display:none">Completing Upload...</div></div>
            <span id="fileCount">0 files</span> (<span id="fileSize">0 KB</span>)        
        </div>
            <kw:FileList Id="fileList" FileSelectorId="fileSelector" runat="server"><ItemTemplate>                               
                                    <kw:FileListElement Element="FileName" runat="server" style="white-space:nowrap"/>
                                    <div style="white-space:nowrap;margin-left:30px">
                                        <kw:FileListElement Element="FileSize" runat="server">(calculating)</kw:FileListElement>
                                        <kw:FileListRemoveCommand runat="server">[x]</kw:FileListRemoveCommand>
                                    </div>
                                    <div style="padding-right:20px;*width:150px;">
                                        <kw:UploadProgressBar runat="server"/>
                                    </div>
                                </ItemTemplate>
</kw:FileList>            
        <div id="selectorContainer">
            <div id="dropInstructions" style="display:none">Drop files here.</div>
            <kw:FileSelector Id="fileSelector" UploadConnectorId="slickUpload" DropZoneId="uploadContainer" OnClientFileAdded="onFileCountChanged" OnClientFileRemoved="onFileCountChanged" OnClientFileValidated="updateFileSize" runat="server"><Template>                               
                                    <a href="javascript:;" style="margin-bottom:6px"><span id="fileSelectorText">Select files from your computer.</span></a>
                                </Template>
<FolderTemplate>                               
                                    <a href="javascript:;"><span id="folderSelectorText">Select a folder from your computer.</span></a>
                               </FolderTemplate>
</kw:FileSelector>            
        </div>
    </div>

    

    <kw:UploadConnector Id="slickUpload" AutoCompleteAfterLastFile="false" AutoUploadOnSubmit="false" OnUploadComplete="slickUpload_UploadComplete" UploadProfile="slick" OnClientUploadFileStarted="onFileStart" runat="server"/>


        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
             <% if (slickUpload.UploadSession != null) { %>
                <p>Result: <%=slickUpload.UploadSession.State.ToString() %></p>
                <% if (slickUpload.UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%=slickUpload.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultsRepeater" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#((UploadedFile)Container.DataItem).ServerLocation.Replace("\\", "\\<wbr />") %></td>
                                    <td><%#((UploadedFile)Container.DataItem).ContentType %></td>
                                    
                                    <td><%#((UploadedFile)Container.DataItem).ContentLength %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (slickUpload.UploadSession.UploadedFiles.Count == 0) { %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <% } else { %>
                <p>Error Summary: <%=slickUpload.UploadSession.ErrorSummary %></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>
        <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button"   onclick="newUploadButton_Click">
 <span><b class="icon newupload"></b> New Upload</span>        </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
                          
        </asp:Panel>
        </form>

    


</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    A fully skinned implementation of SlickUpload with Gmail style async file upload, multiple file selection, folder selection, and drag and drop.
</asp:Content>
