using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;

namespace AspNetWebFormsCs.PostProcess
{
    public partial class PostProcessDefault : System.Web.UI.Page
    {
        
        protected void slickUpload_UploadComplete(object sender, UploadSessionEventArgs e)
        {
            uploadResultPanel.Visible = true;
            uploadPanel.Visible = false;

            if (e.UploadSession != null && e.UploadSession.State == UploadState.Complete)
            {
                if (e.UploadSession.UploadedFiles.Count > 0)
                {
                    
                    // Simulate post processing
                    for (int i = 0; i <= 100; i++)
                    {
                        e.UploadSession.ProcessingStatus["percentComplete"] = i.ToString();
                        e.UploadSession.ProcessingStatus["percentCompleteText"] = i.ToString() + "%";

                        SlickUploadContext.UpdateSession(e.UploadSession);

                        System.Threading.Thread.Sleep(100);
                    }


                    resultsRepeater.DataSource = e.UploadSession.UploadedFiles;
                    resultsRepeater.DataBind();
                }
            }            
        }

        protected void newUploadButton_Click(object sender, EventArgs e)
        {
            uploadResultPanel.Visible = false;
            uploadPanel.Visible = true;
        }        
        

        

    }
}
