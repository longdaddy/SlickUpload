using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Krystalware.SlickUpload;
using AspNetMvc2Cs.Models;

namespace AspNetMvc2Cs.Controllers
{
    public class ValidationController : Controller
    {
        
        public ActionResult Index(ValidationModel model)
        {
            if (model == null)
                model = new ValidationModel();

            

            return View(model);
        }

        public ActionResult UploadResult(ValidationModel model, UploadSession session)
        {
            

            model.UploadSession = session;

            return View(model);
        }
        

        
    }
}
