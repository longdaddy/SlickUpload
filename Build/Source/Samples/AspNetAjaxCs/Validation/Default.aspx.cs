using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;

namespace AspNetAjaxCs.Validation
{
    public partial class ValidationDefault : System.Web.UI.Page
    {
                
        protected void updateButton_Click(object sender, EventArgs e)
        {
            updateLabel.Text = DateTime.Now.ToLongTimeString();
        }
                
        
        protected void slickUpload_UploadComplete(object sender, UploadSessionEventArgs e)
        {
            uploadResultPanel.Visible = true;
            uploadPanel.Visible = false;

            if (e.UploadSession != null && e.UploadSession.State == UploadState.Complete)
            {
                if (e.UploadSession.UploadedFiles.Count > 0)
                {
                    
                    resultsRepeater.DataSource = e.UploadSession.UploadedFiles;
                    resultsRepeater.DataBind();
                }
            }            
        }

        protected void newUploadButton_Click(object sender, EventArgs e)
        {
            uploadResultPanel.Visible = false;
            uploadPanel.Visible = true;
        }        
        

        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            int maxFiles;
            int maxFileSize;

            if (int.TryParse(maxFilesTextBox.Text, out maxFiles))
            {
                slickUpload.MaxFiles = maxFiles;
            }

            if (int.TryParse(maxFileSizeTextBox.Text, out maxFileSize))
            {
                slickUpload.MaxFileSize = maxFileSize;
            }

            slickUpload.ConfirmNavigateDuringUploadMessage = confirmNavigateMessageTextBox.Text;
            slickUpload.ValidExtensions = validExtensionsTextBox.Text;
            slickUpload.InvalidExtensionMessage = fileTypeMessageTextBox.Text;
            slickUpload.InvalidFileSizeMessage = fileSizeMessageTextBox.Text;

            requiredFilesValidator.Enabled = requireFileSelectionCheckBox.Checked;

            summaryValidator.Text = summaryMessageTextBox.Text;
            summaryValidator.Enabled = !allowInvalidUploadCheckBox.Checked;
        }


        
        protected override void Render(HtmlTextWriter writer)
        {
            // Ensure ASP.NET AJAX controls initialize and render properly
            
            ClientScript.RegisterForEventValidation(newUploadButton.UniqueID);

            base.Render(writer);
        }
        
    }
}
