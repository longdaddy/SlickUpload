using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;

namespace AspNetAjaxCs.SqlServer
{
    public partial class SqlServerDefault : System.Web.UI.Page
    {
                
        protected void updateButton_Click(object sender, EventArgs e)
        {
            updateLabel.Text = DateTime.Now.ToLongTimeString();
        }
                
        
        protected void slickUpload_UploadComplete(object sender, UploadSessionEventArgs e)
        {
            uploadResultPanel.Visible = true;
            uploadPanel.Visible = false;

            if (e.UploadSession != null && e.UploadSession.State == UploadState.Complete)
            {
                if (e.UploadSession.UploadedFiles.Count > 0)
                {
                    
                    resultsRepeater.DataSource = e.UploadSession.UploadedFiles;
                    resultsRepeater.DataBind();
                }
            }            
        }

        protected void newUploadButton_Click(object sender, EventArgs e)
        {
            uploadResultPanel.Visible = false;
            uploadPanel.Visible = true;
        }        
        

        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                IList<AspNetAjaxCs.Storage.SqlFile> files = new AspNetAjaxCs.Storage.SqlFileRepository("sqlServer").GetAll();

                if (files.Count > 0)
                {
                    filesRepeater.DataSource = files;
                    filesRepeater.DataBind();

                    existingFilesPanel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage.InnerHtml += ex.GetType().FullName + ": " + ex.Message;
                errorMessage.Visible = true;

                uploadPanel.Visible = false;
                uploadResultPanel.Visible = false;
            }
        }


        
        protected override void Render(HtmlTextWriter writer)
        {
            // Ensure ASP.NET AJAX controls initialize and render properly
            
            ClientScript.RegisterForEventValidation(newUploadButton.UniqueID);

            base.Render(writer);
        }
        
    }
}
