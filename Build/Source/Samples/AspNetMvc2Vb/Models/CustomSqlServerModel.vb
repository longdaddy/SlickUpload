Imports Krystalware.SlickUpload

Imports AspNetMvc2Vb.Storage

Namespace Models
    Public Class CustomSqlServerModel
        Public Property ExistingFiles As List(Of SqlFile)
        Public Property Exception As Exception
        Public Property UploadSession As UploadSession
    End Class
End Namespace