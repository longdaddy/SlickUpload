Imports Krystalware.SlickUpload
Imports AspNetMvc2Vb.Models

Public Class ValidationController
    Inherits System.Web.Mvc.Controller

    
    Public Function Index(model As ValidationModel) As ActionResult
        If model Is Nothing Then
            model = New ValidationModel()
        End If

        

        Return View(model)
    End Function

    Public Function UploadResult(model As ValidationModel, session As UploadSession) As ActionResult
        

        model.UploadSession = session

        Return View(model)
    End Function
    

        
End Class
