<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetWebFormsCs.QuickStartDefault" EnableViewState="false" Title="SlickUpload Quick Start Example" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
    function cancelUpload()
    {
        kw("<%=slickUpload.ClientID %>").cancel();
    }

    function onSessionStarted(data)
    {
        document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";
        document.getElementById("<%=cancelButton.ClientID %>").style.display = "block";
    }
        
    function onBeforeSessionEnd(data)
    {
        document.getElementById("<%=cancelButton.ClientID %>").style.display = "none";
    }
</script>

<style type="text/css">
    /* hide initially  */
    .su-fileselector, .su-folderselector, .su-filelisttemplate, .su-uploadprogressdisplay
    {
        display: none;
        zoom:1;
    }
    
    /* fileselector cursor, link color */
    .su-fileselector, .su-fileselector *, .su-folderselector, .su-folderselector *
    {
        color:blue;
        cursor:pointer;
    }

    /* hover links */
    a
    {
        text-decoration:none
    }
    
    a:hover, .su-hover a
    {
        text-decoration:underline;
    }
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

        <form id="uploadForm" runat="server">


                       <kw:SlickUpload Id="slickUpload" FileSelectorStyle="float:left;padding-right:1em" FileListStyle="clear:both" UploadProgressDisplayStyle="clear:both" Style="overflow: hidden; zoom: 1" UploadProfile="quickStart" OnClientUploadSessionStarted="onSessionStarted" OnClientBeforeSessionEnd="onBeforeSessionEnd" OnUploadComplete="slickUpload_UploadComplete" runat="server"><SelectorTemplate>                          
                                    <a href="javascript:;">Add files</a>
                                    </SelectorTemplate>
<SelectorFolderTemplate>                          
                                    <a href="javascript:;">Add folder</a>
                                    </SelectorFolderTemplate>
<FileItemTemplate>                          
                                        <kw:FileListElement Element="FileName" runat="server"/>
                                        &ndash;
                                        <kw:FileListElement Element="FileSize" runat="server">(calculating)</kw:FileListElement>
                                        <kw:FileListRemoveCommand runat="server" href="javascript:;">[x]</kw:FileListRemoveCommand>
                                        <kw:FileListElement Element="ValidationMessage" runat="server" style="color:#f00"/>
                                    </FileItemTemplate>
<ProgressTemplate>                          
                                        <div>
                                            Uploading <kw:UploadProgressElement Element="FileCount" runat="server"/> file(s),
                                            <kw:UploadProgressElement Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                                        </div>
                                        <div>
                                            Currently uploading: <kw:UploadProgressElement Element="CurrentFileName" runat="server"/>
                                            file <kw:UploadProgressElement Element="CurrentFileIndex" runat="server"/>
                                            of <kw:UploadProgressElement Element="FileCount" runat="server"/>.
                                        </div>
                                        <div>
                                            Speed: <kw:UploadProgressElement Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>
                                        </div>
                                        <div>
                                            <kw:UploadProgressElement Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement>
                                        </div>
                                        <div style="border:1px solid #008800; height: 1.5em; position: relative;">
                                            <kw:UploadProgressBar runat="server" style="background-color:#00ee00;width:0; height:1.5em;"/>
                                            <div style="text-align: center; position: absolute; top: .15em; width: 100%;">
                                                <kw:UploadProgressElement Element="PercentCompleteText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                        </div>
                                    </ProgressTemplate>
</kw:SlickUpload>
                            <hr />
                            <p>
                                <asp:Button Text="Upload" id="uploadButton" runat="server" />
                                <asp:Button Text="Cancel" id="cancelButton" runat="server" style="display:none" onclientclick="cancelUpload();return false;" />
                            </p>
                            <p><asp:Label Id="uploadResult" runat="server" /></p>
                            <asp:Repeater id="uploadFileList" runat="server" enableviewstate="false">
                                <HeaderTemplate><ul></HeaderTemplate>
                                <ItemTemplate><li><%# DataBinder.Eval(Container.DataItem, "ClientName")%></li></ItemTemplate> 
                                <FooterTemplate></ul></FooterTemplate>
                            </asp:Repeater>

        </form>

</asp:Content>
