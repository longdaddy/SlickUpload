using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Krystalware.SlickUpload;

namespace AspNetMvc2Cs.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(UploadSession session)
        {
            return View(session);
        }
    }
}
