Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Web.Controls

Public Class QuickStartDefault
    Inherits System.Web.UI.Page

    Protected Sub slickUpload_UploadComplete(ByVal sender As Object, ByVal e As UploadSessionEventArgs) Handles slickUpload.UploadComplete
        uploadResult.Text = "Upload Result: " + e.UploadSession.State.ToString() 

        If e.UploadSession.State = UploadState.Error Then 
            uploadResult.Text += "<br /><br /> Message: " + e.UploadSession.ErrorSummary
        End If 

        If e.UploadSession.State = UploadState.Complete Then 
            uploadFileList.DataSource = e.UploadSession.UploadedFiles 
            uploadFileList.DataBind() 
        End If
    End Sub
End Class
