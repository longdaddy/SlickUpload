/*
 * Assumes a FILESTREAM group named [FileStreamGroup]. If yours is named differently, do a search and replace to update based on your name.
 *
 * For more information, see: http://msdn.microsoft.com/en-us/library/cc645585.aspx
 */
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SlickUploadFileCustom]') AND type in (N'U')) DROP TABLE SlickUploadFileCustom

GO

CREATE TABLE [dbo].[SlickUploadFileCustom](
	[FileId] [int] IDENTITY(1,1) NOT NULL,
	[DataId] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[Data] [varbinary](max) FILESTREAM  NULL,
	[FileName] [varchar](255) NULL,
	[Category] [varchar](128) COLLATE Latin1_General_CI_AI NULL,
 CONSTRAINT [PK_SlickUploadFileCustom] PRIMARY KEY CLUSTERED 
(
	[FileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] FILESTREAM_ON [FileStreamGroup],
UNIQUE NONCLUSTERED 
(
	[DataId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] FILESTREAM_ON [FileStreamGroup]

GO

ALTER TABLE [dbo].[SlickUploadFileCustom] ADD  DEFAULT (newid()) FOR [DataId]
GO
