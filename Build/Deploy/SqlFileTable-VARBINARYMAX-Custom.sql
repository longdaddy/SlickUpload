IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SlickUploadFileCustom]') AND type in (N'U'))
DROP TABLE [dbo].[SlickUploadFileCustom]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SlickUploadFileCustom](
	[FileId] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](128) COLLATE Latin1_General_CI_AI NULL,
	[Category] [varchar](128) COLLATE Latin1_General_CI_AI NULL,
	[Data] varbinary(MAX) NULL,
 CONSTRAINT [PK_SlickUploadFileCustom] PRIMARY KEY CLUSTERED 
(
	[FileId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF