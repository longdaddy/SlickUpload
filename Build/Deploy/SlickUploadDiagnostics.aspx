﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System.Net" %>
<script type="text/C#" runat="server">
    bool correctNetVersion = (System.Environment.Version.Major > 2 || (System.Environment.Version.Major == 2 && System.Environment.Version.Build >= 50727));

    bool isTraceOff = (HttpContext.Current.Trace == null || !HttpContext.Current.Trace.IsEnabled);
        
    static Assembly slickUploadAssembly = Assembly.Load("Krystalware.SlickUpload");
    
    bool slickUploadInstalled = (slickUploadAssembly != null);
    
    bool correctSlickUploadVersion = slickUploadAssembly != null && (slickUploadAssembly.GetName().Version.Major >= 6);

    bool IsModuleInstalled()
    {
        HttpModuleCollection modules = HttpContext.Current.ApplicationInstance.Modules;

        foreach (string key in modules)
        {
            IHttpModule module = modules[key];

            if (module.GetType().Name == "SlickUploadModule")
                return true;
        }

        return false;
    }

    bool IsConfigSectionRegistered()
    {
        object section = ConfigurationManager.GetSection("slickUpload");

        return section != null && section.GetType().Name == "SlickUploadSection";
    }
    
    bool IsCorrectConfig()
    {
        return correctNetVersion && slickUploadInstalled && correctSlickUploadVersion && IsModuleInstalled() && IsConfigSectionRegistered() && isTraceOff && IsHandlerAccessible();
    }

    bool IsHandlerAccessible()
    {
        WebClient client = new WebClient();
        
        try
        {
            string value = client.DownloadString(new Uri(Request.Url, ResolveUrl("~/SlickUpload.axd?handlerType=diagnostic")));

            return value.IndexOf("accessible") != -1;
        }
        catch (Exception ex)
        {
            // TODO: show exception?
            return false;
        }
    }    
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SlickUpload Diagnostics</title>
    <style type="text/css">
        * {font-family:Calibri, Arial, Verdana, Sans-Serif}
        h1, h2, h3 {font-weight:normal}

        pre
        {
            font-family:Consolas, Courier New, Courier, fixed;
            white-space: -moz-pre-wrap; /* Mozilla, supported since 1999 */
            white-space: -pre-wrap; /* Opera */
            white-space: -o-pre-wrap; /* Opera */
            white-space: pre-wrap; /* CSS3 - Text module (Candidate Recommendation) http://www.w3.org/TR/css3-text/#white-space */
            word-wrap: break-word; /* IE 5.5+ */            
        }
        
        table {border-collapse:collapse;border:1px solid #c0c0c0}
        table td, table th {border:1px solid #c0c0c0}
        table th {background-color:#e0e0e0}
        .pass { font-weight:bold; color:#0a0 }
        .fail { font-weight:bold; color:#f00 }
    </style>
</head>
<body>
    <h1>SlickUpload Diagnostics</h1>
    <% if (IsCorrectConfig()) { %>
    <p>This project is configured properly for SlickUpload. If you are still having issues, post on the <a href="http://krystalware.com/forums/yaf_topics109_SlickUpload-Support.aspx">SlickUpload support forums</a>.</p>
    <% } else { %>
    <p>This project isn't properly configured for SlickUpload. Look below for suggestions on how to solve the issues. Items marked with <strong class="pass">OK</strong> are correctly configured. Items marked with <strong class="fail">FAIL</strong> are issues that need to be resolved.</p>
    <p>If you need some help, post on the <a href="http://krystalware.com/forums/yaf_topics109_SlickUpload-Support.aspx">SlickUpload support forums</a>.</p>
    <% } %>
    <h2>Configuration Details</h2>
    <p>Analyzing application at <%= new Uri(Request.Url, Request.ApplicationPath).ToString() %>. Physical location: <%=Server.MapPath("~/") %>.</p>
    <table width="100%" cellpadding="4" cellspacing="0">
        <thead>
            <tr>
                <th>Element</th>
                <th>Value</th>
                <th>Notes</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>.NET Framework version</td>
                <td><%=System.Environment.Version %>&nbsp;-&nbsp;<strong class="<%=correctNetVersion ? "pass" : "fail"%>"><%=correctNetVersion ? "OK" : "FAIL"%></strong></td>
                <td width="50%">At least .NET Framework v2 SP2 (v 2.0.50727.*) is required.
                    <% if (!correctNetVersion) { %>
                    Download <a href="http://www.microsoft.com/downloads/en/details.aspx?familyid=5b2c0358-915b-4eb5-9b1d-10e506da9d0f&displaylang=en">.NET Framework 2.0 SP2</a> to get up to date.
                    <% } %>
                </td>
            </tr>
            <tr>
                <td>SlickUpload assembly installed?</td>
                <td><%=slickUploadInstalled ? "Yes" : "No"%> - <strong class="<%=slickUploadInstalled ? "pass" : "fail"%>"><%=slickUploadInstalled ? "OK" : "FAIL"%></strong></td>
                <td>
                    <% if (!slickUploadInstalled) { %>
                        The SlickUpload assembly is named Krystalware.SlickUpload.dll. Add a reference to that file in your project, or copy it to your ~/bin folder.
                    <% } %>
                </td>
            </tr>
            <tr>
                <td>SlickUpload assembly version</td>
                <td><%=slickUploadAssembly.GetName().Version %> - <strong class="<%=correctSlickUploadVersion ? "pass" : "fail"%>"><%=correctSlickUploadVersion ? "OK" : "FAIL"%></strong></td>
                <td>
                    This diagnostic page applies to SlickUpload 6.
                    <% if (!correctSlickUploadVersion) { %>
                        Download the latest version of SlickUpload from http://krystalware.com/Products/SlickUpload/Download.aspx. <%--Include install instructions link ?--%>
                    <% } %>
                </td>
            </tr>
            <tr>
                <td>ASP.NET pipeline mode</td>
                <td>
                    <% if (HttpRuntime.UsingIntegratedPipeline) { %>
                        IIS 7+ Integrated
                    <% } else { %>
                        Classic
                    <% } %>
                    - <strong class="pass">OK</strong>            
                </td>
                <td>
                    <p>SlickUpload works with both integrated and classic pipelines. The only difference is where you need to add SlickUpload's HttpModule in your web.config.</p>
                    <ul>
                        <li>Integrated mode: system.webServer/modules key</li>
                        <li>Classic mode: system.web/httpModules key</li>
                    </ul>
                    <p>If you want to be able to run it under both modes, you can add the module to both keys.</p>
                </td>
            </tr>
            <tr>
                <td>SlickUpload HttpModule installed?</td>
                <td><%=IsModuleInstalled() ? "Yes" : "No"%> - <strong class="<%=IsModuleInstalled() ? "pass" : "fail"%>"><%=IsModuleInstalled() ? "OK" : "FAIL"%></strong></td>
                <td>
                    <p>To install the SlickUpload HttpModule, follow the instructions below for your current ASP.NET pipeline mode (see above). If you want to support both modes, follow both sets of instructions.</p>

                    <ul>
                        <li><p>Integrated mode: Add the following section to your web.config in the system.webServer/modules key:</p>
<pre>
&lt;system.webServer&gt;
    ...
    &lt;modules&gt;
        ...
        &lt;add name="SlickUploadModule" type="Krystalware.SlickUpload.Web.SlickUploadModule, Krystalware.SlickUpload"
             preCondition="integratedMode" /&gt;
</pre>
                        </li>
                        <li><p>Classic mode: Add the following section to your web.config in the system.web/httpModules key:</p>
<pre>
&lt;system.web&gt;
    ...
    &lt;httpModules&gt;
        ...
        &lt;add name="SlickUploadModule" type="Krystalware.SlickUpload.Web.SlickUploadModule, Krystalware.SlickUpload" /&gt;
</pre>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>SlickUpload handler installed and accessible<br />(checking <%=new Uri(Request.Url, ResolveUrl("~/SlickUpload.axd?handlerType=diagnostic")).ToString()%>)</td>
                <td><%=IsHandlerAccessible() ? "Yes" : "No"%> - <strong class="<%=IsHandlerAccessible() ? "pass" : "fail"%>"><%=IsHandlerAccessible() ? "OK" : "FAIL"%></strong></td>
                <td>
                    <p>SlickUpload automatically registers its handler, but some things can override this registration. Automatic registration is also not supported for pre SP2 versions of .NET 2.
                    If the automatic registration isn't working, you can manually register the handler in web.config. To do this, follow the instructions below for your current ASP.NET pipeline mode (see above). If you want to support both modes, follow both sets of instructions.</p>
                    <ul>
                        <li><p>Integrated mode: Add the following section to your web.config in the system.webServer/handlers key:</p>
<pre>
&lt;system.webServer&gt;
    ...
    &lt;handlers&gt;
        ...
        &lt;add name="SlickUploadHandler"
             path="SlickUpload.axd" verb="GET,POST" type="Krystalware.SlickUpload.Web.SlickUploadHandlerFactory, Krystalware.SlickUpload"
             preCondition="integratedMode" /&gt;
</pre>
                        </li>
                        <li><p>Classic mode: Add the following section to your web.config in the system.web/httpHandlers key:</p>
<pre>
&lt;system.web&gt;
    ...
    &lt;httpHandlers&gt;
        ...
        &lt;add path="SlickUpload.axd" verb="GET,POST" type="Krystalware.SlickUpload.Web.SlickUploadHandlerFactory, Krystalware.SlickUpload" /&gt;
</pre>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>SlickUpload config section registered?</td>
                <td><%=IsConfigSectionRegistered() ? "Yes" : "No"%> - <strong class="<%=IsConfigSectionRegistered() ? "pass" : "fail"%>"><%=IsConfigSectionRegistered() ? "OK" : "FAIL"%></strong></td>
                <td>
                    <p>To register the SlickUpload configuration section, add the following section at the top of your web.config in the configSections key:</p>
<pre>
&lt;configuration&gt;
    &lt;configSections&gt;
        ...
        &lt;section name="slickUpload" type="Krystalware.SlickUpload.Configuration.SlickUploadSection, Krystalware.SlickUpload" requirePermission="false" /&gt;
</pre>
                </td>
            </tr>
            <tr>
                <td>ASP.NET tracing</td>
                <td><%=isTraceOff ? "Off" : "On"%> - <strong class="<%=isTraceOff ? "pass" : "fail"%>"><%=isTraceOff ? "OK" : "FAIL"%></strong></td>
                <td>
                    <p>SlickUpload requires ASP.NET application wide tracing to be off to intercept the upload request in order to provide progress information.
                    You can turn trace off by adding the following section under the system.web key in your web.config:</p>
                    <pre>&lt;trace enabled="false" /&gt;</pre>
                    <p>You can still use per page tracing (configured in the &lt;%@ Page %&gt; directive).</p>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
