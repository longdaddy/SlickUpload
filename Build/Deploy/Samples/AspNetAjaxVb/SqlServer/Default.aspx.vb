Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Web.Controls

Namespace SqlServer
    Public Class SqlServerDefault
        Inherits System.Web.UI.Page

                
        Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
            updateLabel.Text = DateTime.Now.ToLongTimeString()
        End Sub
                
        
        Protected Sub slickUpload_UploadComplete(ByVal sender As Object, ByVal e As UploadSessionEventArgs) Handles slickUpload.UploadComplete
            uploadResultPanel.Visible = True
            uploadPanel.Visible = False

            If Not e.UploadSession Is Nothing AndAlso e.UploadSession.State = UploadState.Complete Then
                If e.UploadSession.UploadedFiles.Count > 0 Then
                    
                    resultsRepeater.DataSource = e.UploadSession.UploadedFiles
                    resultsRepeater.DataBind()
                End If
            End If
        End Sub

        Protected Sub newUploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newUploadButton.Click
            uploadResultPanel.Visible = False
            uploadPanel.Visible = True
        End Sub
        

        
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim files As IList(Of AspNetAjaxVb.Storage.SqlFile) = New AspNetAjaxVb.Storage.SqlFileRepository("sqlServer").GetAll()

                If files.Count > 0 Then
                    filesRepeater.DataSource = files
                    filesRepeater.DataBind()

                    existingFilesPanel.Visible = true
                End If
            Catch ex As Exception
                errorMessage.InnerHtml += ex.GetType().FullName + ": " + ex.Message
                errorMessage.Visible = true

                uploadPanel.Visible = false
                uploadResultPanel.Visible = false
            End Try
        End Sub


        
        Protected Overrides Sub Render(writer As HtmlTextWriter)
            'Ensure ASP.NET AJAX controls initialize and render properly
            
            ClientScript.RegisterForEventValidation(newUploadButton.UniqueID)

            MyBase.Render(writer)
        End Sub
        
    End Class
End Namespace
