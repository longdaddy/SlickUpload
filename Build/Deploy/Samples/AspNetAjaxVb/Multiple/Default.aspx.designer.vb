'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace Multiple 
    Partial Public Class MultipleDefault

        '''<summary>
        '''uploadForm control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents uploadForm As Global.System.Web.UI.HtmlControls.HtmlForm

        '''<summary>
        '''uploadPanel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents uploadPanel As Global.System.Web.UI.WebControls.Panel

        '''<summary>
        '''slickUpload control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents slickUpload As Global.Krystalware.SlickUpload.Web.Controls.UploadConnector

        '''<summary>
        '''uploadButton control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents uploadButton As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''uploadResultPanel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents uploadResultPanel As Global.System.Web.UI.WebControls.Panel

        '''<summary>
        '''resultsRepeater control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents resultsRepeater As Global.System.Web.UI.WebControls.Repeater

        '''<summary>
        '''newUploadButton control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents newUploadButton As Global.System.Web.UI.WebControls.LinkButton

        
        '''<summary>
        '''scriptManager control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents scriptManager As Global.System.Web.UI.ScriptManager

        '''<summary>
        '''updatePanel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents updatePanel As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''updateLabel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents updateLabel As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''updateButton control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents updateButton As Global.System.Web.UI.WebControls.Button
        

        
    End Class
End Namespace
