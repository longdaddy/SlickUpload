Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Web.Controls

Namespace ClientApi
    Public Class ClientApiDefault
        Inherits System.Web.UI.Page


    Protected ReadOnly Property UploadSession As UploadSession
        Get
            Return SlickUploadContext.CurrentUploadSession
        End Get
    End Property

    Protected Sub uploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uploadButton.Click
        If Not UploadSession Is Nothing AndAlso UploadSession.State = UploadState.Complete Then
            If UploadSession.UploadedFiles.Count > 0 Then
                resultsRepeater.DataSource = UploadSession.UploadedFiles
                resultsRepeater.DataBind()
            End If
        End If
    End Sub

    Protected Sub newUploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newUploadButton.Click
        uploadResultPanel.Visible = False
        uploadPanel.Visible = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not UploadSession Is Nothing Then
            uploadResultPanel.Visible = true
            uploadPanel.Visible = false
        End If
    End Sub

        

    End Class
End Namespace
