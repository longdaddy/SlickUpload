Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Web.Controls

Namespace PostProcess
    Public Class PostProcessDefault
        Inherits System.Web.UI.Page

        
        Protected Sub slickUpload_UploadComplete(ByVal sender As Object, ByVal e As UploadSessionEventArgs) Handles slickUpload.UploadComplete
            uploadResultPanel.Visible = True
            uploadPanel.Visible = False

            If Not e.UploadSession Is Nothing AndAlso e.UploadSession.State = UploadState.Complete Then
                If e.UploadSession.UploadedFiles.Count > 0 Then
                    
                    'Simulate post processing
                    For i As Integer = 0 To 100
                        e.UploadSession.ProcessingStatus("percentComplete") = i.ToString()
                        e.UploadSession.ProcessingStatus("percentCompleteText") = i.ToString() + "%"

                        SlickUploadContext.UpdateSession(e.UploadSession)

                        System.Threading.Thread.Sleep(100)
                    Next i


                    resultsRepeater.DataSource = e.UploadSession.UploadedFiles
                    resultsRepeater.DataBind()
                End If
            End If
        End Sub

        Protected Sub newUploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newUploadButton.Click
            uploadResultPanel.Visible = False
            uploadPanel.Visible = True
        End Sub
        

        

    End Class
End Namespace
