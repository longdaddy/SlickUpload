<%@ Page Language="VB" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="AspNetWebFormsVb.CustomFileName.CustomFileNameDefault" EnableViewState="false" Title="Custom FileName" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function cancelUpload()
        {
            kw("<%=slickUpload.ClientID %>").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").className = "button" + (kw("<%=slickUpload.ClientID %>").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
    kw("<%=slickUpload.ClientID %>").set_Data("fileRoot", document.getElementById("fileCategory").value);

            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

        <form id="uploadForm" runat="server">

        
        <asp:Panel ID="uploadPanel" runat="server">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">File category</th>
                    </tr>
                    <tr>
                        <td>
                            <select id="fileCategory">
                                <option value="documents">Documents</option>
                                <option value="images">Images</option>
                                <option value="audio">Audio</option>
                                <option value="video">Video</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>The category selected above will be passed to the server with the upload (as a cookie), and used to specify the folder in which the upload files are stored.</p>

            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

                            <kw:SlickUpload Id="slickUpload" OnClientUploadSessionStarted="onSessionStarted" OnClientBeforeSessionEnd="onBeforeSessionEnd" OnClientFileAdded="onFileSelectionChanged" OnClientFileRemoved="onFileSelectionChanged" CssClass="simple" ShowDropZoneOnDocumentDragOver="true" Style="overflow:hidden;zoom:1" FileSelectorStyle="float:left" FileListStyle="clear:both" UploadProgressDisplayStyle="clear:both" UploadProfile="customFileName" runat="server"><SelectorTemplate>                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    </SelectorTemplate>
<SelectorFolderTemplate>                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    </SelectorFolderTemplate>
<SelectorDropZoneTemplate>                               
                                    <div>Drag and drop files here.</div>                
                                    </SelectorDropZoneTemplate>
<FileItemTemplate>                               
                                    <div class="filedata">
                                        <kw:FileListElement Element="FileName" runat="server"/>
                                        &ndash;
                                        <kw:FileListElement Element="FileSize" runat="server">(calculating)</kw:FileListElement>
                                    </div>
                                        <kw:FileListRemoveCommand runat="server" href="javascript:;">[x]</kw:FileListRemoveCommand>
                                        <kw:FileListElement Element="ValidationMessage" runat="server" style="color:#f00"/>
                                    </FileItemTemplate>
<ProgressTemplate>                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <kw:UploadProgressElement Element="FileCount" runat="server"/> file(s),
                                                <kw:UploadProgressElement Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                                            </div>
                                            <div>
                                                Currently uploading: <kw:UploadProgressElement Element="CurrentFileName" runat="server"/>
                                                file <kw:UploadProgressElement Element="CurrentFileIndex" runat="server"/>
                                                of <kw:UploadProgressElement Element="FileCount" runat="server"/>.
                                            </div>
                                            <div>
                                                Speed: <kw:UploadProgressElement Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                            <div>
                                                <kw:UploadProgressElement Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                            <div class="progressBarContainer">
                                                <kw:UploadProgressBar runat="server"/>
                                                <div class="progressBarText">
                                                    <kw:UploadProgressElement Element="PercentCompleteText" runat="server">(calculating)</kw:UploadProgressElement>
                                                </div>
                                            </div>
                                        </div>
                                    </ProgressTemplate>
</kw:SlickUpload>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <asp:LinkButton ID="uploadButton" runat="server" CssClass="button disabled"  CausesValidation="true" >
 <span><b class="icon upload"></b> Upload Files</span>        </asp:LinkButton>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
             <% If Not slickUpload.UploadSession Is Nothing Then %>
                <p>Result: <%=slickUpload.UploadSession.State.ToString() %></p>
                <% If slickUpload.UploadSession.State <> UploadState.Error Then %>
                <p>Files Uploaded: <%=slickUpload.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultsRepeater" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#DirectCast(Container.DataItem, UploadedFile).ServerLocation.Replace("\", "\<wbr />") %></td>
                                    <td><%#DirectCast(Container.DataItem, UploadedFile).ContentType %></td>
                                    
                                    <td><%#DirectCast(Container.DataItem, UploadedFile).ContentLength %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% If slickUpload.UploadSession.UploadedFiles.Count = 0 Then %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% End If %>
                    </tbody>
                </table>
                <% Else %>
                <p>Error Summary: <%=slickUpload.UploadSession.ErrorSummary %></p>
                <% End If %>
            <% Else %>
            <p>No upload recieved.</p>
            <% End If %>
            <p>
        <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button"   >
 <span><b class="icon newupload"></b> New Upload</span>        </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
                          
        </asp:Panel>
        </form>

    



</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    Demonstrates how to pass information to the server and generate server filenames for files as they are uploaded.
</asp:Content>
