Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Web.Controls

Namespace CustomSqlServer
    Public Class CustomSqlServerDefault
        Inherits System.Web.UI.Page

        
        Protected Sub slickUpload_UploadComplete(ByVal sender As Object, ByVal e As UploadSessionEventArgs) Handles slickUpload.UploadComplete
            uploadResultPanel.Visible = True
            uploadPanel.Visible = False

            If Not e.UploadSession Is Nothing AndAlso e.UploadSession.State = UploadState.Complete Then
                If e.UploadSession.UploadedFiles.Count > 0 Then
                    
                    resultsRepeater.DataSource = e.UploadSession.UploadedFiles
                    resultsRepeater.DataBind()
                End If
            End If
        End Sub

        Protected Sub newUploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newUploadButton.Click
            uploadResultPanel.Visible = False
            uploadPanel.Visible = True
        End Sub
        

        
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim files As IList(Of AspNetWebFormsVb.Storage.SqlFile) = New AspNetWebFormsVb.Storage.SqlFileRepository("customSqlServer").GetAll(true)

                If files.Count > 0 Then
                    filesRepeater.DataSource = files
                    filesRepeater.DataBind()

                    existingFilesPanel.Visible = true
                End If
            Catch ex As Exception
                errorMessage.InnerHtml += ex.GetType().FullName + ": " + ex.Message
                errorMessage.Visible = true

                uploadPanel.Visible = false
                uploadResultPanel.Visible = false
            End Try
        End Sub


    End Class
End Namespace
