<%@ Page Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage(Of Krystalware.SlickUpload.UploadSession)" EnableViewState="false" Title="Client Api" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>
<asp:Content ContentPlaceHolderID="content" runat="server">
            <h2>Upload Result</h2>
             <% If Not Model Is Nothing Then %>
                <p>Result: <%:Model.State.ToString() %></p>
                <% If Model.State <> UploadState.Error Then %>
                <p>Files Uploaded: <%:Model.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
<% For Each file As UploadedFile In Model.UploadedFiles %>                        <tr>
                            <td>
                                <%=file.ServerLocation.Replace("\", "\<wbr />") %>
                            </td>
                            <td>
                                <%:file.ContentType %>
                            </td>
                            
                            <td>
                                <%:file.ContentLength %>
                            </td>
                        </tr>
<% Next file %>                        <% If Model.UploadedFiles.Count = 0 Then %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% End If %>
                    </tbody>
                </table>
                <% Else %>
                <p>Error Summary: <%:Model.ErrorSummary %></p>
                <% End If %>
            <% Else %>
            <p>No upload recieved.</p>
            <% End If %>
            <p>
        <a id="newUploadButton"  href="<%:Url.Action("Index") %>" class="button" >
 <span><b class="icon newupload"></b> New Upload</span>        </a>
                <div style="clear:both"></div>
            </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="description" runat="server">
Use the SlickUpload client API to create an upload form without using the SlickUpload server side controls or helpers.</asp:Content>
                          
