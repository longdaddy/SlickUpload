Imports Krystalware.SlickUpload
Imports AspNetMvc3Vb.Models

Public Class SqlServerController
    Inherits System.Web.Mvc.Controller

    
    Public Function Index(model As SqlServerModel) As ActionResult
        If model Is Nothing Then
            model = New SqlServerModel()
        End If

        
    Try
        model.ExistingFiles = New AspNetMvc3Vb.Storage.SqlFileRepository("sqlServer").GetAll()   
    Catch ex As Exception
        model.Exception = ex
    End Try


        Return View(model)
    End Function

    Public Function UploadResult(model As SqlServerModel, session As UploadSession) As ActionResult
        

        model.UploadSession = session

        Return View(model)
    End Function
    

    
        Public Function Download(id As Integer) As ActionResult
            Dim repository As AspNetMvc3Vb.Storage.SqlFileRepository = New AspNetMvc3Vb.Storage.SqlFileRepository("sqlServer")
            Dim f As AspNetMvc3Vb.Storage.SqlFile = repository.GetById(id)

            Return File(repository.GetDataStream(f), "application/octet-stream", f.Name)
        End Function
    
End Class
