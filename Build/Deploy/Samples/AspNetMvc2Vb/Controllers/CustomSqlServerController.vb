Imports Krystalware.SlickUpload
Imports AspNetMvc2Vb.Models

Public Class CustomSqlServerController
    Inherits System.Web.Mvc.Controller

    
    Public Function Index(model As CustomSqlServerModel) As ActionResult
        If model Is Nothing Then
            model = New CustomSqlServerModel()
        End If

        
    Try
        model.ExistingFiles = New AspNetMvc2Vb.Storage.SqlFileRepository("customSqlServer").GetAll(true)   
    Catch ex As Exception
        model.Exception = ex
    End Try


        Return View(model)
    End Function

    Public Function UploadResult(model As CustomSqlServerModel, session As UploadSession) As ActionResult
        

        model.UploadSession = session

        Return View(model)
    End Function
    

    
        Public Function Download(id As Integer) As ActionResult
            Dim repository As AspNetMvc2Vb.Storage.SqlFileRepository = New AspNetMvc2Vb.Storage.SqlFileRepository("customSqlServer")
            Dim f As AspNetMvc2Vb.Storage.SqlFile = repository.GetById(id)

            Return File(repository.GetDataStream(f), "application/octet-stream", f.Name)
        End Function
    
End Class
