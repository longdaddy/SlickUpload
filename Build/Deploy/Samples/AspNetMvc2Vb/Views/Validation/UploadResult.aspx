<%@ Page Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage(Of AspNetMvc2Vb.Models.ValidationModel)" EnableViewState="false" Title="Validation" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>
<asp:Content ContentPlaceHolderID="content" runat="server">
            <h2>Upload Result</h2>
             <% If Not Model.UploadSession Is Nothing Then %>
                <p>Result: <%:Model.UploadSession.State.ToString() %></p>
                <% If Model.UploadSession.State <> UploadState.Error Then %>
                <p>Files Uploaded: <%:Model.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
<% For Each file As UploadedFile In Model.UploadSession.UploadedFiles %>                        <tr>
                            <td>
                                <%=file.ServerLocation.Replace("\", "\<wbr />") %>
                            </td>
                            <td>
                                <%:file.ContentType %>
                            </td>
                            
                            <td>
                                <%:file.ContentLength %>
                            </td>
                        </tr>
<% Next file %>                        <% If Model.UploadSession.UploadedFiles.Count = 0 Then %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% End If %>
                    </tbody>
                </table>
                <% Else %>
                <p>Error Summary: <%:Model.UploadSession.ErrorSummary %></p>
                <% End If %>
            <% Else %>
            <p>No upload recieved.</p>
            <% End If %>
            <p>

<% Using (Html.BeginForm("Index", "Validation", FormMethod.Post, New With { .id = "settingsForm" })) %><%:Html.HiddenFor(Function(x) x.MaxFiles) %><%:Html.HiddenFor(Function(x) x.RequireFileSelection) %><%:Html.HiddenFor(Function(x) x.AllowInvalidUpload) %><%:Html.HiddenFor(Function(x) x.ConfirmNavigateMessage) %><%:Html.HiddenFor(Function(x) x.ValidExtensions) %><%:Html.HiddenFor(Function(x) x.FileTypeMessage) %><%:Html.HiddenFor(Function(x) x.MaxFileSize) %><%:Html.HiddenFor(Function(x) x.FileSizeMessage) %><%:Html.HiddenFor(Function(x) x.SummaryMessage) %><% End Using %>                        <a id="newUploadButton" href="javascript:;" onclick="document.getElementById('settingsForm').submit()" class="button" >
 <span><b class="icon newupload"></b> New Upload</span>        </a>

                <div style="clear:both"></div>
            </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="description" runat="server">
Demonstrates SlickUpload validation. Covers file selection, maximum selected file limits, maximum file size limits, file type validation, and requiring file selection.</asp:Content>
                          

