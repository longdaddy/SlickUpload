using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Krystalware.SlickUpload;
using AspNetMvcRazorCs.Models;

namespace AspNetMvcRazorCs.Controllers
{
    public class CustomSqlServerController : Controller
    {
        
        public ActionResult Index(CustomSqlServerModel model)
        {
            if (model == null)
                model = new CustomSqlServerModel();

            
    try
    {
        model.ExistingFiles = new AspNetMvcRazorCs.Storage.SqlFileRepository("customSqlServer").GetAll(true);
    }
    catch (Exception ex)
    {
        model.Exception = ex;
    }


            return View(model);
        }

        public ActionResult UploadResult(CustomSqlServerModel model, UploadSession session)
        {
            

            model.UploadSession = session;

            return View(model);
        }
        

        
        public ActionResult Download(int id)
        {
            AspNetMvcRazorCs.Storage.SqlFileRepository repository = new AspNetMvcRazorCs.Storage.SqlFileRepository("customSqlServer");
            AspNetMvcRazorCs.Storage.SqlFile file = repository.GetById(id);

            return File(repository.GetDataStream(file), "application/octet-stream", file.Name);
        }

    }
}
