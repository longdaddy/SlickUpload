using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Krystalware.SlickUpload;
using AspNetMvcRazorCs.Models;

namespace AspNetMvcRazorCs.Controllers
{
    public class PostProcessController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UploadResult(UploadSession session)
        {
            
                    if (session != null && session.State == UploadState.Complete && session.UploadedFiles.Count > 0)
                    {
                        // Simulate post processing
                        for (int i = 0; i <= 100; i++)
                        {
                            session.ProcessingStatus["percentComplete"] = i.ToString();
                            session.ProcessingStatus["percentCompleteText"] = i.ToString() + "%";

                            SlickUploadContext.UpdateSession(session);

                            System.Threading.Thread.Sleep(100);
                        }
                    }


            return View(session);
        }
        

        
    }
}
