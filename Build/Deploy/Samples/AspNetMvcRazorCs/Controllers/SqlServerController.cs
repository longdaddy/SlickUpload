using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Krystalware.SlickUpload;
using AspNetMvcRazorCs.Models;

namespace AspNetMvcRazorCs.Controllers
{
    public class SqlServerController : Controller
    {
        
        public ActionResult Index(SqlServerModel model)
        {
            if (model == null)
                model = new SqlServerModel();

            
    try
    {
        model.ExistingFiles = new AspNetMvcRazorCs.Storage.SqlFileRepository("sqlServer").GetAll();
    }
    catch (Exception ex)
    {
        model.Exception = ex;
    }


            return View(model);
        }

        public ActionResult UploadResult(SqlServerModel model, UploadSession session)
        {
            

            model.UploadSession = session;

            return View(model);
        }
        

        
        public ActionResult Download(int id)
        {
            AspNetMvcRazorCs.Storage.SqlFileRepository repository = new AspNetMvcRazorCs.Storage.SqlFileRepository("sqlServer");
            AspNetMvcRazorCs.Storage.SqlFile file = repository.GetById(id);

            return File(repository.GetDataStream(file), "application/octet-stream", file.Name);
        }

    }
}
