using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Krystalware.SlickUpload;
using AspNetMvc2Cs.Models;

namespace AspNetMvc2Cs.Controllers
{
    public class SqlServerController : Controller
    {
        
        public ActionResult Index(SqlServerModel model)
        {
            if (model == null)
                model = new SqlServerModel();

            
    try
    {
        model.ExistingFiles = new AspNetMvc2Cs.Storage.SqlFileRepository("sqlServer").GetAll();
    }
    catch (Exception ex)
    {
        model.Exception = ex;
    }


            return View(model);
        }

        public ActionResult UploadResult(SqlServerModel model, UploadSession session)
        {
            

            model.UploadSession = session;

            return View(model);
        }
        

        
        public ActionResult Download(int id)
        {
            AspNetMvc2Cs.Storage.SqlFileRepository repository = new AspNetMvc2Cs.Storage.SqlFileRepository("sqlServer");
            AspNetMvc2Cs.Storage.SqlFile file = repository.GetById(id);

            return File(repository.GetDataStream(file), "application/octet-stream", file.Name);
        }

    }
}
