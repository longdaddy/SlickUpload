<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AspNetMvc2Cs.Models.ValidationModel>" EnableViewState="false" Title="Validation" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>
<asp:Content ContentPlaceHolderID="content" runat="server">
            <h2>Upload Result</h2>
             <% if (Model.UploadSession != null) { %>
                <p>Result: <%=Model.UploadSession.State.ToString() %></p>
                <% if (Model.UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%=Model.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
<% foreach (UploadedFile file in Model.UploadSession.UploadedFiles) { %>                        <tr>
                            <td>
                                <%=file.ServerLocation.Replace("\\", "\\<wbr />") %>
                            </td>
                            <td>
                                <%=file.ContentType %>
                            </td>
                            
                            <td>
                                <%=file.ContentLength %>
                            </td>
                        </tr>
<% } %>                        <% if (Model.UploadSession.UploadedFiles.Count == 0) { %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <% } else { %>
                <p>Error Summary: <%=Model.UploadSession.ErrorSummary %></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>

<% using (Html.BeginForm("Index", "Validation", FormMethod.Post, new { id = "settingsForm" })) { %><%=Html.HiddenFor(x => x.MaxFiles) %><%=Html.HiddenFor(x => x.RequireFileSelection) %><%=Html.HiddenFor(x => x.AllowInvalidUpload) %><%=Html.HiddenFor(x => x.ConfirmNavigateMessage) %><%=Html.HiddenFor(x => x.ValidExtensions) %><%=Html.HiddenFor(x => x.FileTypeMessage) %><%=Html.HiddenFor(x => x.MaxFileSize) %><%=Html.HiddenFor(x => x.FileSizeMessage) %><%=Html.HiddenFor(x => x.SummaryMessage) %><% } %>                        <a id="newUploadButton" href="javascript:;" onclick="document.getElementById('settingsForm').submit()" class="button" >
 <span><b class="icon newupload"></b> New Upload</span>        </a>

                <div style="clear:both"></div>
            </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="description" runat="server">
Demonstrates SlickUpload validation. Covers file selection, maximum selected file limits, maximum file size limits, file type validation, and requiring file selection.</asp:Content>
                          

