using System;

using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Configuration;
using Krystalware.SlickUpload.Storage;

namespace AspNetAjaxCs.Storage
{
    public class CustomFileNameUploadStreamProvider : FileUploadStreamProvider
    {
        public CustomFileNameUploadStreamProvider(UploadStreamProviderElement settings)
            : base(settings)
		{ }

        public override string GetServerFileName(UploadedFile file)
        {
            string fileRoot = file.UploadRequest.Data["fileRoot"];

            return fileRoot + "\\" + GetValidFileName(file.ClientName);
        }
    }
}