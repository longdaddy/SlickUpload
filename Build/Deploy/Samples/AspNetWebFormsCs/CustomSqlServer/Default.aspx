<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetWebFormsCs.CustomSqlServer.CustomSqlServerDefault" EnableViewState="false" Title="Custom Sql Server" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function cancelUpload()
        {
            kw("<%=slickUpload.ClientID %>").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").className = "button" + (kw("<%=slickUpload.ClientID %>").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
    kw("<%=slickUpload.ClientID %>").set_Data("fileCategory", document.getElementById("fileCategory").value);

            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

        <form id="uploadForm" runat="server">

        
        <asp:Panel ID="uploadPanel" runat="server">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">File category</th>
                    </tr>
                    <tr>
                        <td>
                            <select id="fileCategory">
                                <option value="documents">Documents</option>
                                <option value="images">Images</option>
                                <option value="audio">Audio</option>
                                <option value="video">Video</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>The category selected above will be passed to the server with the upload (as a cookie), and used to specify the folder in which the upload files are stored.</p>

            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

                            <kw:SlickUpload Id="slickUpload" OnClientUploadSessionStarted="onSessionStarted" OnClientBeforeSessionEnd="onBeforeSessionEnd" OnClientFileAdded="onFileSelectionChanged" OnClientFileRemoved="onFileSelectionChanged" CssClass="simple" ShowDropZoneOnDocumentDragOver="true" Style="overflow:hidden;zoom:1" FileSelectorStyle="float:left" FileListStyle="clear:both" UploadProgressDisplayStyle="clear:both" UploadProfile="customSqlServer" OnUploadComplete="slickUpload_UploadComplete" runat="server"><SelectorTemplate>                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    </SelectorTemplate>
<SelectorFolderTemplate>                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    </SelectorFolderTemplate>
<SelectorDropZoneTemplate>                               
                                    <div>Drag and drop files here.</div>                
                                    </SelectorDropZoneTemplate>
<FileItemTemplate>                               
                                    <div class="filedata">
                                        <kw:FileListElement Element="FileName" runat="server"/>
                                        &ndash;
                                        <kw:FileListElement Element="FileSize" runat="server">(calculating)</kw:FileListElement>
                                    </div>
                                        <kw:FileListRemoveCommand runat="server" href="javascript:;">[x]</kw:FileListRemoveCommand>
                                        <kw:FileListElement Element="ValidationMessage" runat="server" style="color:#f00"/>
                                    </FileItemTemplate>
<ProgressTemplate>                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <kw:UploadProgressElement Element="FileCount" runat="server"/> file(s),
                                                <kw:UploadProgressElement Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                                            </div>
                                            <div>
                                                Currently uploading: <kw:UploadProgressElement Element="CurrentFileName" runat="server"/>
                                                file <kw:UploadProgressElement Element="CurrentFileIndex" runat="server"/>
                                                of <kw:UploadProgressElement Element="FileCount" runat="server"/>.
                                            </div>
                                            <div>
                                                Speed: <kw:UploadProgressElement Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                            <div>
                                                <kw:UploadProgressElement Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                            <div class="progressBarContainer">
                                                <kw:UploadProgressBar runat="server"/>
                                                <div class="progressBarText">
                                                    <kw:UploadProgressElement Element="PercentCompleteText" runat="server">(calculating)</kw:UploadProgressElement>
                                                </div>
                                            </div>
                                        </div>
                                    </ProgressTemplate>
</kw:SlickUpload>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <asp:LinkButton ID="uploadButton" runat="server" CssClass="button disabled"  CausesValidation="true" >
 <span><b class="icon upload"></b> Upload Files</span>        </asp:LinkButton>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
             <% if (slickUpload.UploadSession != null) { %>
                <p>Result: <%=slickUpload.UploadSession.State.ToString() %></p>
                <% if (slickUpload.UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%=slickUpload.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultsRepeater" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#((UploadedFile)Container.DataItem).ServerLocation.Replace("\\", "\\<wbr />") %></td>
                                    <td><%#((UploadedFile)Container.DataItem).ContentType %></td>
                                    
                                    <td><%#((UploadedFile)Container.DataItem).ContentLength %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (slickUpload.UploadSession.UploadedFiles.Count == 0) { %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <% } else { %>
                <p>Error Summary: <%=slickUpload.UploadSession.ErrorSummary %></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>
        <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button"   onclick="newUploadButton_Click">
 <span><b class="icon newupload"></b> New Upload</span>        </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
                          
        </asp:Panel>
        </form>

    




    <asp:Panel ID="existingFilesPanel" runat="server" Visible="false">
        <h2>Existing Files</h2>
        <table class="results" width="99%" cellpadding="4" cellspacing="0">
            <thead>
                <th align="left">Name</th>
                <th align="left">Category</th>
                <th align="left">Length (bytes)</th>
            </thead>
            <tbody>
            <asp:Repeater ID="filesRepeater" runat="server">
                <ItemTemplate>
                    <tr>
                        <td>
                            <a href="DownloadFile.ashx?id=<%#((AspNetWebFormsCs.Storage.SqlFile)Container.DataItem).Id %>" target="_blank"><%#((AspNetWebFormsCs.Storage.SqlFile)Container.DataItem).Name %></a>
                        </td>
                        <td>
                            <%#((AspNetWebFormsCs.Storage.SqlFile)Container.DataItem).Category %>
                        </td>
                        <td>
                            <%#((AspNetWebFormsCs.Storage.SqlFile)Container.DataItem).Length %>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </tbody>
        </table>
        <br />
    </asp:Panel>
    <div id="errorMessage" runat="server" visible="false" enableviewstate="false" style="border:1px solid #c00;padding:.5em;margin-bottom:1em;">
        <b style="color:#f00">ERROR:</b> Could not connect to database. Please ensure the connection string and table information in the web.config are correct and that the file table has been properly created. Exception:<br /> <br />        
    </div>

</asp:Content>

<asp:Content ContentPlaceHolderID="configuration" runat="server">
    <h2>Configuration</h2>
<p>Configuration for the SQL sample involves creating the file database and table and configuring SlickUpload
   to point at that database. To prepare the database, perform the following steps:</p>
<ol>
    <li>Create a new database, or select an existing database to use</li>
    <li>Change the su connection string in the &lt;connectionStrings&gt; web.config section to point to the database selected above</li>
</ol>
<p>SlickUpload supports writing to one of 3 file column data types: IMAGE, VARBINARY(MAX), and FILESTREAM. Each has different configuration steps:</p>
<h3>IMAGE</h3>
<ol>
    <li>Open the SqlFileTable-IMAGE-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named "customSqlServer", and change the dataType attribute to "IMAGE"</li>
</ol>
<h3>VARBINARY(MAX)</h3>
<ol>
    <li>Open the SqlFileTable-VARBINARYMAX-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named "customSqlServer", and change the dataType attribute to "VarBinaryMax"</li>
</ol>
<h3>FILESTREAM</h3>
<ol>
    <li>Modify the su connection string in the &lt;connectionStrings&gt; web.config section to use integrated security</li>
    <li>Ensure the database selected above has been enabled for FILESTREAM access. For more information, see <a href="http://msdn.microsoft.com/en-us/library/cc645585.aspx">How to: Create a FILESTREAM-Enabled Database</a></li>
    <li>Open the SqlFileTable-FILESTREAM-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above. Make sure to update the script based on the name of the FILESTREAM FileGroup you have created.</li>
    <li>Open the web.config, locate the uploadProfile named "customSqlServer", and change the dataType attribute to "FileStream"</li>
</ol>
</asp:Content>
<asp:Content ContentPlaceHolderID="description" runat="server">

    Demonstrates writing additional fields to SQL as a file is created.
</asp:Content>
