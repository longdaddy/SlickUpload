using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;

namespace AspNetWebFormsCs.ClientApi
{
    public partial class ClientApiDefault : System.Web.UI.Page
    {

        protected UploadSession UploadSession
        {
            get { return SlickUploadContext.CurrentUploadSession; }
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            if (UploadSession != null && UploadSession.State == UploadState.Complete)
            {
                if (UploadSession.UploadedFiles.Count > 0)
                {
                    resultsRepeater.DataSource = UploadSession.UploadedFiles;
                    resultsRepeater.DataBind();
                }
            }
        }

        protected void newUploadButton_Click(object sender, EventArgs e)
        {
            uploadResultPanel.Visible = false;
            uploadPanel.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UploadSession != null)
            {
                uploadResultPanel.Visible = true;
                uploadPanel.Visible = false;
            }
        }

        

    }
}
