<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetWebFormsCs.ClientApi.ClientApiDefault" EnableViewState="false" Title="Client Api" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">


<% if (uploadPanel.Visible) { %>        <kw:KrystalwareScriptRenderer runat="server"/>
        <script type="text/javascript">
            function onFileCountChanged(data)
            {
                var hasFiles = (kw("fileSelector").get_Files().length > 0);

                //document.getElementById("fileListTable").style.display = (hasFiles > 0 ? "" : "none");
                document.getElementById("<%=uploadButton.ClientID %>").className = "button" + (hasFiles ? "" : " disabled");
            }

            function onBeforeSessionEnd(data)
            {
                document.getElementById("cancelButton").style.display = "none";
            }

            function startUpload()
            {
                kw("uploadConnector").start();

                document.getElementById("cancelButton").style.display = "none";
            }

            function cancelUpload()
            {
                kw("uploadConnector").cancel();

                document.getElementById("cancelButton").style.display = "none";
            }

            function clearFiles()
            {
                kw("fileSelector").clear();
            }

            function onSessionStarted(data)
            {
                document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";
                document.getElementById("clearButton").style.display = "none";
                document.getElementById("cancelButton").style.display = "block";
            }

            kw(function ()
            {
                var uploadConnector = new kw.UploadConnector(
                {
                    id: "uploadConnector",
                    uploadHandlerUrl: "<%=ResolveUrl("~/SlickUpload.axd") %>",
                    uploadForm: "<%=uploadForm.ClientID %>",
                    uploadProfile: "clientApi",
                    autoUploadOnSubmit: true,
                    onBeforeSessionEnd: onBeforeSessionEnd,
                    onUploadSessionStarted: onSessionStarted
                });

                var fileSelector = new kw.FileSelector(
                {
                    id : "fileSelector",
                    folderElement : "folderSelector",
                    uploadConnector : uploadConnector,
                    onFileAdded: onFileCountChanged,
                    onFileRemoved: onFileCountChanged,
                    maxFileSize: <%=SlickUploadContext.Config.UploadProfiles["clientApi"].MaxRequestLength %>,
                    dropZone: "fileSelector_dropzone",
                    showDropZoneOnDocumentDragOver: true
                });

                var fileList = new kw.FileList(
                {
                    id : "fileList",
                    templateElement: "fileList_template",
                    fileSelector : "fileSelector"
                });

                var progressDisplay = new kw.UploadProgressDisplay(
                {
                    id:"progressDisplay",
                    uploadConnector: "uploadConnector"
                });

                
            });

            kw.debug = <%=HttpContext.Current.IsDebuggingEnabled.ToString().ToLower() %>;
        </script>
<% } %></asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

        <form id="uploadForm" runat="server">

        
        <asp:Panel ID="uploadPanel" runat="server">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

<div id="slickUpload" class="su-slickupload simple">
    <div id="fileSelector" class="su-fileselector">
        <a class="button"><span><b class="icon add"></b> Add files</span></a>
    </div>
    <div id="folderSelector" class="su-folderselector">
        <a class="button"><span><b class="icon add-folder"></b> Add folder</span></a>
    </div>
    <div id="clearButton">
        <a class="button" onclick="clearFiles()"><span><b class="icon cancel"></b> Clear files</span></a>
    </div>
    <div id="fileSelector_dropzone" class="su-dropzone">
        <div>
            Drag and drop files here.</div>
    </div>
    <div id="fileList" class="su-filelist">
        <div id="fileList_template" class="su-filelisttemplate">
            <div class="filedata">
                <span class="su-filename"></span> &ndash; <span class="su-filesize">(calculating)</span>
            </div>
            <a class="su-removecommand">[x]</a>
            <span class="su-validationmessage"></span>
        </div>
    </div>
    <div id="progressDisplay" class="su-uploadprogressdisplay">
        <div id="duringUpload">
            <div>
                Uploading <span class="su-filecount"></span> file(s), <span class="su-contentlengthtext">(calculating)</span>.
            </div>
            <div>
                Currently uploading: <span class="su-currentfilename"></span>file <span class="su-currentfileindex">
                </span>of <span class="su-filecount"></span>.
            </div>
            <div>
                Speed: <span class="su-speedtext">(calculating)</span>
            </div>
            <div>
                <span class="su-timeremainingtext">(calculating)</span>
            </div>
            <div class="progressBarContainer">
                <div class="su-progressbar">
                </div>
                <div class="progressBarText">
                    <span class="su-percentcompletetext">(calculating)</span>
                </div>
            </div>
        </div>
    </div>
</div>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <asp:LinkButton ID="uploadButton" runat="server" CssClass="button disabled"  CausesValidation="true" onclick="uploadButton_Click">
 <span><b class="icon upload"></b> Upload Files</span>        </asp:LinkButton>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
             <% if (UploadSession != null) { %>
                <p>Result: <%=UploadSession.State.ToString() %></p>
                <% if (UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%=UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultsRepeater" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#((UploadedFile)Container.DataItem).ServerLocation.Replace("\\", "\\<wbr />") %></td>
                                    <td><%#((UploadedFile)Container.DataItem).ContentType %></td>
                                    
                                    <td><%#((UploadedFile)Container.DataItem).ContentLength %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (UploadSession.UploadedFiles.Count == 0) { %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <% } else { %>
                <p>Error Summary: <%=UploadSession.ErrorSummary %></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>
        <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button"   onclick="newUploadButton_Click">
 <span><b class="icon newupload"></b> New Upload</span>        </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
                          
        </asp:Panel>
        </form>

    


</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    Use the SlickUpload client API to create an upload form without using the SlickUpload server side controls or helpers.
</asp:Content>
