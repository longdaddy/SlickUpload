<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetWebFormsCs.Multiple.MultipleDefault" EnableViewState="false" Title="Multiple" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function cancelUpload()
        {
            kw("<%=slickUpload.ClientID %>").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }


        function onFileSelectionChanged(data)
        {
            var hasFiles = kw("<%=fileSelector1.ClientID %>").get_Files().length > 0 || kw("<%=fileSelector2.ClientID %>").get_Files().length > 0;

            document.getElementById("<%=uploadButton.ClientID %>").className = "button" + (hasFiles ? "" : " disabled");
        }

        function onSessionStarted(data)
        {
            
            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

        <form id="uploadForm" runat="server">

        
        <asp:Panel ID="uploadPanel" runat="server">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

    <h2 style="clear:both;margin-top:.5em;margin-bottom:0">Selector 1</h2>
    <div class="simple su-slickupload">
        <kw:FileSelector Id="fileSelector1" OnClientFileAdded="onFileSelectionChanged" OnClientFileRemoved="onFileSelectionChanged" ShowDropZoneOnDocumentDragOver="true" UploadConnectorId="slickUpload" runat="server"><Template>            
                                <a class="button">
                                    <span><b class="icon add"></b> Add files</span>
                                </a>
                                </Template>
<FolderTemplate>            
                                <a class="button">
                                    <span><b class="icon add-folder"></b> Add folder</span>
                                </a>
                                </FolderTemplate>
<DropZoneTemplate>            
                                <div>Drag and drop files here.</div>                
                                </DropZoneTemplate>
</kw:FileSelector>  
        <kw:FileList Id="fileList1" FileSelectorId="fileSelector1" runat="server"><ItemTemplate>            
                                <div class="filedata">
                                    <kw:FileListElement Element="FileName" runat="server"/>
                                    &ndash;
                                    <kw:FileListElement Element="FileSize" runat="server">(calculating)</kw:FileListElement>
                                </div>
                                    <kw:FileListRemoveCommand runat="server">[x]</kw:FileListRemoveCommand>
                                    <kw:FileListElement Element="ValidationMessage" runat="server"/>
                                </ItemTemplate>
</kw:FileList>  
        <div style="clear:both"></div>
    </div>  
    <h2 style="clear:both;margin-top:.5em;margin-bottom:0">Selector 2</h2>
    <div class="simple su-slickupload">
        <kw:FileSelector Id="fileSelector2" OnClientFileAdded="onFileSelectionChanged" OnClientFileRemoved="onFileSelectionChanged" ShowDropZoneOnDocumentDragOver="true" UploadConnectorId="slickUpload" runat="server"><Template>            
                                <a class="button">
                                    <span><b class="icon add"></b> Add files</span>
                                </a>
                                </Template>
<FolderTemplate>            
                                <a class="button">
                                    <span><b class="icon add-folder"></b> Add folder</span>
                                </a>
                                </FolderTemplate>
<DropZoneTemplate>            
                                <div>Drag and drop files here.</div>                
                                </DropZoneTemplate>
</kw:FileSelector>  
        <kw:FileList Id="fileList2" FileSelectorId="fileSelector2" runat="server"><ItemTemplate>            
                                <div class="filedata">
                                    <kw:FileListElement Element="FileName" runat="server"/>
                                    &ndash;
                                    <kw:FileListElement Element="FileSize" runat="server">(calculating)</kw:FileListElement>
                                </div>
                                    <kw:FileListRemoveCommand runat="server">[x]</kw:FileListRemoveCommand>
                                    <kw:FileListElement Element="ValidationMessage" runat="server"/>
                                </ItemTemplate>
</kw:FileList> 
        <div style="clear:both"></div>
    </div> 

<kw:UploadProgressDisplay Id="uploadProgressDisplay" UploadConnectorId="slickUpload" runat="server" Style="clear:both"><Template>        
            <div id="duringUpload">
                <div>
                    Uploading <kw:UploadProgressElement Element="FileCount" runat="server"/> file(s),
                    <kw:UploadProgressElement Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                </div>
                <div>
                    Currently uploading: <kw:UploadProgressElement Element="CurrentFileName" runat="server"/>
                    file <kw:UploadProgressElement Element="CurrentFileIndex" runat="server"/>
                    of <kw:UploadProgressElement Element="FileCount" runat="server"/>.
                </div>
                <div>
                    Speed: <kw:UploadProgressElement Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>
                </div>
                <div>
                    <kw:UploadProgressElement Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement>
                </div>
                <div class="progressBarContainer">
                    <kw:UploadProgressBar runat="server"/>
                    <div class="progressBarText">
                        <kw:UploadProgressElement Element="PercentCompleteText" runat="server">(calculating)</kw:UploadProgressElement>
                    </div>
                </div>
            </div>
        </Template>
</kw:UploadProgressDisplay>

<kw:UploadConnector Id="slickUpload" OnUploadComplete="slickUpload_UploadComplete" UploadProfile="multiple" OnClientUploadSessionStarted="onSessionStarted" OnClientBeforeSessionEnd="onBeforeSessionEnd" runat="server"/>


                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <asp:LinkButton ID="uploadButton" runat="server" CssClass="button disabled"  CausesValidation="true" >
 <span><b class="icon upload"></b> Upload Files</span>        </asp:LinkButton>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
             <% if (slickUpload.UploadSession != null) { %>
                <p>Result: <%=slickUpload.UploadSession.State.ToString() %></p>
                <% if (slickUpload.UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%=slickUpload.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
    <th>FileSelector</th>

                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultsRepeater" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#((UploadedFile)Container.DataItem).ServerLocation.Replace("\\", "\\<wbr />") %></td>
                                    <td><%#((UploadedFile)Container.DataItem).ContentType %></td>
                                    
    <td><%#((UploadedFile)Container.DataItem).FileSelectorId %></td>

                                    <td><%#((UploadedFile)Container.DataItem).ContentLength %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (slickUpload.UploadSession.UploadedFiles.Count == 0) { %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <% } else { %>
                <p>Error Summary: <%=slickUpload.UploadSession.ErrorSummary %></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>
        <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button"   onclick="newUploadButton_Click">
 <span><b class="icon newupload"></b> New Upload</span>        </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
                          



        </asp:Panel>
        </form>

    



</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    How to implement multiple file selection areas on one page.
</asp:Content>
