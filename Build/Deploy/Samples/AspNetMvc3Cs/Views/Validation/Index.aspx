<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AspNetMvc3Cs.Models.ValidationModel>" EnableViewState="false" Title="Validation" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        
        function startUpload()
        {
            if (validateUpload())
                kw("slickUpload").start();
        }
        
        
        function cancelUpload()
        {
            kw("slickUpload").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("uploadButton").className = "button" + (kw("slickUpload").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
            document.getElementById("uploadButton").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("uploadButton").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
        function startEdit()
        {
            if (window.kw && kw("slickUpload"))
                kw("slickUpload").clear();

            var panel = document.getElementById("uploadPanel");

            panel.style.display = "none";

            document.getElementById("settingsBox").style.backgroundColor = "#ffffe0";
            document.getElementById("settingsBox").style.padding = "1em";
            document.getElementById("settingsBox").style.border = "1px solid #ccc";
            document.getElementById("settingsTable").style.width = "99%";
            window.setTimeout(function () { document.getElementById("settingsTable").style.width = "100%"; }, 1);

            document.getElementById("editSettingsButton").style.display = "none";

            document.getElementById("saveSettingsButton").style.display = "";

            var spans = document.getElementById("settingsTable").getElementsByTagName("span");

            for (var i = 0; i < spans.length; i++)
                spans[i].style.display = "none";

            var inputs = document.getElementById("settingsTable").getElementsByTagName("input");

            for (var i = 0; i < inputs.length; i++)
            {
                inputs[i].style.display = "inline";
            }
        }

        
        function saveEdit()
        {
            var uploadForm = document.getElementById("uploadForm");

            uploadForm.action = "<%:Url.Action("Index") %>";

            uploadForm.submit();
        }

        function validateUpload()
        {
            var isValid = true;

            var files = kw("slickUpload").get_Files();

            if (document.getElementById("RequireFileSelection").checked)
            {
                var isRequiredValid = (files.length > 0);

                document.getElementById("requiredFilesValidator").style.display = (isRequiredValid ? "none" : "block");

                if (!isRequiredValid)
                    isValid = false;
            }

            if (!document.getElementById("AllowInvalidUpload").checked)
            {
                var isFilesValid = true;

                for (var i = 0; i < files.length; i++)
                {
                    if (!files[i].get_IsValid())
                    {
                        isFilesValid = false;

                        break;
                    }
                }

                document.getElementById("summaryValidator").style.display = (isFilesValid ? "none" : "block");

                if (!isFilesValid)
                    isValid = false;
            }

            return isValid;
        }
        

    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <% using (Html.BeginForm("UploadResult", "Validation", FormMethod.Post, new { id = "uploadForm", enctype = "multipart/form-data" })) { %>
        
        <div style="margin-bottom:1em;" id="settingsBox">
            <table id="settingsTable" class="settings">
                <tbody>
                    <tr>
                        <th colspan="3" style="font-weight:bold;border-bottom:solid 1px #ccc">General Settings</th>
                    </tr>
                    <tr>
                        <th style="width:12em">Max files:</th>
                        <td style="vertical-align:middle">                            
                            <span><%:Model.MaxFiles %></span>
                            <%:Html.TextBoxFor(x => x.MaxFiles, new { style = "display:none" }) %>
                        </td>
                        <td style="text-align:right"><em>Number of files</em></td>
                    </tr>
                    <tr>
                        <th style="width:12em">Max file size:</th>
                        <td style="vertical-align:middle">                            
                            <span><%:Model.MaxFileSize %></span>
                            <%:Html.TextBoxFor(x => x.MaxFileSize, new { style = "display:none" }) %>
                        </td>
                        <td style="text-align:right"><em>Size, in KB</em></td>
                    </tr>
                    <tr>
                        <th>Confirm navigate message:</th>
                        <td>
                            <span><%:Model.ConfirmNavigateMessage %></span>
                            <%:Html.TextBoxFor(x => x.ConfirmNavigateMessage, new { style = "display:none" }) %>
                        </td>
                        <td style="text-align:right"><em>Prompt when user navigates during upload</em></td>
                    </tr>
                    <tr>
                        <th colspan="3" style="font-weight:bold;border-bottom:solid 1px #ccc">Validation</th>
                    </tr>
                    <tr>
                        <th>Require file selection:</th>
                        <td colspan="2">
                            <span><%:Model.RequireFileSelection ? "Yes" : "No" %></span>
                            <%:Html.CheckBoxFor(x => x.RequireFileSelection, new { style = "display:none" }) %>
                        </td>
                    </tr>
                    <tr>
                        <th>Allow invalid upload:</th>
                        <td>
                            <span><%:Model.AllowInvalidUpload ? "Yes" : "No" %></span>
                            <%:Html.CheckBoxFor(x => x.AllowInvalidUpload, new { style = "display:none" }) %>
                        </td>
                        <td style="text-align:right"><em>Allow uploads, even with invalid files</em></td>
                    </tr>
                    <tr>
                        <th>Valid extensions:</th>
                        <td>                            
                            <span><%:Model.ValidExtensions %></span>
                            <%:Html.TextBoxFor(x => x.ValidExtensions, new { style = "display:none" }) %>
                        </td>
                        <td style="text-align:right"><em>Comma seperated list of valid extensions</em></td>
                    </tr>
                    <tr>
                        <th>Per file type message:</th>
                        <td>                            
                            <span><%:Model.FileTypeMessage %></span>
                            <%:Html.TextBoxFor(x => x.FileTypeMessage, new { style = "display:none" }) %>
                        </td>
                        <td style="text-align:right"><em>Displayed next to files with invalid types</em></td>
                    </tr>
                    <tr>
                        <th>Per file size message:</th>
                        <td>                            
                            <span><%:Model.FileSizeMessage %></span>
                            <%:Html.TextBoxFor(x => x.FileSizeMessage, new { style = "display:none" }) %>
                        </td>
                        <td style="text-align:right"><em>Displayed next to files with invalid sizes</em></td>
                    </tr>
                    <tr>
                        <th>Summary message:</th>
                        <td>                            
                            <span><%:Model.SummaryMessage %></span>
                            <%:Html.TextBoxFor(x => x.SummaryMessage, new { style = "display:none" }) %>
                        </td>
                        <td style="text-align:right"><em>Summary validation message for invalid files</em></td>
                    </tr>
                </tbody>
            </table>
            <p style="margin:.5em 0">
                <a id="editSettingsButton" href="javascript:;" onclick="startEdit()" class="button">
                    <span><b class="icon settings"></b> Edit Settings</span>
                </a>
                        <a id="saveSettingsButton" href="javascript:;" onclick="saveEdit()" class="button" style="display:none">
 <span><b class="icon save"></b> Save Settings</span>        </a>

                <a id="clearSettingsButton" href="javascript:;" onclick="window.location = window.location" class="button">
                    <span><b class="icon cancel"></b> Clear Settings</span>
                </a>
            </p>
            <div style="clear:both"></div>
        </div>

        <div id="uploadPanel">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            


                            <% Html.KrystalwareWebForms(new SlickUpload() { 
Id = "slickUpload",
OnClientUploadSessionStarted = "onSessionStarted",
OnClientBeforeSessionEnd = "onBeforeSessionEnd",
OnClientFileAdded = "onFileSelectionChanged",
OnClientFileRemoved = "onFileSelectionChanged",
SelectorTemplate = new Template(() => { %>
                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    
<% }),
ShowDropZoneOnDocumentDragOver = true,
HtmlAttributes = new { @class = "simple", Style = "overflow:hidden;zoom:1"},
FileSelectorHtmlAttributes = new { Style = "float:left"},
FileListHtmlAttributes = new { Style = "clear:both"},
UploadProgressDisplayHtmlAttributes = new { Style = "clear:both"},
MaxFiles = Model.MaxFiles,
ValidExtensions = Model.ValidExtensions,
MaxFileSize = Model.MaxFileSize,
ConfirmNavigateDuringUploadMessage = Model.ConfirmNavigateMessage,
InvalidExtensionMessage = Model.FileTypeMessage,
InvalidFileSizeMessage = Model.FileSizeMessage,
UploadProfile = "validation",
AutoUploadOnSubmit = true,
UploadFormId = "uploadForm",
SelectorFolderTemplate = new Template(() => { %>
                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    
<% }),
SelectorDropZoneTemplate = new Template(() => { %>
                               
                                    <div>Drag and drop files here.</div>                
                                    
<% }),
FileItemTemplate = new Template(() => { %>
                               
                                    <div class="filedata">
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.FileName } ); %>
                                        &ndash;
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.FileSize, Template = new Template("(calculating)") } ); %>
                                    </div>
                                        <% Html.KrystalwareWebForms(new FileListRemoveCommand() { HtmlAttributes = new { href = "javascript:;"}, Template = new Template("[x]") } ); %>
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.ValidationMessage, HtmlAttributes = new { style = "color:#f00"} } ); %>
                                    
<% }),
ProgressTemplate = new Template(() => { %>
                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.FileCount } ); %> file(s),
                                                <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.ContentLengthText, Template = new Template("(calculating)") } ); %>.
                                            </div>
                                            <div>
                                                Currently uploading: <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.CurrentFileName } ); %>
                                                file <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.CurrentFileIndex } ); %>
                                                of <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.FileCount } ); %>.
                                            </div>
                                            <div>
                                                Speed: <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.SpeedText, Template = new Template("(calculating)") } ); %>
                                            </div>
                                            <div>
                                                <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.TimeRemainingText, Template = new Template("(calculating)") } ); %>
                                            </div>
                                            <div class="progressBarContainer">
                                                <% Html.KrystalwareWebForms(new UploadProgressBar()); %>
                                                <div class="progressBarText">
                                                    <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.PercentCompleteText, Template = new Template("(calculating)") } ); %>
                                                </div>
                                            </div>
                                        </div>
                                    
<% })
 } ); %>
<% Html.KrystalwareWebForms(new KrystalwareScriptRenderer()); %>                            
                            <div id="requiredFilesValidator" style="display:none">Please select at least one file to upload.</div>
                            <div id="summaryValidator" style="display:none"><%:Model.SummaryMessage %></div>                                

                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <a id="uploadButton" href="javascript:;" onclick="startUpload()" class="button disabled" >
 <span><b class="icon upload"></b> Upload Files</span>        </a>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </div>
    <% } %>
    








</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    Demonstrates SlickUpload validation. Covers file selection, maximum selected file limits, maximum file size limits, file type validation, and requiring file selection.
</asp:Content>
