<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AspNetMvc3Cs.Models.SqlServerModel>" EnableViewState="false" Title="Sql Server" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function startUpload()
        {
            var slickUpload = kw("slickUpload");

            if (slickUpload)
                slickUpload.start();
        }
        
        
        function cancelUpload()
        {
            kw("slickUpload").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("uploadButton").className = "button" + (kw("slickUpload").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
            document.getElementById("uploadButton").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("uploadButton").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <% using (Html.BeginForm("UploadResult", "SqlServer", FormMethod.Post, new { id = "uploadForm", enctype = "multipart/form-data" })) { %>
        
        <div id="uploadPanel">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

                            <% Html.KrystalwareWebForms(new SlickUpload() { 
Id = "slickUpload",
OnClientUploadSessionStarted = "onSessionStarted",
OnClientBeforeSessionEnd = "onBeforeSessionEnd",
OnClientFileAdded = "onFileSelectionChanged",
OnClientFileRemoved = "onFileSelectionChanged",
SelectorTemplate = new Template(() => { %>
                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    
<% }),
ShowDropZoneOnDocumentDragOver = true,
HtmlAttributes = new { @class = "simple", Style = "overflow:hidden;zoom:1"},
FileSelectorHtmlAttributes = new { Style = "float:left"},
FileListHtmlAttributes = new { Style = "clear:both"},
UploadProgressDisplayHtmlAttributes = new { Style = "clear:both"},
UploadProfile = "sqlServer",
AutoUploadOnSubmit = true,
UploadFormId = "uploadForm",
SelectorFolderTemplate = new Template(() => { %>
                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    
<% }),
SelectorDropZoneTemplate = new Template(() => { %>
                               
                                    <div>Drag and drop files here.</div>                
                                    
<% }),
FileItemTemplate = new Template(() => { %>
                               
                                    <div class="filedata">
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.FileName } ); %>
                                        &ndash;
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.FileSize, Template = new Template("(calculating)") } ); %>
                                    </div>
                                        <% Html.KrystalwareWebForms(new FileListRemoveCommand() { HtmlAttributes = new { href = "javascript:;"}, Template = new Template("[x]") } ); %>
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.ValidationMessage, HtmlAttributes = new { style = "color:#f00"} } ); %>
                                    
<% }),
ProgressTemplate = new Template(() => { %>
                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.FileCount } ); %> file(s),
                                                <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.ContentLengthText, Template = new Template("(calculating)") } ); %>.
                                            </div>
                                            <div>
                                                Currently uploading: <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.CurrentFileName } ); %>
                                                file <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.CurrentFileIndex } ); %>
                                                of <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.FileCount } ); %>.
                                            </div>
                                            <div>
                                                Speed: <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.SpeedText, Template = new Template("(calculating)") } ); %>
                                            </div>
                                            <div>
                                                <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.TimeRemainingText, Template = new Template("(calculating)") } ); %>
                                            </div>
                                            <div class="progressBarContainer">
                                                <% Html.KrystalwareWebForms(new UploadProgressBar()); %>
                                                <div class="progressBarText">
                                                    <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.PercentCompleteText, Template = new Template("(calculating)") } ); %>
                                                </div>
                                            </div>
                                        </div>
                                    
<% })
 } ); %>
<% Html.KrystalwareWebForms(new KrystalwareScriptRenderer()); %>                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <a id="uploadButton" href="javascript:;" onclick="startUpload()" class="button disabled" >
 <span><b class="icon upload"></b> Upload Files</span>        </a>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </div>
    <% } %>
    
<% if (Model.Exception == null) { %><% if (Model.ExistingFiles.Count > 0) { %>        <h2>Existing Files</h2>
        <table class="results" width="99%" cellpadding="4" cellspacing="0">
            <thead>
                <th align="left">Name</th>
                <th align="left">Length (bytes)</th>
            </thead>
            <tbody>
            <% foreach (AspNetMvc3Cs.Storage.SqlFile file in Model.ExistingFiles) { %>
                <tr>
                    <td>                        
                        <%:Html.ActionLink(file.Name, "Download", new { id = file.Id }, new { target="_blank" }) %>
                    </td>
                    <td>
                        <%:file.Length %>
                    </td>
                </tr>
            <% } %>
            </tbody>
        </table>
        <br />
<% } %><% } else { %>    <div style="border:1px solid #c00;padding:.5em;margin-bottom:1em;">
        <b style="color:#f00">ERROR:</b> Could not connect to database. Please ensure the connection string and table information in the web.config are correct and that the file table has been properly created. Exception:<br /> <br /><%:Model.Exception.GetType().FullName %>: <%:Model.Exception.Message %>
    </div>
<% } %>
</asp:Content>

<asp:Content ContentPlaceHolderID="configuration" runat="server">
    <h2>Configuration</h2>
<p>Configuration for the SQL sample involves creating the file database and table and configuring SlickUpload
   to point at that database. To prepare the database, perform the following steps:</p>
<ol>
    <li>Create a new database, or select an existing database to use</li>
    <li>Change the su connection string in the &lt;connectionStrings&gt; web.config section to point to the database selected above</li>
</ol>
<p>SlickUpload supports writing to one of 3 file column data types: IMAGE, VARBINARY(MAX), and FILESTREAM. Each has different configuration steps:</p>
<h3>IMAGE</h3>
<ol>
    <li>Open the SqlFileTable-IMAGE.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named "sqlServer", and change the dataType attribute to "IMAGE"</li>
</ol>
<h3>VARBINARY(MAX)</h3>
<ol>
    <li>Open the SqlFileTable-VARBINARYMAX.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named "sqlServer", and change the dataType attribute to "VarBinaryMax"</li>
</ol>
<h3>FILESTREAM</h3>
<ol>
    <li>Modify the su connection string in the &lt;connectionStrings&gt; web.config section to use integrated security</li>
    <li>Ensure the database selected above has been enabled for FILESTREAM access. For more information, see <a href="http://msdn.microsoft.com/en-us/library/cc645585.aspx">How to: Create a FILESTREAM-Enabled Database</a></li>
    <li>Open the SqlFileTable-FILESTREAM.sql script in the SlickUpload distribution package root folder and run it on the database created above. Make sure to update the script based on the name of the FILESTREAM FileGroup you have created.</li>
    <li>Open the web.config, locate the uploadProfile named "sqlServer", and change the dataType attribute to "FileStream"</li>
</ol>
</asp:Content>
<asp:Content ContentPlaceHolderID="description" runat="server">

    Upload directly to a SQL Server IMAGE, VARBINARY(MAX), or FILESTREAM field, streaming the file to the field with minimal memory usage.
</asp:Content>
