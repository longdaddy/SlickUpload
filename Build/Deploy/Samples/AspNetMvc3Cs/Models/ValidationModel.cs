using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Krystalware.SlickUpload;

namespace AspNetMvc3Cs.Models
{
    public class ValidationModel
    {
        public int? MaxFiles { get; set; }
        public bool RequireFileSelection { get; set; }
        public bool AllowInvalidUpload { get; set; }
        public string ConfirmNavigateMessage { get; set; }
        public string ValidExtensions { get; set; }
        public string FileTypeMessage { get; set; }
        public int? MaxFileSize { get; set; }
        public string FileSizeMessage { get; set; }
        public string SummaryMessage { get; set; }

        public UploadSession UploadSession { get; set; }
    }
}