﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Krystalware.SlickUpload.Storage;
using Krystalware.SlickUpload.Configuration;
using System.IO;

namespace Krystalware.SlickUpload.WebAppTest
{
    public class ExceptionUploadStreamProvider : UploadStreamProviderBase
    {
        public ExceptionUploadStreamProvider(UploadStreamProviderElement config)
            : base(config)
        {
            
        }

        public override System.IO.Stream GetWriteStream(UploadedFile file)
        {
            return Stream.Null;
        }

        public override void RemoveOutput(UploadedFile file)
        {
            throw new NotImplementedException();
        }

        public override System.IO.Stream GetReadStream(UploadedFile file)
        {
            throw new NotImplementedException();
        }
    }
}