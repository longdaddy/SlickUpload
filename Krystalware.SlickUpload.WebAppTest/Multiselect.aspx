﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Multiselect.aspx.cs" Inherits="Krystalware.SlickUpload.WebAppTest.Multiselect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function startUpload()
        {
            var fileSelector = document.getElementById("fileSelector");

            var file = fileSelector.files[0];

            var xhr = new XMLHttpRequest();

            if (xhr.upload)
            {
                xhr.upload.addEventListener("progress", onUploadProgress, false);
                xhr.upload.addEventListener("load", onUploadComplete, false);
                xhr.upload.addEventListener("error", onUploadError, false);
            }
            else
            {
                xhr.addEventListener("progress", onUploadProgress, false);
                xhr.addEventListener("load", onUploadComplete, false);
                xhr.addEventListener("error", onUploadError, false);
            }

            xhr.open("POST", "/SlickUpload.axd?handlerType=upload&uploadSessionId=session&uploadRequestId=request", true);

            if (window.FormData)
            {
                var formData = new FormData();

                formData.append(fileSelector.name, file);

                xhr.send(formData);
            }
            else
            {
                xhr.setRequestHeader("X-File-Name", file.fileName);
                xhr.setRequestHeader("X-File-Content-Type", file.type);
                xhr.setRequestHeader("X-File-Source-Element", fileSelector.name);
                xhr.setRequestHeader("X-File-Size", file.fileSize);
                xhr.setRequestHeader("Content-Type", "application/octet-stream");

                if (xhr.sendAsBinary && file.getAsBinary)
                    xhr.sendAsBinary(file.getAsBinary());
                else
                    xhr.send(file);
            }
        }

        function onUploadProgress(e)
        {
            var position = e.position || e.loaded;
            var total = e.totalSize || e.total;

            document.getElementById("current").innerHTML = position;
            document.getElementById("total").innerHTML = total;
        }

        function onUploadComplete(e)
        {
            alert("upload complete!");
        }

        function onUploadError(e)
        {
            alert("upload error!");
        }
</script>
</head>
<body>
    <p><input type="file" id="fileSelector" name="fileSelector" /></p>
    <p><input type="button" onclick="startUpload()" /></p>
    <p><span id="current"></span>/<span id="total"></span></p>
</body>
</html>
