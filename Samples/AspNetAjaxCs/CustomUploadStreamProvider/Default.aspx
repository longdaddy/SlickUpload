<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetAjaxCs.CustomUploadStreamProvider.CustomUploadStreamProviderDefault" EnableViewState="false" Title="Custom UploadStreamProvider" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function cancelUpload()
        {
            kw("<%=slickUpload.ClientID %>").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").className = "button" + (kw("<%=slickUpload.ClientID %>").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

        <form id="uploadForm" runat="server">

        <asp:ScriptManager ID="scriptManager" runat="server" EnablePartialRendering="true">
        </asp:ScriptManager>               
        <asp:UpdatePanel ID="updatePanelTime" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <p>Time: <asp:Label ID="updateLabel" runat="server" /></p>
                <p><asp:Button ID="updateButton" runat="server" Text="Update Time" 
                        onclick="updateButton_Click" /></p>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        
        <asp:Panel ID="uploadPanel" runat="server">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

                            <kw:SlickUpload Id="slickUpload" OnClientUploadSessionStarted="onSessionStarted" OnClientBeforeSessionEnd="onBeforeSessionEnd" OnClientFileAdded="onFileSelectionChanged" OnClientFileRemoved="onFileSelectionChanged" CssClass="simple" ShowDropZoneOnDocumentDragOver="true" Style="overflow:hidden;zoom:1" FileSelectorStyle="float:left" FileListStyle="clear:both" UploadProgressDisplayStyle="clear:both" UploadProfile="customUploadStreamProvider" OnUploadComplete="slickUpload_UploadComplete" runat="server"><SelectorTemplate>                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    </SelectorTemplate>
<SelectorFolderTemplate>                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    </SelectorFolderTemplate>
<SelectorDropZoneTemplate>                               
                                    <div>Drag and drop files here.</div>                
                                    </SelectorDropZoneTemplate>
<FileItemTemplate>                               
                                    <div class="filedata">
                                        <kw:FileListElement Element="FileName" runat="server"/>
                                        &ndash;
                                        <kw:FileListElement Element="FileSize" runat="server">(calculating)</kw:FileListElement>
                                    </div>
                                        <kw:FileListRemoveCommand runat="server" href="javascript:;">[x]</kw:FileListRemoveCommand>
                                        <kw:FileListElement Element="ValidationMessage" runat="server" style="color:#f00"/>
                                    </FileItemTemplate>
<ProgressTemplate>                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <kw:UploadProgressElement Element="FileCount" runat="server"/> file(s),
                                                <kw:UploadProgressElement Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                                            </div>
                                            <div>
                                                Currently uploading: <kw:UploadProgressElement Element="CurrentFileName" runat="server"/>
                                                file <kw:UploadProgressElement Element="CurrentFileIndex" runat="server"/>
                                                of <kw:UploadProgressElement Element="FileCount" runat="server"/>.
                                            </div>
                                            <div>
                                                Speed: <kw:UploadProgressElement Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                            <div>
                                                <kw:UploadProgressElement Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                            <div class="progressBarContainer">
                                                <kw:UploadProgressBar runat="server"/>
                                                <div class="progressBarText">
                                                    <kw:UploadProgressElement Element="PercentCompleteText" runat="server">(calculating)</kw:UploadProgressElement>
                                                </div>
                                            </div>
                                        </div>
                                    </ProgressTemplate>
</kw:SlickUpload>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <asp:LinkButton ID="uploadButton" runat="server" CssClass="button disabled"  CausesValidation="true" >
 <span><b class="icon upload"></b> Upload Files</span>        </asp:LinkButton>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
             <% if (slickUpload.UploadSession != null) { %>
                <p>Result: <%=slickUpload.UploadSession.State.ToString() %></p>
                <% if (slickUpload.UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%=slickUpload.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultsRepeater" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#((UploadedFile)Container.DataItem).ServerLocation.Replace("\\", "\\<wbr />") %></td>
                                    <td><%#((UploadedFile)Container.DataItem).ContentType %></td>
                                    
                                    <td><%#((UploadedFile)Container.DataItem).ContentLength %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (slickUpload.UploadSession.UploadedFiles.Count == 0) { %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <% } else { %>
                <p>Error Summary: <%=slickUpload.UploadSession.ErrorSummary %></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>
        <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button"   onclick="newUploadButton_Click">
 <span><b class="icon newupload"></b> New Upload</span>        </asp:LinkButton>
                <div style="clear:both"></div>
            </p>
                          
        </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>            
        </form>

    
</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    Demonstrates how to write a custom UploadStreamProvider to stream uploads to an arbitrary location. The example UploadStreamProvider zips files as they are uploaded.
</asp:Content>
