using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.IO;

namespace AspNetAjaxCs.Storage
{
    public class CustomDownloadFileHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            long id = long.Parse(context.Request.QueryString["id"]);

            SqlFileRepository repository = new SqlFileRepository("customSqlServer");

            SqlFile file = repository.GetById(id);

            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            context.Response.AddHeader("Content-Length", file.Length.ToString());
            context.Response.ContentType = "application/octet-stream";

            using (Stream dataStream = repository.GetDataStream(file))
            {
                byte[] buffer = new byte[8192];

                int read;

                while ((read = dataStream.Read(buffer, 0, 8192)) > 0)
                    context.Response.OutputStream.Write(buffer, 0, read);
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}