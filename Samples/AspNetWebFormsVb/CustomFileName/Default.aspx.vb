Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Web.Controls

Namespace CustomFileName
    Public Class CustomFileNameDefault
        Inherits System.Web.UI.Page

        
        Protected Sub slickUpload_UploadComplete(ByVal sender As Object, ByVal e As UploadSessionEventArgs) Handles slickUpload.UploadComplete
            uploadResultPanel.Visible = True
            uploadPanel.Visible = False

            If Not e.UploadSession Is Nothing AndAlso e.UploadSession.State = UploadState.Complete Then
                If e.UploadSession.UploadedFiles.Count > 0 Then
                    
                    resultsRepeater.DataSource = e.UploadSession.UploadedFiles
                    resultsRepeater.DataBind()
                End If
            End If
        End Sub

        Protected Sub newUploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newUploadButton.Click
            uploadResultPanel.Visible = False
            uploadPanel.Visible = True
        End Sub
        

        

    End Class
End Namespace
