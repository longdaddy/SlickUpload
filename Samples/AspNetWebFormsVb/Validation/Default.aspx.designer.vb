'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace Validation 
    Partial Public Class ValidationDefault

        '''<summary>
        '''uploadForm control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents uploadForm As Global.System.Web.UI.HtmlControls.HtmlForm

        '''<summary>
        '''uploadPanel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents uploadPanel As Global.System.Web.UI.WebControls.Panel

        '''<summary>
        '''slickUpload control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents slickUpload As Global.Krystalware.SlickUpload.Web.Controls.SlickUpload

        '''<summary>
        '''uploadButton control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents uploadButton As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''uploadResultPanel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents uploadResultPanel As Global.System.Web.UI.WebControls.Panel

        '''<summary>
        '''resultsRepeater control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents resultsRepeater As Global.System.Web.UI.WebControls.Repeater

        '''<summary>
        '''newUploadButton control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents newUploadButton As Global.System.Web.UI.WebControls.LinkButton


        
        '''<summary>
        '''settingsBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents settingsBox As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''maxFilesTextBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents maxFilesTextBox As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''requireFileSelectionCheckBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents requireFileSelectionCheckBox As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''allowInvalidUploadCheckBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents allowInvalidUploadCheckBox As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''confirmNavigateMessageTextBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents confirmNavigateMessageTextBox As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''validExtensionsTextBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents validExtensionsTextBox As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''fileTypeMessageTextBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents fileTypeMessageTextBox As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''maxFileSizeTextBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents maxFileSizeTextBox As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''fileSizeMessageTextBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents fileSizeMessageTextBox As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''summaryMessageTextBox control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents summaryMessageTextBox As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''saveSettingsButton control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents saveSettingsButton As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''requiredFilesValidator control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents requiredFilesValidator As Global.System.Web.UI.WebControls.CustomValidator

        '''<summary>
        '''summaryValidator control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents summaryValidator As Global.System.Web.UI.WebControls.CustomValidator

    End Class
End Namespace
