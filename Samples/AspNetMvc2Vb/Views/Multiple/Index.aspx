<%@ Page Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" EnableViewState="false" Title="Multiple" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function startUpload()
        {
            var slickUpload = kw("slickUpload");

            if (slickUpload)
                slickUpload.start();
        }
        
        
        function cancelUpload()
        {
            kw("slickUpload").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }


        function onFileSelectionChanged(data)
        {
            var hasFiles = kw("fileSelector1").get_Files().length > 0 || kw("fileSelector2").get_Files().length > 0;

            document.getElementById("uploadButton").className = "button" + (hasFiles ? "" : " disabled");
        }

        function onSessionStarted(data)
        {
            
            document.getElementById("uploadButton").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("uploadButton").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <% Using (Html.BeginForm("UploadResult", "Multiple", FormMethod.Post, New With { .id = "uploadForm", .enctype = "multipart/form-data" })) %>
        
        <div id="uploadPanel">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

    <h2 style="clear:both;margin-top:.5em;margin-bottom:0">Selector 1</h2>
    <div class="simple su-slickupload">
        <% Html.KrystalwareWebForms(New FileSelector() With { _
.Id = "fileSelector1", _
.OnClientFileAdded = "onFileSelectionChanged", _
.OnClientFileRemoved = "onFileSelectionChanged", _
.ShowDropZoneOnDocumentDragOver = true, _
.UploadConnectorId = "slickUpload", _
.Template = New Template(Sub() %>
            
                                <a class="button">
                                    <span><b class="icon add"></b> Add files</span>
                                </a>
                                
<% End Sub), _
.FolderTemplate = New Template(Sub() %>
            
                                <a class="button">
                                    <span><b class="icon add-folder"></b> Add folder</span>
                                </a>
                                
<% End Sub), _
.DropZoneTemplate = New Template(Sub() %>
            
                                <div>Drag and drop files here.</div>                
                                
<% End Sub) _
 } ) %>  
        <% Html.KrystalwareWebForms(New FileList() With { _
.Id = "fileList1", _
.FileSelectorId = "fileSelector1", _
.ItemTemplate = New Template(Sub() %>
            
                                <div class="filedata">
                                    <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileName } ) %>
                                    &ndash;
                                    <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileSize, .Template = New Template("(calculating)") } ) %>
                                </div>
                                    <% Html.KrystalwareWebForms(New FileListRemoveCommand() With { .Template = New Template("[x]") } ) %>
                                    <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.ValidationMessage } ) %>
                                
<% End Sub) _
 } ) %>  
        <div style="clear:both"></div>
    </div>  
    <h2 style="clear:both;margin-top:.5em;margin-bottom:0">Selector 2</h2>
    <div class="simple su-slickupload">
        <% Html.KrystalwareWebForms(New FileSelector() With { _
.Id = "fileSelector2", _
.OnClientFileAdded = "onFileSelectionChanged", _
.OnClientFileRemoved = "onFileSelectionChanged", _
.ShowDropZoneOnDocumentDragOver = true, _
.UploadConnectorId = "slickUpload", _
.Template = New Template(Sub() %>
            
                                <a class="button">
                                    <span><b class="icon add"></b> Add files</span>
                                </a>
                                
<% End Sub), _
.FolderTemplate = New Template(Sub() %>
            
                                <a class="button">
                                    <span><b class="icon add-folder"></b> Add folder</span>
                                </a>
                                
<% End Sub), _
.DropZoneTemplate = New Template(Sub() %>
            
                                <div>Drag and drop files here.</div>                
                                
<% End Sub) _
 } ) %>  
        <% Html.KrystalwareWebForms(New FileList() With { _
.Id = "fileList2", _
.FileSelectorId = "fileSelector2", _
.ItemTemplate = New Template(Sub() %>
            
                                <div class="filedata">
                                    <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileName } ) %>
                                    &ndash;
                                    <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileSize, .Template = New Template("(calculating)") } ) %>
                                </div>
                                    <% Html.KrystalwareWebForms(New FileListRemoveCommand() With { .Template = New Template("[x]") } ) %>
                                    <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.ValidationMessage } ) %>
                                
<% End Sub) _
 } ) %> 
        <div style="clear:both"></div>
    </div> 

<% Html.KrystalwareWebForms(New UploadProgressDisplay() With { _
.Id = "uploadProgressDisplay", _
.UploadConnectorId = "slickUpload", _
.HtmlAttributes = New With { .Style = "clear:both"}, _
.Template = New Template(Sub() %>
        
            <div id="duringUpload">
                <div>
                    Uploading <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.FileCount } ) %> file(s),
                    <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.ContentLengthText, .Template = New Template("(calculating)") } ) %>.
                </div>
                <div>
                    Currently uploading: <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.CurrentFileName } ) %>
                    file <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.CurrentFileIndex } ) %>
                    of <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.FileCount } ) %>.
                </div>
                <div>
                    Speed: <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.SpeedText, .Template = New Template("(calculating)") } ) %>
                </div>
                <div>
                    <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.TimeRemainingText, .Template = New Template("(calculating)") } ) %>
                </div>
                <div class="progressBarContainer">
                    <% Html.KrystalwareWebForms(New UploadProgressBar()) %>
                    <div class="progressBarText">
                        <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.PercentCompleteText, .Template = New Template("(calculating)") } ) %>
                    </div>
                </div>
            </div>
        
<% End Sub) _
 } ) %>

<% Html.KrystalwareWebForms(New UploadConnector() With { .Id = "slickUpload", .UploadFormId = "uploadForm", .UploadProfile = "multiple", .OnClientUploadSessionStarted = "onSessionStarted", .OnClientBeforeSessionEnd = "onBeforeSessionEnd" } ) %>


<% Html.KrystalwareWebForms(New KrystalwareScriptRenderer()) %>                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <a id="uploadButton" href="javascript:;" onclick="startUpload()" class="button disabled" >
 <span><b class="icon upload"></b> Upload Files</span>        </a>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </div>
    <% End Using %>
    



</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    How to implement multiple file selection areas on one page.
</asp:Content>
