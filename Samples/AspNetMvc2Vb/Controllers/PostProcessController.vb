Imports Krystalware.SlickUpload
Imports AspNetMvc2Vb.Models

Public Class PostProcessController
    Inherits System.Web.Mvc.Controller

    
    Public Function Index() As ActionResult
        Return View()
    End Function

    Public Function UploadResult(session As UploadSession) As ActionResult
        
                    If Not session Is Nothing AndAlso session.State = UploadState.Complete AndAlso session.UploadedFiles.Count > 0 Then
                        'Simulate post processing
                        For i As Integer = 0 To 100
                            session.ProcessingStatus("percentComplete") = i.ToString()
                            session.ProcessingStatus("percentCompleteText") = i.ToString() + "%"

                            SlickUploadContext.UpdateSession(session)

                            System.Threading.Thread.Sleep(100)
                        Next i
                    End If


        Return View(session)
    End Function
    

        
End Class
