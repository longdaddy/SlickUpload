<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AspNetMvc3Cs.Models.CustomSqlServerModel>" EnableViewState="false" Title="Custom Sql Server" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>
<asp:Content ContentPlaceHolderID="content" runat="server">
            <h2>Upload Result</h2>
             <% if (Model.UploadSession != null) { %>
                <p>Result: <%:Model.UploadSession.State.ToString() %></p>
                <% if (Model.UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%:Model.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
<% foreach (UploadedFile file in Model.UploadSession.UploadedFiles) { %>                        <tr>
                            <td>
                                <%=file.ServerLocation.Replace("\\", "\\<wbr />") %>
                            </td>
                            <td>
                                <%:file.ContentType %>
                            </td>
                            
                            <td>
                                <%:file.ContentLength %>
                            </td>
                        </tr>
<% } %>                        <% if (Model.UploadSession.UploadedFiles.Count == 0) { %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <% } else { %>
                <p>Error Summary: <%:Model.UploadSession.ErrorSummary %></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>
        <a id="newUploadButton"  href="<%:Url.Action("Index") %>" class="button" >
 <span><b class="icon newupload"></b> New Upload</span>        </a>
                <div style="clear:both"></div>
            </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="description" runat="server">
Demonstrates writing additional fields to SQL as a file is created.</asp:Content>
                          
