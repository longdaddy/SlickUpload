using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Krystalware.SlickUpload;
using AspNetMvc3Cs.Models;

namespace AspNetMvc3Cs.Controllers
{
    public class CustomSqlServerController : Controller
    {
        
        public ActionResult Index(CustomSqlServerModel model)
        {
            if (model == null)
                model = new CustomSqlServerModel();

            
    try
    {
        model.ExistingFiles = new AspNetMvc3Cs.Storage.SqlFileRepository("customSqlServer").GetAll(true);
    }
    catch (Exception ex)
    {
        model.Exception = ex;
    }


            return View(model);
        }

        public ActionResult UploadResult(CustomSqlServerModel model, UploadSession session)
        {
            

            model.UploadSession = session;

            return View(model);
        }
        

        
        public ActionResult Download(int id)
        {
            AspNetMvc3Cs.Storage.SqlFileRepository repository = new AspNetMvc3Cs.Storage.SqlFileRepository("customSqlServer");
            AspNetMvc3Cs.Storage.SqlFile file = repository.GetById(id);

            return File(repository.GetDataStream(file), "application/octet-stream", file.Name);
        }

    }
}
