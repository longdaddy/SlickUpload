<%@ Page Language="C#" MasterPageFile="~/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AspNetWebFormsCs.Validation.ValidationDefault" EnableViewState="false" Title="Validation" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        
        function cancelUpload()
        {
            kw("<%=slickUpload.ClientID %>").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").className = "button" + (kw("<%=slickUpload.ClientID %>").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("<%=uploadButton.ClientID %>").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
        function startEdit()
        {
            if (window.kw && kw("<%=slickUpload.ClientID %>"))
                kw("<%=slickUpload.ClientID %>").clear();

            var panel = document.getElementById("<%=uploadPanel.ClientID %>");

            
            if (!panel)
                panel = document.getElementById("<%=uploadResultPanel.ClientID %>");
            
            panel.style.display = "none";

            document.getElementById("settingsBox").style.backgroundColor = "#ffffe0";
            document.getElementById("settingsBox").style.padding = "1em";
            document.getElementById("settingsBox").style.border = "1px solid #ccc";
            document.getElementById("settingsTable").style.width = "99%";
            window.setTimeout(function () { document.getElementById("settingsTable").style.width = "100%"; }, 1);

            document.getElementById("editSettingsButton").style.display = "none";

            document.getElementById("<%=saveSettingsButton.ClientID %>").style.display = "";

            var spans = document.getElementById("settingsTable").getElementsByTagName("span");

            for (var i = 0; i < spans.length; i++)
                spans[i].style.display = "none";

            var inputs = document.getElementById("settingsTable").getElementsByTagName("input");

            for (var i = 0; i < inputs.length; i++)
            {
                inputs[i].style.display = "inline";
                
                if (inputs[i].parentNode.style.display == "none")
                    inputs[i].parentNode.style.display = "inline";
                
            }
        }

        
        function Validate_SlickUploadRequiredFiles(source, args)
        {
            args.IsValid = (kw("<%=slickUpload.ClientID %>").get_Files().length > 0);
        }

        function Validate_SlickUploadValidFiles(source, args)
        {
            var files = kw("<%=slickUpload.ClientID %>").get_Files();

            args.IsValid = true;

            for (var i = 0; i < files.length; i++)
            {
                if (!files[i].get_IsValid())
                {
                    args.IsValid = false;

                    return;
                }
            }
        }
        

    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

        <form id="uploadForm" runat="server">

        
        <div style="margin-bottom:1em;" id="settingsBox">
            <table id="settingsTable" class="settings">
                <tbody>
                    <tr>
                        <th colspan="3" style="font-weight:bold;border-bottom:solid 1px #ccc">General Settings</th>
                    </tr>
                    <tr>
                        <th style="width:12em">Max files:</th>
                        <td style="vertical-align:middle">                            
                            <span><%=maxFilesTextBox.Text %></span>
                                    <asp:TextBox ID="maxFilesTextBox" runat="server" style = "display:none" />

                        </td>
                        <td style="text-align:right"><em>Number of files</em></td>
                    </tr>
                    <tr>
                        <th style="width:12em">Max file size:</th>
                        <td style="vertical-align:middle">                            
                            <span><%=maxFileSizeTextBox.Text %></span>
                                    <asp:TextBox ID="maxFileSizeTextBox" runat="server" style = "display:none" />

                        </td>
                        <td style="text-align:right"><em>Size, in KB</em></td>
                    </tr>
                    <tr>
                        <th>Confirm navigate message:</th>
                        <td>
                            <span><%=confirmNavigateMessageTextBox.Text %></span>
                                    <asp:TextBox ID="confirmNavigateMessageTextBox" runat="server" style = "display:none" />

                        </td>
                        <td style="text-align:right"><em>Prompt when user navigates during upload</em></td>
                    </tr>
                    <tr>
                        <th colspan="3" style="font-weight:bold;border-bottom:solid 1px #ccc">Validation</th>
                    </tr>
                    <tr>
                        <th>Require file selection:</th>
                        <td colspan="2">
                            <span><%=requireFileSelectionCheckBox.Checked ? "Yes" : "No" %></span>
                                    <asp:CheckBox ID="requireFileSelectionCheckBox" runat="server" style = "display:none" />

                        </td>
                    </tr>
                    <tr>
                        <th>Allow invalid upload:</th>
                        <td>
                            <span><%=allowInvalidUploadCheckBox.Checked ? "Yes" : "No" %></span>
                                    <asp:CheckBox ID="allowInvalidUploadCheckBox" runat="server" style = "display:none" />

                        </td>
                        <td style="text-align:right"><em>Allow uploads, even with invalid files</em></td>
                    </tr>
                    <tr>
                        <th>Valid extensions:</th>
                        <td>                            
                            <span><%=validExtensionsTextBox.Text %></span>
                                    <asp:TextBox ID="validExtensionsTextBox" runat="server" style = "display:none" />

                        </td>
                        <td style="text-align:right"><em>Comma seperated list of valid extensions</em></td>
                    </tr>
                    <tr>
                        <th>Per file type message:</th>
                        <td>                            
                            <span><%=fileTypeMessageTextBox.Text %></span>
                                    <asp:TextBox ID="fileTypeMessageTextBox" runat="server" style = "display:none" />

                        </td>
                        <td style="text-align:right"><em>Displayed next to files with invalid types</em></td>
                    </tr>
                    <tr>
                        <th>Per file size message:</th>
                        <td>                            
                            <span><%=fileSizeMessageTextBox.Text %></span>
                                    <asp:TextBox ID="fileSizeMessageTextBox" runat="server" style = "display:none" />

                        </td>
                        <td style="text-align:right"><em>Displayed next to files with invalid sizes</em></td>
                    </tr>
                    <tr>
                        <th>Summary message:</th>
                        <td>                            
                            <span><%=summaryMessageTextBox.Text %></span>
                                    <asp:TextBox ID="summaryMessageTextBox" runat="server" style = "display:none" />

                        </td>
                        <td style="text-align:right"><em>Summary validation message for invalid files</em></td>
                    </tr>
                </tbody>
            </table>
            <p style="margin:.5em 0">
                <a id="editSettingsButton" href="javascript:;" onclick="startEdit()" class="button">
                    <span><b class="icon settings"></b> Edit Settings</span>
                </a>
                        <asp:LinkButton ID="saveSettingsButton" runat="server" CssClass="button" style="display:none" CausesValidation="false" >
 <span><b class="icon save"></b> Save Settings</span>        </asp:LinkButton>

                <a id="clearSettingsButton" href="javascript:;" onclick="window.location = window.location" class="button">
                    <span><b class="icon cancel"></b> Clear Settings</span>
                </a>
            </p>
            <div style="clear:both"></div>
        </div>

        <asp:Panel ID="uploadPanel" runat="server">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            


                            <kw:SlickUpload Id="slickUpload" OnClientUploadSessionStarted="onSessionStarted" OnClientBeforeSessionEnd="onBeforeSessionEnd" OnClientFileAdded="onFileSelectionChanged" OnClientFileRemoved="onFileSelectionChanged" CssClass="simple" ShowDropZoneOnDocumentDragOver="true" Style="overflow:hidden;zoom:1" FileSelectorStyle="float:left" FileListStyle="clear:both" UploadProgressDisplayStyle="clear:both" UploadProfile="validation" OnUploadComplete="slickUpload_UploadComplete" runat="server"><SelectorTemplate>                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    </SelectorTemplate>
<SelectorFolderTemplate>                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    </SelectorFolderTemplate>
<SelectorDropZoneTemplate>                               
                                    <div>Drag and drop files here.</div>                
                                    </SelectorDropZoneTemplate>
<FileItemTemplate>                               
                                    <div class="filedata">
                                        <kw:FileListElement Element="FileName" runat="server"/>
                                        &ndash;
                                        <kw:FileListElement Element="FileSize" runat="server">(calculating)</kw:FileListElement>
                                    </div>
                                        <kw:FileListRemoveCommand runat="server" href="javascript:;">[x]</kw:FileListRemoveCommand>
                                        <kw:FileListElement Element="ValidationMessage" runat="server" style="color:#f00"/>
                                    </FileItemTemplate>
<ProgressTemplate>                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <kw:UploadProgressElement Element="FileCount" runat="server"/> file(s),
                                                <kw:UploadProgressElement Element="ContentLengthText" runat="server">(calculating)</kw:UploadProgressElement>.
                                            </div>
                                            <div>
                                                Currently uploading: <kw:UploadProgressElement Element="CurrentFileName" runat="server"/>
                                                file <kw:UploadProgressElement Element="CurrentFileIndex" runat="server"/>
                                                of <kw:UploadProgressElement Element="FileCount" runat="server"/>.
                                            </div>
                                            <div>
                                                Speed: <kw:UploadProgressElement Element="SpeedText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                            <div>
                                                <kw:UploadProgressElement Element="TimeRemainingText" runat="server">(calculating)</kw:UploadProgressElement>
                                            </div>
                                            <div class="progressBarContainer">
                                                <kw:UploadProgressBar runat="server"/>
                                                <div class="progressBarText">
                                                    <kw:UploadProgressElement Element="PercentCompleteText" runat="server">(calculating)</kw:UploadProgressElement>
                                                </div>
                                            </div>
                                        </div>
                                    </ProgressTemplate>
</kw:SlickUpload>
                            
                            <asp:CustomValidator ID="requiredFilesValidator" runat="server" ClientValidationFunction="Validate_SlickUploadRequiredFiles" Text="Please select at least one file to upload." Display="Dynamic" />
                            <asp:CustomValidator ID="summaryValidator" runat="server" ClientValidationFunction="Validate_SlickUploadValidFiles" Display="Dynamic" />

                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <asp:LinkButton ID="uploadButton" runat="server" CssClass="button disabled"  CausesValidation="true" >
 <span><b class="icon upload"></b> Upload Files</span>        </asp:LinkButton>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </asp:Panel>
        <asp:Panel ID="uploadResultPanel" runat="server" Visible="false">
            <h2>Upload Result</h2>
             <% if (slickUpload.UploadSession != null) { %>
                <p>Result: <%=slickUpload.UploadSession.State.ToString() %></p>
                <% if (slickUpload.UploadSession.State != UploadState.Error) { %>
                <p>Files Uploaded: <%=slickUpload.UploadSession.UploadedFiles.Count.ToString() %></p>
                <table class="results" width="99%" cellpadding="4" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">Server Location</th>
                            <th align="left">Mime Type</th>
                            
                            <th align="left">Length (bytes)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultsRepeater" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#((UploadedFile)Container.DataItem).ServerLocation.Replace("\\", "\\<wbr />") %></td>
                                    <td><%#((UploadedFile)Container.DataItem).ContentType %></td>
                                    
                                    <td><%#((UploadedFile)Container.DataItem).ContentLength %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (slickUpload.UploadSession.UploadedFiles.Count == 0) { %>
                        <tr class="nodata">
                            <td colspan="3">No files recieved.</td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <% } else { %>
                <p>Error Summary: <%=slickUpload.UploadSession.ErrorSummary %></p>
                <% } %>
            <% } else { %>
            <p>No upload recieved.</p>
            <% } %>
            <p>

                        <asp:LinkButton ID="newUploadButton" runat="server" CssClass="button"   onclick="newUploadButton_Click">
 <span><b class="icon newupload"></b> New Upload</span>        </asp:LinkButton>

                <div style="clear:both"></div>
            </p>
                          

        </asp:Panel>
        </form>

    








</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    Demonstrates SlickUpload validation. Covers file selection, maximum selected file limits, maximum file size limits, file type validation, and requiring file selection.
</asp:Content>
