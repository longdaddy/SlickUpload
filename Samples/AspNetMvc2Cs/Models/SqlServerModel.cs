using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Krystalware.SlickUpload;

using AspNetMvc2Cs.Storage;

namespace AspNetMvc2Cs.Models
{
    public class SqlServerModel
    {
        public List<SqlFile> ExistingFiles { get; set; }
        public Exception Exception { get; set; }
        public UploadSession UploadSession { get; set; }
    }
}