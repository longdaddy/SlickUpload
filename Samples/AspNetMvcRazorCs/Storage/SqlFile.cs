using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Configuration;
using Krystalware.SlickUpload.Storage;
using Krystalware.SlickUpload.Storage.Streams;

namespace AspNetMvcRazorCs.Storage
{
    public class SqlFile
    {
        long _id;
        string _name;
        string _category;
        string _keyField;
        long _length;

        public long Id
        {
            get
            {
                return _id;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string Category
        {
            get
            {
                return _category;
            }
        }

        public long Length
        {
            get
            {
                return _length;
            }
        }

        public SqlFile(long id, string name, long length, string keyField)
            : this(id, name, length, keyField, null)
        { }
        
        public SqlFile(long id, string name, long length, string keyField, string category)
        {
            _id = id;
            _name = name;
            _length = length;
            _category = category;
            _keyField = keyField;
        }

        internal string BuildCriteria(IDbCommand cmd)
        {
            IDbDataParameter param = cmd.CreateParameter();

            param.ParameterName = "@keyValue";
            param.DbType = DbType.String;
            param.Value = _id;

            cmd.Parameters.Add(param);

            return _keyField + "=@keyValue";
        }
    }
}