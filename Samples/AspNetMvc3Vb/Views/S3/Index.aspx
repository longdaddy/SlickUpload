<%@ Page Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" EnableViewState="false" Title="Amazon S3" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        
        function startUpload()
        {
            var slickUpload = kw("slickUpload");

            if (slickUpload)
                slickUpload.start();
        }
        
        
        function cancelUpload()
        {
            kw("slickUpload").cancel();

            document.getElementById("cancelButton").style.display = "none";
        }

        
        function onFileSelectionChanged(data)
        {
            document.getElementById("uploadButton").className = "button" + (kw("slickUpload").get_Files().length > 0 ? "" : " disabled");
        }
        

        function onSessionStarted(data)
        {
            
            document.getElementById("uploadButton").style.display = "none";

            document.getElementById("cancelButton").style.display = "block";
        }
        
        function onBeforeSessionEnd(data)
        {
            document.getElementById("uploadButton").style.display = "none";
            document.getElementById("cancelButton").style.display = "none";
        }
        

        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <% Using (Html.BeginForm("UploadResult", "S3", FormMethod.Post, New With { .id = "uploadForm", .enctype = "multipart/form-data" })) %>
        
        <div id="uploadPanel">
            
            <table class="settings">
                <tbody>
                    <tr>
                        <th style="font-weight:bold;border-bottom:solid 1px #ccc">Select files to upload</th>
                    </tr>
                    <tr>
                        <td>
                            

                            <% Html.KrystalwareWebForms(New SlickUpload() With { _
.Id = "slickUpload", _
.OnClientUploadSessionStarted = "onSessionStarted", _
.OnClientBeforeSessionEnd = "onBeforeSessionEnd", _
.OnClientFileAdded = "onFileSelectionChanged", _
.OnClientFileRemoved = "onFileSelectionChanged", _
.SelectorTemplate = New Template(Sub() %>
                               
                                    <a class="button">
                                        <span><b class="icon add"></b> Add files</span>
                                    </a>
                                    
<% End Sub), _
.ShowDropZoneOnDocumentDragOver = true, _
.HtmlAttributes = New With { .class = "simple", .Style = "overflow:hidden;zoom:1"}, _
.FileSelectorHtmlAttributes = New With { .Style = "float:left"}, _
.FileListHtmlAttributes = New With { .Style = "clear:both"}, _
.UploadProgressDisplayHtmlAttributes = New With { .Style = "clear:both"}, _
.UploadProfile = "s3", _
.AutoUploadOnSubmit = true, _
.UploadFormId = "uploadForm", _
.SelectorFolderTemplate = New Template(Sub() %>
                               
                                    <a class="button">
                                        <span><b class="icon add-folder"></b> Add folder</span>
                                    </a>
                                    
<% End Sub), _
.SelectorDropZoneTemplate = New Template(Sub() %>
                               
                                    <div>Drag and drop files here.</div>                
                                    
<% End Sub), _
.FileItemTemplate = New Template(Sub() %>
                               
                                    <div class="filedata">
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileName } ) %>
                                        &ndash;
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.FileSize, .Template = New Template("(calculating)") } ) %>
                                    </div>
                                        <% Html.KrystalwareWebForms(New FileListRemoveCommand() With { .HtmlAttributes = New With { .href = "javascript:;"}, .Template = New Template("[x]") } ) %>
                                        <% Html.KrystalwareWebForms(New FileListElement() With { .Element = FileListElementType.ValidationMessage, .HtmlAttributes = New With { .style = "color:#f00"} } ) %>
                                    
<% End Sub), _
.ProgressTemplate = New Template(Sub() %>
                               
                                        <div id="duringUpload">
                                            <div>
                                                Uploading <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.FileCount } ) %> file(s),
                                                <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.ContentLengthText, .Template = New Template("(calculating)") } ) %>.
                                            </div>
                                            <div>
                                                Currently uploading: <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.CurrentFileName } ) %>
                                                file <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.CurrentFileIndex } ) %>
                                                of <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.FileCount } ) %>.
                                            </div>
                                            <div>
                                                Speed: <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.SpeedText, .Template = New Template("(calculating)") } ) %>
                                            </div>
                                            <div>
                                                <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.TimeRemainingText, .Template = New Template("(calculating)") } ) %>
                                            </div>
                                            <div class="progressBarContainer">
                                                <% Html.KrystalwareWebForms(New UploadProgressBar()) %>
                                                <div class="progressBarText">
                                                    <% Html.KrystalwareWebForms(New UploadProgressElement() With { .Element = UploadProgressElementType.PercentCompleteText, .Template = New Template("(calculating)") } ) %>
                                                </div>
                                            </div>
                                        </div>
                                    
<% End Sub) _
 } ) %>
<% Html.KrystalwareWebForms(New KrystalwareScriptRenderer()) %>                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                        <a id="uploadButton" href="javascript:;" onclick="startUpload()" class="button disabled" >
 <span><b class="icon upload"></b> Upload Files</span>        </a>

                <a id="cancelButton" href="javascript:;" onclick="cancelUpload()" style="display:none" class="button">
                    <span><b class="icon cancel"></b> Cancel</span>
                </a>
                <div style="clear:both"></div>
            </p>
        </div>
    <% End Using %>
    
<% Dim configElement As Krystalware.SlickUpload.Configuration.UploadStreamProviderElement = SlickUploadContext.Config.UploadProfiles("s3").UploadStreamProvider %>
<% If string.IsNullOrEmpty(configElement.Parameters("accessKeyId")) OrElse string.IsNullOrEmpty(configElement.Parameters("secretAccessKey")) OrElse string.IsNullOrEmpty(configElement.Parameters("bucketName")) Then %>    
<div id="errorMessage" style="border:1px solid #c00;padding:.5em;margin-bottom:1em;">
    <b style="color:#f00">ERROR:</b> Please enter your Amazon S3 credentials and bucket name in the uploadStreamProvider section of the s3 uploadProfile in the web.config to enable this sample. The bucket name you specify must already exist.
</div>
<% End If %>
</asp:Content>

<asp:Content ContentPlaceHolderID="description" runat="server">

    Use the SlickUpload S3 provider to implement streaming upload to Amazon S3.
</asp:Content>
