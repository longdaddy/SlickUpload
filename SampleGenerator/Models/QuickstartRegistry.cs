﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleGenerator.Models
{
    public class QuickstartRegistry : Registry
    {
        public QuickstartRegistry()
        {
            Samples.Add(new Sample()
            {
                Path = "QuickStart",
                Name = "SlickUpload Quick Start Example"
            });

            Projects.Add(new Project()
            {
                Name = "AspNetWebFormsCs",
                Versions = new VsVersion[] { VsVersion.VS2005, VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.WebForms
            });

            Projects.Add(new Project()
            {
                Name = "AspNetWebFormsVb",
                Versions = new VsVersion[] { VsVersion.VS2005, VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.WebForms
            });

            Projects.Add(new Project()
            {
                Name = "AspNetAjaxCs",
                Versions = new VsVersion[] { VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.WebForms,
                IsAspNetAjax = true
            });

            Projects.Add(new Project()
            {
                Name = "AspNetAjaxVb",
                Versions = new VsVersion[] { VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.WebForms,
                IsAspNetAjax = true
            });

            /*Projects.Add(new Project()
            {
                Name = "AspNetMvc1Cs",
                Versions = new VsVersion[] { VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc1
            });

            Projects.Add(new Project()
            {
                Name = "AspNetMvc1Vb",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc1
            });*/

            Projects.Add(new Project()
            {
                Name = "AspNetMvc2Cs",
                Versions = new VsVersion[] { VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc2
            });

            Projects.Add(new Project()
            {
                Name = "AspNetMvc2Vb",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc2
            });

            Projects.Add(new Project()
            {
                Name = "AspNetMvc3Cs",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc3
            });

            Projects.Add(new Project()
            {
                Name = "AspNetMvc3Vb",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc3
            });
            
            Projects.Add(new Project()
            {
                Name = "AspNetMvcRazorCs",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.Razor,
                MvcVersion = MvcVersion.Mvc3
            });

            /*Projects.Add(new Project()
            {
                Name = "AspNetMvcRazorVb",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.Razor,
                MvcVersion = MvcVersion.Mvc3
            });*/

            Solutions.Add(new Solution()
            {
                Name = "VS2005",
                Projects = SelectProjects(Projects, VsVersion.VS2005, "AspNetWebForms"),
                Version = VsVersion.VS2005
            });

            Solutions.Add(new Solution()
            {
                Name = "VS2008",
                Projects = SelectProjects(Projects, VsVersion.VS2008, "AspNetWebForms", "AspNetAjax"),
                Version = VsVersion.VS2008
            });

            Solutions.Add(new Solution()
            {
                Name = "VS2010",
                Projects = SelectProjects(Projects, VsVersion.VS2008, "AspNetWebForms", "AspNetAjax"),
                Version = VsVersion.VS2010
            });

            /*Solutions.Add(new Solution()
            {
                Name = "Mvc1-VS2008",
                Projects = SelectProjects(Projects, VsVersion.VS2008, "AspNetMvc1"),
                Version = VsVersion.VS2008
            });*/

            Solutions.Add(new Solution()
            {
                Name = "Mvc2-VS2008",
                Projects = SelectProjects(Projects, VsVersion.VS2008, "AspNetMvc2"),
                Version = VsVersion.VS2008
            });

            /*Solutions.Add(new Solution()
            {
                Name = "Mvc1-VS2010",
                Projects = SelectProjects(Projects, VsVersion.VS2010, "AspNetMvc1"),
                Version = VsVersion.VS2010
            });*/

            Solutions.Add(new Solution()
            {
                Name = "Mvc2-VS2010",
                Projects = SelectProjects(Projects, VsVersion.VS2010, "AspNetMvc2"),
                Version = VsVersion.VS2010
            });

            Solutions.Add(new Solution()
            {
                Name = "Mvc3+Razor-VS2010",
                Projects = SelectProjects(Projects, VsVersion.VS2010, "AspNetMvc3", "AspNetMvcRazor"),
                Version = VsVersion.VS2010
            });
        }
    }
}