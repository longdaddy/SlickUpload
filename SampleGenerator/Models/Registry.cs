﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleGenerator.Models
{
    public abstract class Registry
    {
        List<Sample> _samples = new List<Sample>();
        List<Project> _projects = new List<Project>();
        List<Solution> _solutions = new List<Solution>();

        public List<Sample> Samples
        {
            get { return _samples; }
        }

        public List<Project> Projects
        {
            get { return _projects; }
        }

        public List<Solution> Solutions
        {
            get { return _solutions; }
        }

        protected List<Project> SelectProjects(List<Project> projects, VsVersion version, params string[] names)
        {
            List<Project> matchedProjects = new List<Project>();

            foreach (string name in names)
                matchedProjects.AddRange(projects.Where(x => x.Versions.Contains(version) && x.Name.StartsWith(name, StringComparison.InvariantCultureIgnoreCase)));

            return matchedProjects;
        }
    }
}