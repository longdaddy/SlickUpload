﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleGenerator.Models
{
    public class SamplesRegistry : Registry
    {
        public SamplesRegistry()
        {
            Samples.Add(new Sample()
            {
                Path = "Slick",
                Name = "Slick",
                Description =
@"A fully skinned implementation of SlickUpload with Gmail style async file upload, multiple file selection, folder selection, and drag and drop."
            });

            Samples.Add(new Sample()
            {
                Path = "ClientApi",
                Name = "Client Api",
                Description =
@"Use the SlickUpload client API to create an upload form without using the SlickUpload server side controls or helpers.",
                HasNoAspNetAjaxSupport = true
            });

            Samples.Add(new Sample()
            {
                Path = "PostProcess",
                Name = "Post Processing",
                Description =
@"Implements a post processing step to process files after they are uploaded, with accompanying progress bar.",
                HasNoAspNetAjaxSupport = true
            });
            
            Samples.Add(new Sample()
            {
                Path = "CustomFileName",
                Name = "Custom FileName",
                Description =
@"Demonstrates how to pass information to the server and generate server filenames for files as they are uploaded."
            });

            Samples.Add(new Sample()
            {
                Path = "CustomUploadStreamProvider",
                Name = "Custom UploadStreamProvider",
                Description =
@"Demonstrates how to write a custom UploadStreamProvider to stream uploads to an arbitrary location. The example UploadStreamProvider zips files as they are uploaded."
            });

            Samples.Add(new Sample()
            {
                Path = "SqlServer",
                Name = "Sql Server",
                Description =
@"Upload directly to a SQL Server IMAGE, VARBINARY(MAX), or FILESTREAM field, streaming the file to the field with minimal memory usage.",
                Configuration =
@"<p>Configuration for the SQL sample involves creating the file database and table and configuring SlickUpload
   to point at that database. To prepare the database, perform the following steps:</p>
<ol>
    <li>Create a new database, or select an existing database to use</li>
    <li>Change the su connection string in the &lt;connectionStrings&gt; web.config section to point to the database selected above</li>
</ol>
<p>SlickUpload supports writing to one of 3 file column data types: IMAGE, VARBINARY(MAX), and FILESTREAM. Each has different configuration steps:</p>
<h3>IMAGE</h3>
<ol>
    <li>Open the SqlFileTable-IMAGE.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named ""sqlServer"", and change the dataType attribute to ""IMAGE""</li>
</ol>
<h3>VARBINARY(MAX)</h3>
<ol>
    <li>Open the SqlFileTable-VARBINARYMAX.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named ""sqlServer"", and change the dataType attribute to ""VarBinaryMax""</li>
</ol>
<h3>FILESTREAM</h3>
<ol>
    <li>Modify the su connection string in the &lt;connectionStrings&gt; web.config section to use integrated security</li>
    <li>Ensure the database selected above has been enabled for FILESTREAM access. For more information, see <a href=""http://msdn.microsoft.com/en-us/library/cc645585.aspx"">How to: Create a FILESTREAM-Enabled Database</a></li>
    <li>Open the SqlFileTable-FILESTREAM.sql script in the SlickUpload distribution package root folder and run it on the database created above. Make sure to update the script based on the name of the FILESTREAM FileGroup you have created.</li>
    <li>Open the web.config, locate the uploadProfile named ""sqlServer"", and change the dataType attribute to ""FileStream""</li>
</ol>
",
                HasModel = true
            });

            Samples.Add(new Sample()
            {
                Path = "CustomSqlServer",
                Name = "Custom Sql Server",
                Description =
@"Demonstrates writing additional fields to SQL as a file is created.",
                Configuration =
@"<p>Configuration for the SQL sample involves creating the file database and table and configuring SlickUpload
   to point at that database. To prepare the database, perform the following steps:</p>
<ol>
    <li>Create a new database, or select an existing database to use</li>
    <li>Change the su connection string in the &lt;connectionStrings&gt; web.config section to point to the database selected above</li>
</ol>
<p>SlickUpload supports writing to one of 3 file column data types: IMAGE, VARBINARY(MAX), and FILESTREAM. Each has different configuration steps:</p>
<h3>IMAGE</h3>
<ol>
    <li>Open the SqlFileTable-IMAGE-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named ""customSqlServer"", and change the dataType attribute to ""IMAGE""</li>
</ol>
<h3>VARBINARY(MAX)</h3>
<ol>
    <li>Open the SqlFileTable-VARBINARYMAX-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above</li>
    <li>Open the web.config, locate the uploadProfile named ""customSqlServer"", and change the dataType attribute to ""VarBinaryMax""</li>
</ol>
<h3>FILESTREAM</h3>
<ol>
    <li>Modify the su connection string in the &lt;connectionStrings&gt; web.config section to use integrated security</li>
    <li>Ensure the database selected above has been enabled for FILESTREAM access. For more information, see <a href=""http://msdn.microsoft.com/en-us/library/cc645585.aspx"">How to: Create a FILESTREAM-Enabled Database</a></li>
    <li>Open the SqlFileTable-FILESTREAM-Custom.sql script in the SlickUpload distribution package root folder and run it on the database created above. Make sure to update the script based on the name of the FILESTREAM FileGroup you have created.</li>
    <li>Open the web.config, locate the uploadProfile named ""customSqlServer"", and change the dataType attribute to ""FileStream""</li>
</ol>
",
                HasModel = true
            });

            Samples.Add(new Sample()
            {
                Path = "Multiple",
                Name = "Multiple",
                Description =
@"How to implement multiple file selection areas on one page."
            });

            Samples.Add(new Sample()
            {
                Path = "S3",
                Name = "Amazon S3",
                Description =
@"Use the SlickUpload S3 provider to implement streaming upload to Amazon S3."
            });

            Samples.Add(new Sample()
            {
                Path = "Validation",
                Name = "Validation",
                Description =
@"Demonstrates SlickUpload validation. Covers file selection, maximum selected file limits, maximum file size limits, file type validation, and requiring file selection.",
                HasModel = true
            });

            Projects.Add(new Project()
            {
                Name = "AspNetWebFormsCs",
                Versions = new VsVersion[] { VsVersion.VS2005, VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.WebForms
            });

            Projects.Add(new Project()
            {
                Name = "AspNetWebFormsVb",
                Versions = new VsVersion[] { VsVersion.VS2005, VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.WebForms
            });

            Projects.Add(new Project()
            {
                Name = "AspNetAjaxCs",
                Versions = new VsVersion[] { VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.WebForms,
                IsAspNetAjax = true
            });

            Projects.Add(new Project()
            {
                Name = "AspNetAjaxVb",
                Versions = new VsVersion[] { VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.WebForms,
                IsAspNetAjax = true
            });

            /*Projects.Add(new Project()
            {
                Name = "AspNetMvc1Cs",
                Versions = new VsVersion[] { VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc1
            });

            Projects.Add(new Project()
            {
                Name = "AspNetMvc1Vb",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc1
            });*/

            Projects.Add(new Project()
            {
                Name = "AspNetMvc2Cs",
                Versions = new VsVersion[] { VsVersion.VS2008, VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc2
            });

            Projects.Add(new Project()
            {
                Name = "AspNetMvc2Vb",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc2
            });

            Projects.Add(new Project()
            {
                Name = "AspNetMvc3Cs",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc3
            });

            Projects.Add(new Project()
            {
                Name = "AspNetMvc3Vb",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.Mvc,
                MvcVersion = MvcVersion.Mvc3
            });
            
            Projects.Add(new Project()
            {
                Name = "AspNetMvcRazorCs",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Cs,
                ViewEngine = ViewEngine.Razor,
                MvcVersion = MvcVersion.Mvc3
            });

            /*Projects.Add(new Project()
            {
                Name = "AspNetMvcRazorVb",
                Versions = new VsVersion[] { VsVersion.VS2010 },
                Language = CodeLanguage.Vb,
                ViewEngine = ViewEngine.Razor,
                MvcVersion = MvcVersion.Mvc3
            });*/

            Solutions.Add(new Solution()
            {
                Name = "VS2005",
                Projects = SelectProjects(Projects, VsVersion.VS2005, "AspNetWebForms"),
                Version = VsVersion.VS2005
            });

            Solutions.Add(new Solution()
            {
                Name = "VS2008",
                Projects = SelectProjects(Projects, VsVersion.VS2008, "AspNetWebForms", "AspNetAjax"),
                Version = VsVersion.VS2008
            });

            Solutions.Add(new Solution()
            {
                Name = "VS2010",
                Projects = SelectProjects(Projects, VsVersion.VS2008, "AspNetWebForms", "AspNetAjax"),
                Version = VsVersion.VS2010
            });

            /*Solutions.Add(new Solution()
            {
                Name = "Mvc1-VS2008",
                Projects = SelectProjects(Projects, VsVersion.VS2008, "AspNetMvc1"),
                Version = VsVersion.VS2008
            });*/

            Solutions.Add(new Solution()
            {
                Name = "Mvc2-VS2008",
                Projects = SelectProjects(Projects, VsVersion.VS2008, "AspNetMvc2"),
                Version = VsVersion.VS2008
            });

            /*Solutions.Add(new Solution()
            {
                Name = "Mvc1-VS2010",
                Projects = SelectProjects(Projects, VsVersion.VS2010, "AspNetMvc1"),
                Version = VsVersion.VS2010
            });*/

            Solutions.Add(new Solution()
            {
                Name = "Mvc2-VS2010",
                Projects = SelectProjects(Projects, VsVersion.VS2010, "AspNetMvc2"),
                Version = VsVersion.VS2010
            });

            Solutions.Add(new Solution()
            {
                Name = "Mvc3+Razor-VS2010",
                Projects = SelectProjects(Projects, VsVersion.VS2010, "AspNetMvc3", "AspNetMvcRazor"),
                Version = VsVersion.VS2010
            });
        }
    }
}