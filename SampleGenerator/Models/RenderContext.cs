﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleGenerator.Models
{
    public class RenderContext
    {
        public string Path { get; private set; }
        public string Name { get; private set; }

        public RenderContext(string path, string name)
        {
            Path = path;
            Name = name;
        }
    }
}