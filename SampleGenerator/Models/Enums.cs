﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleGenerator.Models
{
    public enum FrameworkVersion
    {
        NET2,
        NET35,
        NET4
    }

    public enum CodeLanguage
    {
        Cs,
        Vb
    }

    public enum ViewEngine
    {
        WebForms,
        Mvc,
        Razor
    }

    public enum VsVersion
    {
        VS2005,
        VS2008,
        VS2010
    }

    public enum MvcVersion
    {
        None,
        Mvc1,
        Mvc2,
        Mvc3
    }

}