﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleGenerator.Models
{
    public class Sample
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Configuration { get; set; }
        public bool HasModel { get; set; }
        public bool HasNoAspNetAjaxSupport { get; set; }
    }
}