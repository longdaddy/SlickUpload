﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleGenerator.Models
{
    public class Project
    {
        readonly static Guid _csTypeGuid = new Guid("{fae04ec0-301f-11d3-bf4b-00c04f79efbc}");
        readonly static Guid _vbTypeGuid = new Guid("{F184B08F-C81C-45F6-A57F-5ABD9991F28F}");

        public string Name { get; set; }
        public VsVersion[] Versions { get; set; }
        public MvcVersion MvcVersion { get; set; }
        public ViewEngine ViewEngine { get; set; }
        public CodeLanguage Language { get; set; }
        public bool IsAspNetAjax { get; set; }

        Guid _projectGuid = Guid.NewGuid();

        public Guid ProjectGuid
        {
            get { return _projectGuid; }
        }

        public Guid TypeGuid
        {
            get
            {
                return Language == CodeLanguage.Cs ? _csTypeGuid : _vbTypeGuid;
            }
        }

        public string GetProjectFileName(int versionIndex)
        {
            return Name + (versionIndex > 0 ? "-" + Versions[versionIndex].ToString() : "") + "." + Language.ToString().ToLower() + "proj";
        }

        public string GetProjectFileName(VsVersion version)
        {
            for (int i = 0; i < Versions.Length; i++)
            {
                if (Versions[i] == version)
                    return GetProjectFileName(i);
            }

            throw new Exception("Version not found.");
        }
        public FrameworkVersion TargetFramework
        {
            get
            {
                if (MvcVersion == MvcVersion.Mvc3 || (ViewEngine == ViewEngine.Mvc && Language == CodeLanguage.Vb))
                    return FrameworkVersion.NET4;
                else if (ViewEngine == ViewEngine.Mvc || IsAspNetAjax)
                    return FrameworkVersion.NET35;
                else
                    return FrameworkVersion.NET2;
            }
        }
    }
}