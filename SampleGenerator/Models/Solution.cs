﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleGenerator.Models
{
    public class Solution
    {
        public string Name { get; set; }
        public VsVersion Version { get; set; }
        public List<Project> Projects { get; set; }
    }
}