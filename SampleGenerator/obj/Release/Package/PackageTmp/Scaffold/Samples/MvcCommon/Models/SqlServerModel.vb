﻿Imports Krystalware.SlickUpload

Imports $Namespace.Storage

Namespace Models
    Public Class SqlServerModel
        Public Property ExistingFiles As List(Of SqlFile)
        Public Property Exception As Exception
        Public Property UploadSession As UploadSession
    End Class
End Namespace