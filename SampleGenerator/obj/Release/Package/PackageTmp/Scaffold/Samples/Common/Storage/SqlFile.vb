Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Configuration
Imports Krystalware.SlickUpload.Storage
Imports Krystalware.SlickUpload.Storage.Streams

Namespace Storage
    Public Class SqlFile
        Private _id As Long
        Private _name As String
        Private _category As String
        Private _keyField As String
        Private _length As Long

        Public ReadOnly Property Id() As Long
            Get
                Return _id
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                Return _name
            End Get
        End Property

        Public ReadOnly Property Category() As String
            Get
                Return _category
            End Get
        End Property

        Public ReadOnly Property Length() As Long
            Get
                Return _length
            End Get
        End Property

        Public Sub New(ByVal id As Long, ByVal name As String, ByVal length As Long, keyField As String)
            Me.New(id, name, length, keyfield, Nothing)
        End Sub

        Public Sub New(ByVal id As Long, ByVal name As String, ByVal length As Long, keyField As String, ByVal category As String)
            _id = id
            _name = name
            _length = length
            _keyField = keyField
            _category = category
        End Sub

        Friend Function BuildCriteria(cmd As IDbCommand) As String
            Dim param As IDbDataParameter = cmd.CreateParameter()

            param.ParameterName = "@keyValue"
            param.DbType = DbType.String
            param.Value = _id

            cmd.Parameters.Add(param)

            Return _keyField + "=@keyValue"
        End Function
    End Class
End Namespace