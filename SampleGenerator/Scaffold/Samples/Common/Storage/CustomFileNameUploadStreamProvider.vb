﻿Imports System

Imports Krystalware.SlickUpload
Imports Krystalware.SlickUpload.Configuration
Imports Krystalware.SlickUpload.Storage

Namespace Storage
    Public Class CustomFileNameUploadStreamProvider
        Inherits FileUploadStreamProvider

        Public Sub New(settings As UploadStreamProviderElement)
            MyBase.New(settings)
        End Sub

        Public Overrides Function GetServerFileName(file As UploadedFile) As String
            Dim fileRoot As String = file.UploadRequest.Data("fileRoot")

            Return fileRoot + "\" + GetValidFileName(file.ClientName)
        End Function
    End Class
End Namespace