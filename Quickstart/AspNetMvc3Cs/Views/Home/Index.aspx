<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Krystalware.SlickUpload.UploadSession>" EnableViewState="false" Title="SlickUpload Quick Start Example" %>
<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web" %>
<%@ Import Namespace="Krystalware.SlickUpload.Web.Mvc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
    function cancelUpload()
    {
        kw("slickUpload").cancel();
    }

    function onSessionStarted(data)
    {
        document.getElementById("uploadButton").style.display = "none";
        document.getElementById("cancelButton").style.display = "block";
    }
        
    function onBeforeSessionEnd(data)
    {
        document.getElementById("cancelButton").style.display = "none";
    }
</script>

<style type="text/css">
    /* hide initially  */
    .su-fileselector, .su-folderselector, .su-filelisttemplate, .su-uploadprogressdisplay
    {
        display: none;
        zoom:1;
    }
    
    /* fileselector cursor, link color */
    .su-fileselector, .su-fileselector *, .su-folderselector, .su-folderselector *
    {
        color:blue;
        cursor:pointer;
    }

    /* hover links */
    a
    {
        text-decoration:none
    }
    
    a:hover, .su-hover a
    {
        text-decoration:underline;
    }
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <% using (Html.BeginForm("Index", "Home", FormMethod.Post, new { id = "uploadForm", enctype = "multipart/form-data" })) { %>

                       <% Html.KrystalwareWebForms(new SlickUpload() { 
Id = "slickUpload",
FileSelectorHtmlAttributes = new { Style = "float:left;padding-right:1em"},
FileListHtmlAttributes = new { Style = "clear:both"},
UploadProgressDisplayHtmlAttributes = new { Style = "clear:both"},
HtmlAttributes = new { Style = "overflow: hidden; zoom: 1"},
UploadProfile = "quickStart",
OnClientUploadSessionStarted = "onSessionStarted",
OnClientBeforeSessionEnd = "onBeforeSessionEnd",
ShowDropZoneOnDocumentDragOver = true,
AutoUploadOnSubmit = true,
UploadFormId = "uploadForm",
SelectorTemplate = new Template(() => { %>
                          
                                    <a href="javascript:;">Add files</a>
                                    
<% }),
SelectorFolderTemplate = new Template(() => { %>
                          
                                    <a href="javascript:;">Add folder</a>
                                    
<% }),
FileItemTemplate = new Template(() => { %>
                          
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.FileName } ); %>
                                        &ndash;
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.FileSize, Template = new Template("(calculating)") } ); %>
                                        <% Html.KrystalwareWebForms(new FileListRemoveCommand() { HtmlAttributes = new { href = "javascript:;"}, Template = new Template("[x]") } ); %>
                                        <% Html.KrystalwareWebForms(new FileListElement() { Element = FileListElementType.ValidationMessage, HtmlAttributes = new { style = "color:#f00"} } ); %>
                                    
<% }),
ProgressTemplate = new Template(() => { %>
                          
                                        <div>
                                            Uploading <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.FileCount } ); %> file(s),
                                            <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.ContentLengthText, Template = new Template("(calculating)") } ); %>.
                                        </div>
                                        <div>
                                            Currently uploading: <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.CurrentFileName } ); %>
                                            file <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.CurrentFileIndex } ); %>
                                            of <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.FileCount } ); %>.
                                        </div>
                                        <div>
                                            Speed: <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.SpeedText, Template = new Template("(calculating)") } ); %>
                                        </div>
                                        <div>
                                            <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.TimeRemainingText, Template = new Template("(calculating)") } ); %>
                                        </div>
                                        <div style="border:1px solid #008800; height: 1.5em; position: relative;">
                                            <% Html.KrystalwareWebForms(new UploadProgressBar() { HtmlAttributes = new { style = "background-color:#00ee00;width:0; height:1.5em;"} } ); %>
                                            <div style="text-align: center; position: absolute; top: .15em; width: 100%;">
                                                <% Html.KrystalwareWebForms(new UploadProgressElement() { Element = UploadProgressElementType.PercentCompleteText, Template = new Template("(calculating)") } ); %>
                                            </div>
                                        </div>
                                    
<% })
 } ); %>
<% Html.KrystalwareWebForms(new KrystalwareScriptRenderer()); %>                            <hr />
                                <p>
                                    <input type="submit" value="Upload" id="uploadButton" />
                                    <input type="button" value="Cancel" id="cancelButton" style="display:none" onclick="cancelUpload()" />
                                </p>
<% if (Model != null) { %>                        
                        Upload Result: <%:Model.State.ToString() %><% if (Model.State == UploadState.Error) { %>                        <br /><br />
                        Message: <%:Model.ErrorSummary %><% } %>                        <ul>
                        <% foreach (UploadedFile file in Model.UploadedFiles) { %>
                        <li>
                            <%:file.ClientName %>
                        </li>
                        <% } %>
                        </ul>
<% } %>
    <% } %>
</asp:Content>
