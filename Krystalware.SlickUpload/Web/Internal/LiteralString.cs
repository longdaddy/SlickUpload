﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Krystalware.SlickUpload.Web
{
    class LiteralString
    {
        public string Value { get; private set; }

        public LiteralString(string value)
        {
            Value = value;
        }
    }
}
