﻿using System;
using System.Collections.Generic;
using System.Text;
using Krystalware.SlickUpload.Web.Mvc;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Krystalware.SlickUpload.Web
{
    interface ISlickUpload : IRenderableComponent
    {
        IFileSelector FileSelector { get; }
        IFileList FileList { get; }
        IUploadProgressDisplay UploadProgressDisplay { get; }
        IUploadConnector UploadConnector { get; }
    }
}
