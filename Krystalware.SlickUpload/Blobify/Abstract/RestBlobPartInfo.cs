﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Krystalware.Blobify.Abstract
{
    public abstract class RestBlobPartInfo
    {
        public string PartId { get; internal set; }
        public int Length { get; internal set; }
    }
}
