﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Krystalware.Blobify.Abstract
{
    public class S3BlobPartInfo : RestBlobPartInfo
    {
        public string ETag { get; internal set; }
    }
}
